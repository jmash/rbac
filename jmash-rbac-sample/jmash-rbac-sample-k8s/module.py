import subprocess
import argparse
import pathlib 
import sys

def run_mvn_command(name,param):
    command = f"mvn -f ../{name}/pom.xml clean source:jar {param}"
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print("Maven命令执行成功！")
        print("标准输出：", result.stdout.decode())
    except subprocess.CalledProcessError as e:
        print("Maven命令执行失败！")
        print("错误码：", e.returncode)
        print("标准输出：", e.stdout.decode() if e.stdout else "")
        print("标准错误：", e.stderr.decode())


def package_sample(name):
    """
    Maven打包和Go Gateway打包
    """
    run_mvn_command(f"{name}","deploy")

    command = f"../{name}-gateway/gen-grpc-gateway.sh"
    if (sys.platform.startswith('win')):
       command = f"../{name}-gateway/gen-grpc-gateway.bat"

    try:
        subprocess.run(command, shell=True, check=True)
        print(f"{name}-gateway 执行完成.")
    except subprocess.CalledProcessError as e:
        print(f"{name}-gateway 执行出错: {e}")

def docker_build_push(name,arch,version):
    """
    docker build 和 push 到docker 仓库
    """
    command1 = f"docker build --platform linux/{arch} --build-arg GOARCH={arch}  -t {name}-service-{arch}:1.0.0  -f ./Dockerfile  ../"
    command2 = f"docker login harbor.crenjoy.com:9443 --username jmash --password Jmash123"
    command3 = f"docker tag  {name}-service-{arch}:1.0.0  harbor.crenjoy.com:9443/library/{name}-service-{arch}:{version}"    
    command4 = f"docker push harbor.crenjoy.com:9443/library/{name}-service-{arch}:{version}"
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        subprocess.run(command4, shell=True, check=True)
        print(f"应用程序{name}-service-{arch}:{version}已push docker 仓库.")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{name}-service-{arch}:{version} 出错: {e}")


def replace_text_save_new_file(file_path, replacements, new_file_path):
    """
    替换多个文本并保存新文件
    """
    with open(file_path, 'r') as file:
        content = file.read()

    for old_text, new_text in replacements:
        content = content.replace(old_text, new_text)

    with open(new_file_path, 'w') as file:
        file.write(content)

def tag_docker(name , arch,version):
    """
    标记docker应用
    """
    command1 = f"docker  pull harbor.crenjoy.com/library/{name}-service-{arch}:1.0.0"
    command2 = f"docker  tag  {name}-service-{arch}:1.0.0  harbor.crenjoy.com:9443/library/{name}-service-{arch}:{version}"    
    command3 = f"docker  push harbor.crenjoy.com:9443/library/{name}-service-{arch}:{version}"
    try:
       # subprocess.run(command1, shell=True, check=True)
       # subprocess.run(command2, shell=True, check=True)
       # subprocess.run(command3, shell=True, check=True)
        print(f"应用程序{name}-service-{arch}:{version}已push docker 仓库.")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{name}-service-{arch}:{version} 出错: {e}")

def deploy_application(name , arch,version):
    """
    部署K8S应用程序
    """        
    replacements = [ ('{arch}', arch), ('{version}', version), ]
    
    replace_text_save_new_file("k8s-deployment-template.yaml",replacements,f"k8s-deployment-{arch}.yaml")

    command1 = f"kubectl apply -f k8s-configmap.yaml"    
    command2 = f"kubectl apply -f k8s-deployment-{arch}.yaml"
    try:
         subprocess.run(command1, shell=True, check=True)
         subprocess.run(command2, shell=True, check=True)
         print(f"应用程序{name}已成功部署到 {arch} 架构的节点上。")
    except subprocess.CalledProcessError as e:
        print(f"部署应用程序{name}时出错: {e}")

def restart_application(name , arch,namespace):
    """
    重启K8S应用程序
    """
    command1 = f"kubectl apply -f k8s-configmap.yaml" 
    command2 = f"kubectl rollout restart deploy {name}-service-deploy -n {namespace} "    
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        print(f"应用程序{name}已成功重启。")
    except subprocess.CalledProcessError as e:
        print(f"启动应用程序{name}时出错: {e}")

def subapp_application(name,arch,namespace):
    """
    更新应用
    """
    command1 = f"kubectl apply -f ./{name}-k8s/k8s-ingress.yaml"
    command2 = f"kubectl apply -f ./{name}-k8s/k8s-ingress-gateway.yaml" 
    command3 = f"kubectl apply -f ./{name}-k8s/k8s-service.yaml"  
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        print(f"应用程序{name}配置更新。")
        scale_application(name,namespace,0);
    except subprocess.CalledProcessError as e:
        print(f"启动应用程序{name}配置更新时出错: {e}")        

def delete_application(name ,arch):
    """
    删除K8S应用程序
    """
    command1 = f"kubectl delete -f k8s-configmap.yaml" 
    command2 = f"kubectl delete -f k8s-deployment-{arch}.yaml"    
    command3 = f"kubectl delete -f k8s-service.yaml"
    command4 = f"kubectl delete -f k8s-ingress.yaml"
    command5 = f"kubectl delete -f k8s-ingress-gateway.yaml"
    
    try:
        subprocess.run(command1, shell=True, check=True)
        subprocess.run(command2, shell=True, check=True)
        subprocess.run(command3, shell=True, check=True)
        subprocess.run(command4, shell=True, check=True)
        subprocess.run(command5, shell=True, check=True)
        print(f"应用程序{name}已成功在 {arch} 架构的节点上停止。")
    except subprocess.CalledProcessError as e:
        print(f"停止应用程序{name}时出错: {e}")
        
def scale_application(name, namespace, replicas):
    """
    设置副本数量K8S应用程序
    """
    command1 = f"kubectl scale deployment {name}-service-deploy  --replicas={replicas} -n {namespace} "  
    try:
        subprocess.run(command1, shell=True, check=True) 
        print(f"应用程序{name}-service-deploy 设置副本数量{replicas}。")
    except subprocess.CalledProcessError as e:
        print(f"设置副本数量应用程序{name}时出错: {e}")

def test_application(name, namespace, replicas):
    """
    本地测试服务
    """
    command1 = f"kubectl apply -f k8s-test-ingress.yaml"  
    command2 = f"kubectl apply -f k8s-test-ingress-gateway.yaml"  
    try:
        subprocess.run(command1, shell=True, check=True) 
        subprocess.run(command2, shell=True, check=True) 
        print(f"应用程序{name}-service-deploy 开启本地测试服务。")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{name}-service-deploy 开启本地测试服务时出错: {e}")
def notest_application(name, namespace, replicas):
    """
    停止本地测试服务
    """
    command1 = f"kubectl apply -f k8s-ingress-gateway.yaml"  
    try:
        subprocess.run(command1, shell=True, check=True) 
        print(f"应用程序{name}-service-deploy 停止测试服务。")
    except subprocess.CalledProcessError as e:
        print(f"应用程序{name}-service-deploy 停止测试服务时出错: {e}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="K8S应用程序管理脚本")
    parser.add_argument("action", choices=["package","publish","deploy", "restart", "delete", "scale", "test","notest"], help="要执行的操作")
    parser.add_argument("arch", choices=["amd64", "arm64"], help="目标架构")
    
    parser.add_argument("--name", default="jmash-rbac-sample", help="应用程序名称")
    parser.add_argument("--namespace", default="jmash", help="命名空间")
    parser.add_argument("--version", default="1.0.0", help="版本")
    parser.add_argument("--replicas", default="1", help="副本数量")
            
    args = parser.parse_args()
    # 路径
    # args.path= pathlib.Path.cwd()

    
    print(args);

    if args.action == "package":
        package_sample(args.name)
    elif args.action == "publish":
        docker_build_push(args.name,args.arch, args.version)
    elif args.action == "deploy":
        deploy_application(args.name, args.arch, args.version)
        subapp_application("jmash-captcha", args.arch, args.namespace)
        subapp_application("jmash-dict", args.arch, args.namespace)
        subapp_application("jmash-file", args.arch, args.namespace)      
        subapp_application("jmash-sms", args.arch, args.namespace)
        subapp_application("jmash-rbac", args.arch, args.namespace)
        subapp_application("jmash-region", args.arch, args.namespace)
    elif args.action == "restart":
        restart_application(args.name, args.arch, args.namespace)
        subapp_application("jmash-captcha", args.arch, args.namespace)
        subapp_application("jmash-dict", args.arch, args.namespace)
        subapp_application("jmash-file", args.arch, args.namespace)    
        subapp_application("jmash-sms", args.arch, args.namespace)
        subapp_application("jmash-rbac", args.arch, args.namespace)  
        subapp_application("jmash-region", args.arch, args.namespace)
    elif args.action == "delete":
        delete_application(args.name, args.arch)
    elif args.action == "scale":
        scale_application(args.name, args.namespace, args.replicas)
    elif args.action == "test":
        test_application(args.name,args.namespace, args.replicas)
    elif args.action == "notest":
        notest_application(args.name, args.namespace, args.replicas)
   

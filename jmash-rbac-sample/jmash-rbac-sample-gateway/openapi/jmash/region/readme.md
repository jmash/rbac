# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/region/region_rpc.proto](#jmash_region_region_rpc-proto)
    - [Region](#jmash-region-Region)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_region_region_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/region/region_rpc.proto


 

 

 


<a name="jmash-region-Region"></a>

### Region
Region Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| findEnumList | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.EnumValueList](#jmash-protobuf-EnumValueList) | 枚举值列表 |
| findEnumMap | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap) | 枚举值Map |
| findEnumEntry | [.jmash.protobuf.EnumEntryReq](#jmash-protobuf-EnumEntryReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 枚举值 |
| findDictRegionList | [DictRegionReq](#jmash-region-DictRegionReq) | [DictRegionList](#jmash-region-DictRegionList) | 查询列表信息地区信息 |
| findDictRegionById | [DictRegionKey](#jmash-region-DictRegionKey) | [DictRegionModel](#jmash-region-DictRegionModel) | 查询地区信息 |
| findDictRegionByReq | [DictRegionReq](#jmash-region-DictRegionReq) | [DictRegionModel](#jmash-region-DictRegionModel) | 根据地区编码查询地区信息 |
| createDictRegion | [DictRegionCreateReq](#jmash-region-DictRegionCreateReq) | [DictRegionModel](#jmash-region-DictRegionModel) | 创建实体地区信息 |
| updateDictRegion | [DictRegionUpdateReq](#jmash-region-DictRegionUpdateReq) | [DictRegionModel](#jmash-region-DictRegionModel) | 修改实体地区信息 |
| moveDictRegion | [DictRegionMoveKey](#jmash-region-DictRegionMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移 |
| enableDictRegion | [DictRegionEnableKey](#jmash-region-DictRegionEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用/禁用 |
| deleteDictRegion | [DictRegionKey](#jmash-region-DictRegionKey) | [DictRegionModel](#jmash-region-DictRegionModel) | 删除地区信息 |
| batchDeleteDictRegion | [DictRegionKeyList](#jmash-region-DictRegionKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除地区信息 |
| downloadDictRegion | [DictRegionExportReq](#jmash-region-DictRegionExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载导入模板地区信息 |
| importDictRegion | [DictRegionImportReq](#jmash-region-DictRegionImportReq) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 导入地区信息 |
| exportDictRegion | [DictRegionExportReq](#jmash-region-DictRegionExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出地区信息 |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |


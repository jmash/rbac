# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/dict/dict_rpc.proto](#jmash_dict_dict_rpc-proto)
    - [Dict](#jmash-dict-Dict)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_dict_dict_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/dict/dict_rpc.proto


 

 

 


<a name="jmash-dict-Dict"></a>

### Dict
Dict Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| findEnumList | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.EnumValueList](#jmash-protobuf-EnumValueList) | 枚举值列表 |
| findEnumMap | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap) | 枚举值Map |
| findEnumEntry | [.jmash.protobuf.EnumEntryReq](#jmash-protobuf-EnumEntryReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 枚举值 |
| findDictEntryPage | [DictEntryReq](#jmash-dict-DictEntryReq) | [DictEntryPage](#jmash-dict-DictEntryPage) | 查询翻页信息数据字典 |
| findDictEntryList | [DictEntryReq](#jmash-dict-DictEntryReq) | [DictEntryList](#jmash-dict-DictEntryList) | 查询列表信息数据字典 |
| findDictEntryById | [DictEntryKey](#jmash-dict-DictEntryKey) | [DictEntryModel](#jmash-dict-DictEntryModel) | 查询数据字典 |
| createDictEntry | [DictEntryCreateReq](#jmash-dict-DictEntryCreateReq) | [DictEntryModel](#jmash-dict-DictEntryModel) | 创建实体数据字典 |
| updateDictEntry | [DictEntryUpdateReq](#jmash-dict-DictEntryUpdateReq) | [DictEntryModel](#jmash-dict-DictEntryModel) | 修改实体数据字典 |
| deleteDictEntry | [DictEntryKey](#jmash-dict-DictEntryKey) | [DictEntryModel](#jmash-dict-DictEntryModel) | 删除数据字典 |
| batchDeleteDictEntry | [DictEntryKeyList](#jmash-dict-DictEntryKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除数据字典 |
| moveDictEntry | [DictEntryMoveKey](#jmash-dict-DictEntryMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移 |
| enableDictEntry | [DictEntryEnableKey](#jmash-dict-DictEntryEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用/禁用 |
| findDictLayEntryList | [DictLayEntryReq](#jmash-dict-DictLayEntryReq) | [DictLayEntryList](#jmash-dict-DictLayEntryList) | 查询列表信息层级字典实体 |
| findDictLayEntryById | [DictLayEntryKey](#jmash-dict-DictLayEntryKey) | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | 查询层级字典实体 |
| createDictLayEntry | [DictLayEntryCreateReq](#jmash-dict-DictLayEntryCreateReq) | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | 创建实体层级字典实体 |
| updateDictLayEntry | [DictLayEntryUpdateReq](#jmash-dict-DictLayEntryUpdateReq) | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | 修改实体层级字典实体 |
| deleteDictLayEntry | [DictLayEntryKey](#jmash-dict-DictLayEntryKey) | [DictLayEntryModel](#jmash-dict-DictLayEntryModel) | 删除层级字典实体 |
| batchDeleteDictLayEntry | [DictLayEntryKeyList](#jmash-dict-DictLayEntryKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除层级字典实体 |
| moveDictLayEntry | [DictLayEntryMoveKey](#jmash-dict-DictLayEntryMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移 |
| enableDictLayEntry | [DictLayEntryEnableKey](#jmash-dict-DictLayEntryEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用/禁用 |
| findDictTypePage | [DictTypeReq](#jmash-dict-DictTypeReq) | [DictTypePage](#jmash-dict-DictTypePage) | 查询翻页信息字典类型 |
| findDictTypeList | [DictTypeReq](#jmash-dict-DictTypeReq) | [DictTypeList](#jmash-dict-DictTypeList) | 查询列表信息字典类型 |
| findDictTypeById | [DictTypeKey](#jmash-dict-DictTypeKey) | [DictTypeModel](#jmash-dict-DictTypeModel) | 查询字典类型 |
| createDictType | [DictTypeCreateReq](#jmash-dict-DictTypeCreateReq) | [DictTypeModel](#jmash-dict-DictTypeModel) | 创建实体字典类型 |
| updateDictType | [DictTypeUpdateReq](#jmash-dict-DictTypeUpdateReq) | [DictTypeModel](#jmash-dict-DictTypeModel) | 修改实体字典类型 |
| deleteDictType | [DictTypeKey](#jmash-dict-DictTypeKey) | [DictTypeModel](#jmash-dict-DictTypeModel) | 删除字典类型 |
| batchDeleteDictType | [DictTypeKeyList](#jmash-dict-DictTypeKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除字典类型 |
| moveDictType | [DictTypeMoveKey](#jmash-dict-DictTypeMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移 |
| enableDictType | [DictTypeEnableKey](#jmash-dict-DictTypeEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用/禁用 |
| downloadDictType | [DictTypeExportReq](#jmash-dict-DictTypeExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载导入模板字典类型 |
| importDictType | [DictTypeImportReq](#jmash-dict-DictTypeImportReq) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 导入字典类型 |
| exportDictType | [DictTypeExportReq](#jmash-dict-DictTypeExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出字典类型 |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |


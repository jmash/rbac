# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/rbac/rbac_rpc.proto](#jmash_rbac_rbac_rpc-proto)
    - [Rbac](#jmash-rbac-Rbac)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_rbac_rbac_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/rbac_rpc.proto


 

 

 


<a name="jmash-rbac-Rbac"></a>

### Rbac
Rbac Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| findEnumList | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.EnumValueList](#jmash-protobuf-EnumValueList) | 枚举值列表 |
| findEnumMap | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap) | 枚举值Map |
| findEnumEntry | [.jmash.protobuf.EnumEntryReq](#jmash-protobuf-EnumEntryReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 枚举值 |
| login | [LoginReq](#jmash-rbac-LoginReq) | [TokenResp](#jmash-rbac-TokenResp) | 用户登录 |
| loginByQrcode | [LoginQrcodeReq](#jmash-rbac-LoginQrcodeReq) | [TokenResp](#jmash-rbac-TokenResp) | 二维码扫码登录. |
| sendValidCode | [SendValidCodeReq](#jmash-rbac-SendValidCodeReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 发送验证码. |
| loginByValidCode | [ValidCodeLoginReq](#jmash-rbac-ValidCodeLoginReq) | [TokenResp](#jmash-rbac-TokenResp) | 通过验证码登录 |
| selectDsdRoles | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [DsdRoleListResp](#jmash-rbac-DsdRoleListResp) | 登录选择动态互斥角色,无需权限. |
| logout | [LogoutReq](#jmash-rbac-LogoutReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 用户登出. |
| refreshToken | [RefreshTokenReq](#jmash-rbac-RefreshTokenReq) | [TokenResp](#jmash-rbac-TokenResp) | 刷新Token |
| userInfo | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [UserModel](#jmash-rbac-UserModel) | 获取当前会话用户(含DSD动态职责分离). |
| updateUserInfo | [UpdateUserReq](#jmash-rbac-UpdateUserReq) | [UserModel](#jmash-rbac-UserModel) | 个人中心修改个人信息 |
| userRolesPerms | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [RolePermSet](#jmash-rbac-RolePermSet) | 获取当前会话角色列表和权限列表. |
| userMenus | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [MenuList](#jmash-rbac-MenuList) | 获取当前会话用户菜单 |
| changePwd | [ChangePwdReq](#jmash-rbac-ChangePwdReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 修改密码 |
| bindPhoneEmail | [BindPhoneEmailReq](#jmash-rbac-BindPhoneEmailReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 重新绑定手机号/邮箱 |
| runAsUser | [RunAsReq](#jmash-rbac-RunAsReq) | [UserModel](#jmash-rbac-UserModel) | 切换用户身份 |
| allowRunAsUser | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [UserList](#jmash-rbac-UserList) | 默认可切换用户 |
| findUserPage | [UserReq](#jmash-rbac-UserReq) | [UserPage](#jmash-rbac-UserPage) | 查询翻页信息用户 |
| findUserList | [UserReq](#jmash-rbac-UserReq) | [UserList](#jmash-rbac-UserList) | 查询列表信息用户 |
| findUserById | [UserKey](#jmash-rbac-UserKey) | [UserModel](#jmash-rbac-UserModel) | 查询用户 |
| findUserByName | [UserNameReq](#jmash-rbac-UserNameReq) | [UserModel](#jmash-rbac-UserModel) | 根据用户名查询用户 |
| findUserInfoList | [UserReq](#jmash-rbac-UserReq) | [UserInfoList](#jmash-rbac-UserInfoList) | 查询用户列表 |
| findUserInfoPage | [UserReq](#jmash-rbac-UserReq) | [UserInfoPage](#jmash-rbac-UserInfoPage) | 查询用户分页 |
| createUser | [UserCreateReq](#jmash-rbac-UserCreateReq) | [UserModel](#jmash-rbac-UserModel) | 创建实体用户 |
| updateUser | [UserUpdateReq](#jmash-rbac-UserUpdateReq) | [UserModel](#jmash-rbac-UserModel) | 修改实体用户 |
| deleteUser | [UserKey](#jmash-rbac-UserKey) | [UserModel](#jmash-rbac-UserModel) | 删除用户 |
| batchDeleteUser | [UserKeyList](#jmash-rbac-UserKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除用户 |
| existUser | [VerifyUserReq](#jmash-rbac-VerifyUserReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 校验邮箱/手机号/用户名/是否存在 |
| batchEnableUser | [EnableUserReq](#jmash-rbac-EnableUserReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量启用禁用用户 |
| enableUser | [UserEnableKey](#jmash-rbac-UserEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用禁用用户 |
| lockUser | [LockUserReq](#jmash-rbac-LockUserReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 锁定/解锁用户 |
| approvedUser | [ApprovedUserReq](#jmash-rbac-ApprovedUserReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 审核/取消审核用户 |
| assignUser | [UserRoleReq](#jmash-rbac-UserRoleReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 给用户分配角色 |
| deassignUser | [UserRoleReq](#jmash-rbac-UserRoleReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 取消用户分配角色 |
| resetPwd | [UserResetPwdReq](#jmash-rbac-UserResetPwdReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 后台用户重置密码 |
| exportUser | [UserExportReq](#jmash-rbac-UserExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出用户 |
| printUser | [UserExportReq](#jmash-rbac-UserExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 打印用户 |
| downloadUserTemplate | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载导入模板用户 |
| importUser | [UserImportReq](#jmash-rbac-UserImportReq) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 导入用户 |
| selectDirectoryIds | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [DirectoryListResp](#jmash-rbac-DirectoryListResp) | 查询目录ID |
| findModulePage | [ModuleReq](#jmash-rbac-ModuleReq) | [ModulePage](#jmash-rbac-ModulePage) | 查询翻页信息系统模块 |
| findModuleList | [ModuleReq](#jmash-rbac-ModuleReq) | [ModuleList](#jmash-rbac-ModuleList) | 查询列表信息系统模块 |
| findModuleById | [ModuleKey](#jmash-rbac-ModuleKey) | [ModuleModel](#jmash-rbac-ModuleModel) | 查询系统模块 |
| createModule | [ModuleCreateReq](#jmash-rbac-ModuleCreateReq) | [ModuleModel](#jmash-rbac-ModuleModel) | 创建实体系统模块 |
| updateModule | [ModuleUpdateReq](#jmash-rbac-ModuleUpdateReq) | [ModuleModel](#jmash-rbac-ModuleModel) | 修改实体系统模块 |
| deleteModule | [ModuleKey](#jmash-rbac-ModuleKey) | [ModuleModel](#jmash-rbac-ModuleModel) | 删除系统模块 |
| batchDeleteModule | [ModuleKeyList](#jmash-rbac-ModuleKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除系统模块 |
| moveModule | [ModuleMoveKey](#jmash-rbac-ModuleMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移模块 |
| checkModuleCode | [ModuleCheck](#jmash-rbac-ModuleCheck) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 模块编码校验 |
| findOperationPage | [OperationReq](#jmash-rbac-OperationReq) | [OperationPage](#jmash-rbac-OperationPage) | 查询翻页信息操作表 |
| findOperationList | [OperationReq](#jmash-rbac-OperationReq) | [OperationList](#jmash-rbac-OperationList) | 查询列表信息操作表 |
| findOperationById | [OperationKey](#jmash-rbac-OperationKey) | [OperationModel](#jmash-rbac-OperationModel) | 查询操作表 |
| createOperation | [OperationCreateReq](#jmash-rbac-OperationCreateReq) | [OperationModel](#jmash-rbac-OperationModel) | 创建实体操作表 |
| updateOperation | [OperationUpdateReq](#jmash-rbac-OperationUpdateReq) | [OperationModel](#jmash-rbac-OperationModel) | 修改实体操作表 |
| deleteOperation | [OperationKey](#jmash-rbac-OperationKey) | [OperationModel](#jmash-rbac-OperationModel) | 删除操作表 |
| batchDeleteOperation | [OperationKeyList](#jmash-rbac-OperationKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除操作表 |
| moveOperation | [OperationMoveKey](#jmash-rbac-OperationMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移操作 |
| checkOperationCode | [OperationCheck](#jmash-rbac-OperationCheck) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 操作编码校验 |
| findDeptList | [DeptReq](#jmash-rbac-DeptReq) | [DeptList](#jmash-rbac-DeptList) | 查询列表信息组织机构cc |
| findDeptTreeList | [DeptReq](#jmash-rbac-DeptReq) | [TreeList](#jmash-rbac-TreeList) | 查询树结构列表组织机构 |
| findDeptById | [DeptKey](#jmash-rbac-DeptKey) | [DeptModel](#jmash-rbac-DeptModel) | 查询组织机构 |
| createDept | [DeptCreateReq](#jmash-rbac-DeptCreateReq) | [DeptModel](#jmash-rbac-DeptModel) | 创建实体组织机构 |
| updateDept | [DeptUpdateReq](#jmash-rbac-DeptUpdateReq) | [DeptModel](#jmash-rbac-DeptModel) | 修改实体组织机构 |
| deleteDept | [DeptKey](#jmash-rbac-DeptKey) | [DeptModel](#jmash-rbac-DeptModel) | 删除组织机构 |
| batchDeleteDept | [DeptKeyList](#jmash-rbac-DeptKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除组织机构 |
| moveDept | [DeptMoveKey](#jmash-rbac-DeptMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移部门 |
| lockDept | [EnableDeptReq](#jmash-rbac-EnableDeptReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量启用禁用部门 |
| enableDept | [DeptEnableKey](#jmash-rbac-DeptEnableKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 启用禁用部门 |
| downloadDept | [.jmash.protobuf.TenantReq](#jmash-protobuf-TenantReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载导入模板组织机构 |
| importDept | [DeptImportReq](#jmash-rbac-DeptImportReq) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 导入组织机构 |
| exportDept | [DeptExportReq](#jmash-rbac-DeptExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出组织机构 |
| findLogPage | [LogReq](#jmash-rbac-LogReq) | [LogPage](#jmash-rbac-LogPage) | 查询翻页信息操作日志 |
| findLogList | [LogReq](#jmash-rbac-LogReq) | [LogList](#jmash-rbac-LogList) | 查询列表信息操作日志 |
| findLogById | [LogKey](#jmash-rbac-LogKey) | [LogModel](#jmash-rbac-LogModel) | 查询操作日志 |
| deleteLog | [LogDelReq](#jmash-rbac-LogDelReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 删除安全日志 |
| selectUserInfo | [UserKey](#jmash-rbac-UserKey) | [UserDeptJobInfoRes](#jmash-rbac-UserDeptJobInfoRes) | 查询用户部门岗位信息 |
| exportLog | [LogExportReq](#jmash-rbac-LogExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出操作日志 |
| findOpensList | [OpensReq](#jmash-rbac-OpensReq) | [OpensList](#jmash-rbac-OpensList) | 查询列表信息三方登录 |
| findOpensById | [OpensKey](#jmash-rbac-OpensKey) | [OpensModel](#jmash-rbac-OpensModel) | 查询三方登录 |
| deleteOpens | [OpensKey](#jmash-rbac-OpensKey) | [OpensModel](#jmash-rbac-OpensModel) | 解绑三方登录 |
| findOpensUnionId | [UserOpensReq](#jmash-rbac-UserOpensReq) | [UnionIdList](#jmash-rbac-UnionIdList) | 查询用户开放平台绑定UnionId |
| findOpenAppPage | [OpenAppReq](#jmash-rbac-OpenAppReq) | [OpenAppPage](#jmash-rbac-OpenAppPage) | 查询翻页信息开发平台应用 |
| findOpenAppList | [OpenAppReq](#jmash-rbac-OpenAppReq) | [OpenAppList](#jmash-rbac-OpenAppList) | 查询列表信息开发平台应用 |
| findOpenAppById | [OpenAppKey](#jmash-rbac-OpenAppKey) | [OpenAppModel](#jmash-rbac-OpenAppModel) | 查询开发平台应用 |
| createOpenApp | [OpenAppCreateReq](#jmash-rbac-OpenAppCreateReq) | [OpenAppModel](#jmash-rbac-OpenAppModel) | 创建实体开发平台应用 |
| updateOpenApp | [OpenAppUpdateReq](#jmash-rbac-OpenAppUpdateReq) | [OpenAppModel](#jmash-rbac-OpenAppModel) | 修改实体开发平台应用 |
| deleteOpenApp | [OpenAppKey](#jmash-rbac-OpenAppKey) | [OpenAppModel](#jmash-rbac-OpenAppModel) | 删除开发平台应用 |
| batchDeleteOpenApp | [OpenAppKeyList](#jmash-rbac-OpenAppKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除开发平台应用 |
| findPermPage | [PermReq](#jmash-rbac-PermReq) | [PermPage](#jmash-rbac-PermPage) | 查询翻页信息权限表 |
| findPermList | [PermReq](#jmash-rbac-PermReq) | [PermList](#jmash-rbac-PermList) | 查询列表信息权限表 |
| findResourcePermList | [ResourceReq](#jmash-rbac-ResourceReq) | [ResourcePermList](#jmash-rbac-ResourcePermList) | 查询资源权限新信息 |
| findPermById | [PermKey](#jmash-rbac-PermKey) | [PermModel](#jmash-rbac-PermModel) | 查询权限表 |
| createPerm | [PermCreateReq](#jmash-rbac-PermCreateReq) | [PermModel](#jmash-rbac-PermModel) | 创建实体权限表 |
| updatePerm | [PermUpdateReq](#jmash-rbac-PermUpdateReq) | [PermModel](#jmash-rbac-PermModel) | 修改实体权限表 |
| deletePerm | [PermKey](#jmash-rbac-PermKey) | [PermModel](#jmash-rbac-PermModel) | 删除权限表 |
| batchDeletePerm | [PermKeyList](#jmash-rbac-PermKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除权限表 |
| checkPermCode | [PermCheck](#jmash-rbac-PermCheck) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 权限编码校验 |
| findResourceList | [ResourceReq](#jmash-rbac-ResourceReq) | [ResourceList](#jmash-rbac-ResourceList) | 查询列表信息资源表 |
| findResourceTreeList | [ResourceReq](#jmash-rbac-ResourceReq) | [TreeList](#jmash-rbac-TreeList) | 查询资源树级结构 |
| findResourceById | [ResourceKey](#jmash-rbac-ResourceKey) | [ResourceModel](#jmash-rbac-ResourceModel) | 查询资源表 |
| createResource | [ResourceCreateReq](#jmash-rbac-ResourceCreateReq) | [ResourceModel](#jmash-rbac-ResourceModel) | 创建实体资源表 |
| updateResource | [ResourceUpdateReq](#jmash-rbac-ResourceUpdateReq) | [ResourceModel](#jmash-rbac-ResourceModel) | 修改实体资源表 |
| deleteResource | [ResourceKey](#jmash-rbac-ResourceKey) | [ResourceModel](#jmash-rbac-ResourceModel) | 删除资源表 |
| batchDeleteResource | [ResourceKeyList](#jmash-rbac-ResourceKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除资源表 |
| downloadResource | [ResourceExportReq](#jmash-rbac-ResourceExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 下载导入资源 |
| importResource | [ResourceImportReq](#jmash-rbac-ResourceImportReq) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 导入资源表 |
| exportResource | [ResourceExportReq](#jmash-rbac-ResourceExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出资源表 |
| existResource | [VerifyResourceReq](#jmash-rbac-VerifyResourceReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 校验资源 |
| moveResouce | [ResourceMoveKey](#jmash-rbac-ResourceMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移 |
| findRoleList | [RoleReq](#jmash-rbac-RoleReq) | [RoleList](#jmash-rbac-RoleList) | 查询列表信息角色/职务表 |
| findRoleTreeList | [RoleReq](#jmash-rbac-RoleReq) | [TreeList](#jmash-rbac-TreeList) | 查询列表信息角色/职务表 |
| findRoleById | [RoleKey](#jmash-rbac-RoleKey) | [RoleModel](#jmash-rbac-RoleModel) | 查询角色/职务表 |
| createRole | [RoleCreateReq](#jmash-rbac-RoleCreateReq) | [RoleModel](#jmash-rbac-RoleModel) | 创建实体角色/职务表 |
| updateRole | [RoleUpdateReq](#jmash-rbac-RoleUpdateReq) | [RoleModel](#jmash-rbac-RoleModel) | 修改实体角色/职务表 |
| deleteRole | [RoleKey](#jmash-rbac-RoleKey) | [RoleModel](#jmash-rbac-RoleModel) | 删除角色/职务表 |
| batchDeleteRole | [RoleKeyList](#jmash-rbac-RoleKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除角色/职务表 |
| grantPerm | [RolePermReq](#jmash-rbac-RolePermReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 角色授予权限. |
| revokePerm | [RolePermReq](#jmash-rbac-RolePermReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 取消角色授予权限. |
| moveRole | [RoleMoveKey](#jmash-rbac-RoleMoveKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 上移/下移角色 |
| existRole | [VerifyRoleReq](#jmash-rbac-VerifyRoleReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 校验角色 |
| findUserLogPage | [UserLogReq](#jmash-rbac-UserLogReq) | [UserLogPage](#jmash-rbac-UserLogPage) | 查询翻页信息安全日志 |
| findUserLogList | [UserLogReq](#jmash-rbac-UserLogReq) | [UserLogList](#jmash-rbac-UserLogList) | 查询列表信息安全日志 |
| findUserLogById | [UserLogKey](#jmash-rbac-UserLogKey) | [UserLogModel](#jmash-rbac-UserLogModel) | 查询安全日志 |
| deleteUserLog | [UserLogDelReq](#jmash-rbac-UserLogDelReq) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 删除安全日志 |
| exportUserLog | [UserLogExportReq](#jmash-rbac-UserLogExportReq) | [.google.api.HttpBody](#google-api-HttpBody) stream | 导出安全日志 |
| getUserDeptInfo | [UserKey](#jmash-rbac-UserKey) | [UserDeptJobInfoRes](#jmash-rbac-UserDeptJobInfoRes) | 日志获取用户信息. |
| miniAppLogin | [MiniAppLoginReq](#jmash-rbac-MiniAppLoginReq) | [MiniAppLoginResp](#jmash-rbac-MiniAppLoginResp) | 小程序授权登录 |
| miniAppPhoneNumber | [MiniAppPhoneNumberReq](#jmash-rbac-MiniAppPhoneNumberReq) | [MiniAppLoginResp](#jmash-rbac-MiniAppLoginResp) | 获取微信小程序手机号 |
| miniAppBindPhone | [MiniAppBindPhoneReq](#jmash-rbac-MiniAppBindPhoneReq) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 已登录状态小程序更换绑定手机号 |
| loginOrgan | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [TokenResp](#jmash-rbac-TokenResp) | 登录组织，用户Token换组织用户Token，参数：组织token值 organID@e01. |
| createOrganUser | [OrganUserCreateReq](#jmash-rbac-OrganUserCreateReq) | [UserModel](#jmash-rbac-UserModel) | 创建组织用户. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |



set cur_cd=%cd%

cd %~dp0

:: 初始化go mod 

go mod init jmash-rbac-sample-gateway

:: go get gitee.com/jmash/rbac/jmash-rbac/trunk/jmash-rbac-gateway@master
:: go get gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway@master

:: 更新mod 

go mod tidy

:: 拷贝目录
@echo off
for /f "tokens=1,2 delims= " %%i in ('go list -m  gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway') do ( 
    set a=%%i
    set b=%%j
)
set a=%a:/=\%
@echo on
xcopy %userprofile%\go\pkg\mod\%a%@%b%\openapi  openapi\ /s /e /y

@echo off
for /f "tokens=1,2 delims= " %%i in ('go list -m  gitee.com/jmash/rbac/jmash-rbac/trunk/jmash-rbac-gateway') do ( 
    set a=%%i
    set b=%%j
)
set a=%a:/=\%
@echo on
xcopy %userprofile%\go\pkg\mod\%a%@%b%\openapi  openapi\ /s /e /y


:: copy 
xcopy openapi bin\openapi\ /s /e /y
xcopy openapi-ui bin\openapi-ui\ /s /e /y

:: build 

SET GOOS=windows
SET GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

SET CGO_ENABLED=0
SET GOOS=linux
SET GOARCH=amd64

go build -o ./bin/main_x86 ./src/main.go

go build -o ./bin/main_amd64 ./src/main.go

export GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go

cd %cur_cd%

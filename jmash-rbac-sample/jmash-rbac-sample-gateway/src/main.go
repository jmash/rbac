package main

import (
	"context"
	"log"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	 "gitee.com/jmash/jmash/trunk/jmash-core-gateway/src/jmash"
	 rbacservice     "gitee.com/jmash/rbac/jmash-rbac/trunk/jmash-rbac-gateway/src/jmash/rbac"
	 captchaservice  "gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway/src/jmash/captcha"
	 basicservice    "gitee.com/jmash/jmash/trunk/jmash-core-gateway/src/jmash/basic"
	 fileservice     "gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway/src/jmash/file"
	 smsservice      "gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway/src/jmash/sms"
	 dictservice     "gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway/src/jmash/dict"
	 regionservice   "gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway/src/jmash/region"
)


func main() {
	
	log.Println("gateway wait grpc 50051 ...  !")
	// Create a client connection to the gRPC server we just started
	// This is where the gRPC-Gateway proxies the requests
	conn, err := grpc.DialContext(
		context.Background(),
		"0.0.0.0:50051",
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatalln("Failed to dial server:", err)
	}
    
	//FileServeMuxOption 去掉换行符.
	gwmux := runtime.NewServeMux(jmash.FileServeMuxOption())
    
        // Register Captcha
	err = captchaservice.RegisterCaptchaHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register Captcha gateway:", err)
	}
	
	// Register RABC
	err = rbacservice.RegisterRbacHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register RABC gateway:", err)
	}
	
	// Register SMS
	err = smsservice.RegisterSmsHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register SMS gateway:", err)
	}
	
	// Register Dict
	err = dictservice.RegisterDictHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register Dict gateway:", err)
	}
	
	// Register FileBasic
	err = basicservice.RegisterFileBasicHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}
	
	// Register File
	err = fileservice.RegisterFileHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}
	
	// Register 
	err = regionservice.RegisterRegionHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register gateway:", err)
	}
	
	log.Println("Register Grpc File Upload Starting !")
	err =jmash.RegisterFileUploadHandler(context.Background(), gwmux, conn)
	if err != nil {
		log.Fatalln("Failed to register Grpc file Upload gateway:", err)
	}
	
	log.Println("OpenApi Starting !")
	// Serve the Grpc GateWay And OpenApi,
	mux := http.NewServeMux()
	mux.Handle("/", gwmux)
	
	apifs := http.FileServer(http.Dir("openapi"))
	mux.Handle("/openapi/", http.StripPrefix("/openapi", apifs))
	
	uifs := http.FileServer(http.Dir("openapi-ui"))
	mux.Handle("/openapi-ui/", http.StripPrefix("/openapi-ui", uifs))
	
	log.Println("Serving gRPC-Gateway on http://0.0.0.0:8090")
	
	handler := jmash.AllowCORS(mux)
	
	err = http.ListenAndServe(":8090", handler)
	if err != nil {
		log.Fatal(err)
	}
}

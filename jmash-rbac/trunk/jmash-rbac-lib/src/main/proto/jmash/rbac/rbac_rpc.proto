syntax = "proto3";

package jmash.rbac;

option go_package = "jmash/rbac;rbac";
option java_multiple_files = true;
option java_package = "jmash.rbac";
option java_outer_classname = "RbacProto";

import  "google/api/annotations.proto";
import  "google/api/httpbody.proto";
import  "google/protobuf/wrappers.proto";
import  "google/protobuf/empty.proto";
import  "protoc-gen-openapiv2/options/annotations.proto";

import  "jmash/protobuf/basic.proto";
import  "jmash/protobuf/file_basic.proto";

import  "jmash/rbac/protobuf/rbac_dept_message.proto";
import  "jmash/rbac/protobuf/rbac_log_message.proto";
import  "jmash/rbac/protobuf/rbac_module_message.proto";
import  "jmash/rbac/protobuf/rbac_opens_message.proto";
import  "jmash/rbac/protobuf/rbac_open_app_message.proto";
import  "jmash/rbac/protobuf/rbac_operation_message.proto";
import  "jmash/rbac/protobuf/rbac_perm_message.proto";
import  "jmash/rbac/protobuf/rbac_resource_message.proto";
import  "jmash/rbac/protobuf/rbac_role_message.proto";
import  "jmash/rbac/protobuf/rbac_user_message.proto";
import  "jmash/rbac/protobuf/rbac_user_log_message.proto";
import  "jmash/rbac/protobuf/rbac_auth_message.proto";
import  "jmash/rbac/protobuf/miniapp_message.proto";

option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
  info: {
    title: "基于角色的用户授权管理模块";
    version: "v1.0.0";
  };
};
//Rbac Service
service Rbac {

  //版本
  rpc version(google.protobuf.Empty) returns (google.protobuf.StringValue){
    option (google.api.http) = {
      get: "/v1/rbac/version"
    };
  }

  // 枚举值列表
  rpc findEnumList(google.protobuf.StringValue) returns (jmash.protobuf.EnumValueList){}

  // 枚举值Map
  rpc findEnumMap(google.protobuf.StringValue) returns (jmash.protobuf.CustomEnumValueMap){}

  // 枚举值
  rpc findEnumEntry(jmash.protobuf.EnumEntryReq) returns (jmash.protobuf.EntryList){
    option (google.api.http) = {
      get: "/v1/rbac/enum/entry"
    };
  }

  // 用户登录
  rpc login(LoginReq) returns (TokenResp){
    option (google.api.http) = {
      post: "/v1/rbac/auth/login"
      body: "*"
    };
  }

  // 二维码扫码登录.
  rpc loginByQrcode(LoginQrcodeReq) returns (TokenResp){
  	option (google.api.http) = {
      post: "/v1/rbac/auth/login_qrcode"
      body: "*"
    };
  }

  //发送验证码.
  rpc sendValidCode(SendValidCodeReq) returns (google.protobuf.BoolValue) {
    option (google.api.http) = {
      post: "/v1/rbac/auth/sendcode"
      body: "*"
    };
  }

  //通过验证码登录
  rpc loginByValidCode(ValidCodeLoginReq) returns (TokenResp) {
    option (google.api.http) = {
      post: "/v1/rbac/auth/logincode"
      body: "*"
    };
  }

  //登录选择动态互斥角色,无需权限.
  rpc selectDsdRoles(jmash.protobuf.TenantReq) returns(DsdRoleListResp){
    option (google.api.http) = {
      get: "/v1/rbac/auth/select_dsd_roles"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:false}}
    };
  }

  // 用户登出.
  rpc logout(LogoutReq) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      delete: "/v1/rbac/auth/logout"
    };
  }

  // 刷新Token
  rpc refreshToken(RefreshTokenReq) returns (TokenResp) {
    option (google.api.http) = {
      post: "/v1/rbac/auth/refresh_token"
      body: "*"
      additional_bindings {
        post: "/v1/front/rbac/auth/refresh_token"
        body: "*"
      }
    };
  }

  // 获取当前会话用户(含DSD动态职责分离).
  rpc userInfo(jmash.protobuf.TenantReq) returns (UserModel){
    option (google.api.http) = {
      get: "/v1/rbac/auth/user"
      additional_bindings {get: "/v1/front/rbac/auth/user"}
    };
  }

  // 个人中心修改个人信息
  rpc updateUserInfo(UpdateUserReq) returns (UserModel) {
    option (google.api.http) = {
      patch: "/v1/rbac/auth/user",
      body: "*"
    };
  }

  // 获取当前会话角色列表和权限列表.
  rpc userRolesPerms(jmash.protobuf.TenantReq) returns (RolePermSet){
    option (google.api.http) = {
      get: "/v1/rbac/auth/roles_perms"
    };
  }

  // 获取当前会话用户菜单
  rpc userMenus(jmash.protobuf.TenantReq) returns (MenuList) {
    option (google.api.http) = {
      get: "/v1/rbac/auth/menus"
    };
  }

  // 修改密码
  rpc changePwd(ChangePwdReq) returns (google.protobuf.BoolValue) {
    option (google.api.http) = {
      patch: "/v1/rbac/auth/change_pwd"
      body: "*"
    };
  }

  //重新绑定手机号/邮箱
  rpc bindPhoneEmail(BindPhoneEmailReq) returns (google.protobuf.BoolValue){
   option (google.api.http) = {
      post: "/v1/rbac/auth/bind_phone_email"
      body: "*"
    };
  }

  //切换用户身份
  rpc runAsUser(RunAsReq) returns (UserModel) {
    option (google.api.http) = {
      post: "/v1/rbac/auth/run_as"
      body: "*"
    };
  }

  //默认可切换用户
  rpc allowRunAsUser(jmash.protobuf.TenantReq) returns (UserList) {
    option (google.api.http) = {
      get: "/v1/rbac/auth/allow_run_as_user"
    };
  }

  // 查询翻页信息用户
  rpc findUserPage(UserReq) returns (UserPage) {
    option (google.api.http) = {
      get: "/v1/rbac/user/page"
    };
  }
  // 查询列表信息用户
  rpc findUserList(UserReq) returns (UserList) {
    option (google.api.http) = {
      get: "/v1/rbac/user/list"
    };
  }
  // 查询用户
  rpc findUserById(UserKey) returns (UserModel){
    option (google.api.http) = {
      get: "/v1/rbac/user/id"
    };
  }
  // 根据用户名查询用户
  rpc findUserByName(UserNameReq) returns (UserModel){
    option (google.api.http) = {
      get: "/v1/rbac/user/name"
    };
  }
  // 查询用户列表
  rpc findUserInfoList(UserReq) returns (UserInfoList){
    option (google.api.http) = {
      get: "/v1/rbac/user/list_user"
    };
  }
  // 查询用户分页
  rpc findUserInfoPage(UserReq) returns (UserInfoPage){
    option (google.api.http) = {
      get: "/v1/rbac/user/page_user"
    };
  }
  // 创建实体用户
  rpc createUser(UserCreateReq) returns(UserModel){
    option (google.api.http) = {
      post: "/v1/rbac/user"
      body: "*"
    };
  }
  // 修改实体用户
  rpc updateUser(UserUpdateReq) returns(UserModel){
    option (google.api.http) = {
      patch: "/v1/rbac/user"
      body: "*"
    };
  }
  // 删除用户
  rpc deleteUser(UserKey) returns(UserModel){
    option (google.api.http) = {
      delete: "/v1/rbac/user/id"
    };
  }
  // 批量删除用户
  rpc batchDeleteUser(UserKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/user/batch"
      body: "*"
    };
  }

  //校验邮箱/手机号/用户名/是否存在
  rpc existUser(VerifyUserReq) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/user/exist"
    };
  }

  //批量启用禁用用户
  rpc batchEnableUser(EnableUserReq) returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      put: "/v1/rbac/user/batch_enable"
      body: "*"
    };
  }

  //启用禁用用户
  rpc enableUser(UserEnableKey) returns (google.protobuf.BoolValue) {
    option (google.api.http) = {
      put: "/v1/rbac/user/enable"
      body: "*"
    };
  }

  //锁定/解锁用户
  rpc lockUser(LockUserReq)  returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      put: "/v1/rbac/user/lock"
      body: "*"
    };
  }

  //审核/取消审核用户
  rpc approvedUser(ApprovedUserReq)  returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      put: "/v1/rbac/user/approved"
      body: "*"
    };
  }

  //给用户分配角色
  rpc assignUser(UserRoleReq) returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      patch: "/v1/rbac/user/assign"
      body: "*"
    };
  }

  //取消用户分配角色
  rpc deassignUser(UserRoleReq) returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      patch: "/v1/rbac/user/deassign"
      body: "*"
    };
  }


  //后台用户重置密码
  rpc resetPwd(UserResetPwdReq)  returns (google.protobuf.BoolValue) {
    option (google.api.http) = {
      put: "/v1/rbac/user/resetpwd"
      body: "*"
    };
  }

  // 导出用户
  rpc exportUser(UserExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/user/{tenant}/users.xlsx"
    };
  }

  // 打印用户
  rpc printUser(UserExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/user/{tenant}/users.pdf"
    };
  }

  // 下载导入模板用户
  rpc downloadUserTemplate(jmash.protobuf.TenantReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/user/template.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  // 导入用户
  rpc importUser(UserImportReq) returns (google.protobuf.StringValue) {
    option (google.api.http) = {
      patch: "/v1/rbac/user/import"
      body: "*"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }
  //查询目录ID
  rpc selectDirectoryIds(jmash.protobuf.TenantReq) returns (DirectoryListResp){
    option (google.api.http) = {
      get: "/v1/rbac/user/select_directory_id"
    };
  }

  // 查询翻页信息系统模块
  rpc findModulePage(ModuleReq) returns (ModulePage) {
    option (google.api.http) = {
      get: "/v1/rbac/module/page"
    };
  }
  // 查询列表信息系统模块
  rpc findModuleList(ModuleReq) returns (ModuleList) {
    option (google.api.http) = {
      get: "/v1/rbac/module/list"
    };
  }
  // 查询系统模块
  rpc findModuleById(ModuleKey) returns (ModuleModel){
    option (google.api.http) = {
      get: "/v1/rbac/module/id"
    };
  }
  // 创建实体系统模块
  rpc createModule(ModuleCreateReq) returns(ModuleModel){
    option (google.api.http) = {
      post: "/v1/rbac/module"
      body: "*"
    };
  }
  // 修改实体系统模块
  rpc updateModule(ModuleUpdateReq) returns(ModuleModel){
    option (google.api.http) = {
      patch: "/v1/rbac/module"
      body: "*"
    };
  }
  // 删除系统模块
  rpc deleteModule(ModuleKey) returns(ModuleModel){
    option (google.api.http) = {
      delete: "/v1/rbac/module/id"
    };
  }
  // 批量删除系统模块
  rpc batchDeleteModule(ModuleKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/module/batch"
      body: "*"
    };
  }
  // 上移/下移模块
  rpc moveModule(ModuleMoveKey) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      put: "/v1/rbac/module/move"
      body: "*"
    };
  }

  // 模块编码校验
  rpc checkModuleCode(ModuleCheck) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/module/check"
    };
  }

  // 查询翻页信息操作表
  rpc findOperationPage(OperationReq) returns (OperationPage) {
    option (google.api.http) = {
      get: "/v1/rbac/operation/page"
    };
  }
  // 查询列表信息操作表
  rpc findOperationList(OperationReq) returns (OperationList) {
    option (google.api.http) = {
      get: "/v1/rbac/operation/list"
    };
  }
  // 查询操作表
  rpc findOperationById(OperationKey) returns (OperationModel){
    option (google.api.http) = {
      get: "/v1/rbac/operation/id"
    };
  }
  // 创建实体操作表
  rpc createOperation(OperationCreateReq) returns(OperationModel){
    option (google.api.http) = {
      post: "/v1/rbac/operation"
      body: "*"
    };
  }
  // 修改实体操作表
  rpc updateOperation(OperationUpdateReq) returns(OperationModel){
    option (google.api.http) = {
      patch: "/v1/rbac/operation"
      body: "*"
    };
  }
  // 删除操作表
  rpc deleteOperation(OperationKey) returns(OperationModel){
    option (google.api.http) = {
      delete: "/v1/rbac/operation/id"
    };
  }
  // 批量删除操作表
  rpc batchDeleteOperation(OperationKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/operation/batch"
      body: "*"
    };
  }
  // 上移/下移操作
  rpc moveOperation(OperationMoveKey) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      put: "/v1/rbac/operation/move"
      body: "*"
    };
  }

  // 操作编码校验
  rpc checkOperationCode(OperationCheck) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/operation/check"
    };
  }

  // 查询列表信息组织机构cc
  rpc findDeptList(DeptReq) returns (DeptList) {
    option (google.api.http) = {
      get: "/v1/rbac/dept/list"
    };
  }
  // 查询树结构列表组织机构
  rpc findDeptTreeList(DeptReq) returns (TreeList) {
    option (google.api.http) = {
      get: "/v1/rbac/dept/treelist"
      additional_bindings {get: "/v1/front/rbac/dept/treelist"}
    };
  }
  // 查询组织机构
  rpc findDeptById(DeptKey) returns (DeptModel){
    option (google.api.http) = {
      get: "/v1/rbac/dept/id"
    };
  }
  // 创建实体组织机构
  rpc createDept(DeptCreateReq) returns(DeptModel){
    option (google.api.http) = {
      post: "/v1/rbac/dept"
      body: "*"
    };
  }
  // 修改实体组织机构
  rpc updateDept(DeptUpdateReq) returns(DeptModel){
    option (google.api.http) = {
      patch: "/v1/rbac/dept"
      body: "*"
    };
  }
  // 删除组织机构
  rpc deleteDept(DeptKey) returns(DeptModel){
    option (google.api.http) = {
      delete: "/v1/rbac/dept/id"
    };
  }
  // 批量删除组织机构
  rpc batchDeleteDept(DeptKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/dept/batch"
      body: "*"
    };
  }
  // 上移/下移部门
  rpc moveDept(DeptMoveKey) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      put: "/v1/rbac/dept/move"
      body: "*"
    };
  }

  // 批量启用禁用部门
  rpc lockDept(EnableDeptReq) returns (google.protobuf.Int32Value) {
    option (google.api.http) = {
      put: "/v1/rbac/dept/lock"
      body: "*"
    };
  }

  // 启用禁用部门
  rpc enableDept(DeptEnableKey) returns (google.protobuf.BoolValue) {
    option (google.api.http) = {
      put: "/v1/rbac/dept/enable"
      body: "*"
    };
  }

  // 下载导入模板组织机构
  rpc downloadDept(jmash.protobuf.TenantReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/dept/template.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }
  // 导入组织机构
  rpc importDept(DeptImportReq) returns (google.protobuf.StringValue) {
    option (google.api.http) = {
      patch: "/v1/rbac/dept/import"
      body: "*"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }
  // 导出组织机构
  rpc exportDept(DeptExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/dept/{tenant}/depts.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  // 查询翻页信息操作日志
  rpc findLogPage(LogReq) returns (LogPage) {
    option (google.api.http) = {
      get: "/v1/rbac/log/page"
    };
  }
  // 查询列表信息操作日志
  rpc findLogList(LogReq) returns (LogList) {
    option (google.api.http) = {
      get: "/v1/rbac/log/list"
    };
  }
  // 查询操作日志
  rpc findLogById(LogKey) returns (LogModel){
    option (google.api.http) = {
      get: "/v1/rbac/log/id"
    };
  }

  // 删除安全日志
  rpc deleteLog(LogDelReq) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      delete: "/v1/rbac/log/batch"
    };
  }

  // 查询用户部门岗位信息
  rpc selectUserInfo(UserKey) returns (UserDeptJobInfoRes){
    option (google.api.http) = {
      get: "/v1/rbac/log/user"
    };
  }

  // 导出操作日志
  rpc exportLog(LogExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/log/{tenant}/logs.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }


  // 查询列表信息三方登录
  rpc findOpensList(OpensReq) returns (OpensList) {
    option (google.api.http) = {
      get: "/v1/rbac/opens/list"
    };
  }

  // 查询三方登录
  rpc findOpensById(OpensKey) returns (OpensModel){
    option (google.api.http) = {
      get: "/v1/rbac/opens/id"
    };
  }

  // 解绑三方登录
  rpc deleteOpens(OpensKey) returns(OpensModel){
    option (google.api.http) = {
      delete: "/v1/rbac/opens/id"
    };
  }

  //查询用户开放平台绑定UnionId
  rpc findOpensUnionId(UserOpensReq) returns (UnionIdList){}

  // 查询翻页信息开发平台应用
  rpc findOpenAppPage(OpenAppReq) returns (OpenAppPage) {
    option (google.api.http) = {
      get: "/v1/rbac/open_app/page/{tenant}"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  // 查询列表信息开发平台应用
  rpc findOpenAppList(OpenAppReq) returns (OpenAppList) {
    option (google.api.http) = {
      get: "/v1/rbac/open_app/list/{tenant}"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  // 查询开发平台应用
  rpc findOpenAppById(OpenAppKey) returns (OpenAppModel){
    option (google.api.http) = {
      get: "/v1/rbac/open_app/id/{tenant}/{open_app_id}"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  // 创建实体开发平台应用
  rpc createOpenApp(OpenAppCreateReq) returns(OpenAppModel){
    option (google.api.http) = {
      post: "/v1/rbac/open_app"
      body: "*"
    };
  }

  // 修改实体开发平台应用
  rpc updateOpenApp(OpenAppUpdateReq) returns(OpenAppModel){
    option (google.api.http) = {
      patch: "/v1/rbac/open_app"
      body: "*"
    };
  }

  // 删除开发平台应用
  rpc deleteOpenApp(OpenAppKey) returns(OpenAppModel){
    option (google.api.http) = {
      delete: "/v1/rbac/open_app/id"
    };
  }

  // 批量删除开发平台应用
  rpc batchDeleteOpenApp(OpenAppKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/open_app/batch"
      body: "*"
    };
  }

  // 查询翻页信息权限表
  rpc findPermPage(PermReq) returns (PermPage) {
    option (google.api.http) = {
      get: "/v1/rbac/perm/page"
    };
  }
  // 查询列表信息权限表
  rpc findPermList(PermReq) returns (PermList) {
    option (google.api.http) = {
      get: "/v1/rbac/perm/list"
    };
  }
  //查询资源权限新信息
  rpc findResourcePermList(ResourceReq) returns (ResourcePermList) {
    option (google.api.http) = {
      get: "/v1/rbac/perm/permlist"
    };
  }
  // 查询权限表
  rpc findPermById(PermKey) returns (PermModel){
    option (google.api.http) = {
      get: "/v1/rbac/perm/id"
    };
  }
  // 创建实体权限表
  rpc createPerm(PermCreateReq) returns(PermModel){
    option (google.api.http) = {
      post: "/v1/rbac/perm"
      body: "*"
    };
  }
  // 修改实体权限表
  rpc updatePerm(PermUpdateReq) returns(PermModel){
    option (google.api.http) = {
      patch: "/v1/rbac/perm"
      body: "*"
    };
  }
  // 删除权限表
  rpc deletePerm(PermKey) returns(PermModel){
    option (google.api.http) = {
      delete: "/v1/rbac/perm/id"
    };
  }
  // 批量删除权限表
  rpc batchDeletePerm(PermKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/perm/batch"
      body: "*"
    };
  }

  // 权限编码校验
  rpc checkPermCode(PermCheck) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/perm/check"
    };
  }

  // 查询列表信息资源表
  rpc findResourceList(ResourceReq) returns (ResourceList) {
    option (google.api.http) = {
      get: "/v1/rbac/resource/list"
    };
  }

  // 查询资源树级结构
  rpc findResourceTreeList(ResourceReq) returns (TreeList) {
    option (google.api.http) = {
      get: "/v1/rbac/resource/treelist"
    };
  }

  // 查询资源表
  rpc findResourceById(ResourceKey) returns (ResourceModel){
    option (google.api.http) = {
      get: "/v1/rbac/resource/id"
    };
  }
  // 创建实体资源表
  rpc createResource(ResourceCreateReq) returns(ResourceModel){
    option (google.api.http) = {
      post: "/v1/rbac/resource"
      body: "*"
    };
  }
  // 修改实体资源表
  rpc updateResource(ResourceUpdateReq) returns(ResourceModel){
    option (google.api.http) = {
      patch: "/v1/rbac/resource"
      body: "*"
    };
  }
  // 删除资源表
  rpc deleteResource(ResourceKey) returns(ResourceModel){
    option (google.api.http) = {
      delete: "/v1/rbac/resource/id"
    };
  }
  // 批量删除资源表
  rpc batchDeleteResource(ResourceKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/resource/batch"
      body: "*"
    };
  }

  // 下载导入资源
  rpc downloadResource(ResourceExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/resource/template.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }
  // 导入资源表
  rpc importResource(ResourceImportReq) returns (google.protobuf.StringValue) {
    option (google.api.http) = {
      patch: "/v1/rbac/resource/import"
      body: "*"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }
  // 导出资源表
  rpc exportResource(ResourceExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/resource/{tenant}/resources.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  //校验资源
  rpc existResource(VerifyResourceReq) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/resource/exist"
    };
  }

  //上移/下移
  rpc moveResouce(ResourceMoveKey) returns (google.protobuf.BoolValue){
    option (google.api.http) = {
      put: "/v1/rbac/resource/move"
      body: "*"
    };
  }

  // 查询列表信息角色/职务表
  rpc findRoleList(RoleReq) returns (RoleList) {
    option (google.api.http) = {
      get: "/v1/rbac/role/list"
    };
  }
  // 查询列表信息角色/职务表
  rpc findRoleTreeList(RoleReq) returns (TreeList) {
    option (google.api.http) = {
      get: "/v1/rbac/role/treelist"
    };
  }
  // 查询角色/职务表
  rpc findRoleById(RoleKey) returns (RoleModel){
    option (google.api.http) = {
      get: "/v1/rbac/role/id"
    };
  }
  // 创建实体角色/职务表
  rpc createRole(RoleCreateReq) returns(RoleModel){
    option (google.api.http) = {
      post: "/v1/rbac/role"
      body: "*"
    };
  }
  // 修改实体角色/职务表
  rpc updateRole(RoleUpdateReq) returns(RoleModel){
    option (google.api.http) = {
      patch: "/v1/rbac/role"
      body: "*"
    };
  }
  // 删除角色/职务表
  rpc deleteRole(RoleKey) returns(RoleModel){
    option (google.api.http) = {
      delete: "/v1/rbac/role/id"
    };
  }
  // 批量删除角色/职务表
  rpc batchDeleteRole(RoleKeyList) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      put: "/v1/rbac/role/batch"
      body: "*"
    };
  }

  //角色授予权限.
  rpc grantPerm(RolePermReq)  returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      patch: "/v1/rbac/role/grant"
      body: "*"
    };
  }

  //取消角色授予权限.
  rpc revokePerm(RolePermReq) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      patch: "/v1/rbac/role/revoke"
      body: "*"
    };
  }

  //上移/下移角色
  rpc moveRole(RoleMoveKey) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      put: "/v1/rbac/role/move"
      body: "*"
    };
  }

  //校验角色
  rpc existRole(VerifyRoleReq) returns(google.protobuf.BoolValue){
    option (google.api.http) = {
      get: "/v1/rbac/role/exist"
    };
  }

  // 查询翻页信息安全日志
  rpc findUserLogPage(UserLogReq) returns (UserLogPage) {
    option (google.api.http) = {
      get: "/v1/rbac/userLog/page"
    };
  }
  // 查询列表信息安全日志
  rpc findUserLogList(UserLogReq) returns (UserLogList) {
    option (google.api.http) = {
      get: "/v1/rbac/userLog/list"
    };
  }
  // 查询安全日志
  rpc findUserLogById(UserLogKey) returns (UserLogModel){
    option (google.api.http) = {
      get: "/v1/rbac/userLog/id"
    };
  }

  // 删除安全日志
  rpc deleteUserLog(UserLogDelReq) returns(google.protobuf.Int32Value){
    option (google.api.http) = {
      delete: "/v1/rbac/userLog/batch"
    };
  }

  // 导出安全日志
  rpc exportUserLog(UserLogExportReq) returns (stream google.api.HttpBody) {
    option (google.api.http) = {
      get: "/v1/rbac/userLog/{tenant}/userLogs.xlsx"
    };
    option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
      parameters: {headers: {type: 1; name : "Authorization"; required:true}}
    };
  }

  //日志获取用户信息.
  rpc getUserDeptInfo(UserKey) returns (UserDeptJobInfoRes) {
    option (google.api.http) = {
      get: "/v1/rbac/userLog/user"
    };
  }

  //小程序授权登录
  rpc miniAppLogin(MiniAppLoginReq) returns (MiniAppLoginResp){
    option (google.api.http) = {
      post: "/v1/front/rbac/miniapp_login/{tenant}"
      body: "*"
    };
  }

  //获取微信小程序手机号
  rpc miniAppPhoneNumber(MiniAppPhoneNumberReq) returns (MiniAppLoginResp){
    option (google.api.http) = {
      post: "/v1/front/rbac/miniapp_phonenumber/{tenant}"
      body: "*"
    };
  }

  //已登录状态小程序更换绑定手机号
  rpc miniAppBindPhone(MiniAppBindPhoneReq) returns (google.protobuf.BoolValue){
  	 option (google.api.http) = {
      post: "/v1/front/rbac/miniapp_bindphone"
      body: "*"
    };
  }

  //登录组织，用户Token换组织用户Token，参数：组织token值 organID@e01.
  rpc loginOrgan(google.protobuf.StringValue) returns (TokenResp){
     option (google.api.http) = {
        post: "/v1/rbac/login_organ"
        body: "*"
     };
  }

  //创建组织用户.
  rpc createOrganUser(OrganUserCreateReq) returns (UserModel){}

}

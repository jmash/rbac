package com.gitee.jmash.rbac.client.token;

import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperties;


/**
 * 客户端认证配置.
 *
 * @author cgd
 */
@ConfigProperties(prefix = "jmash.client.auth")
@ApplicationScoped
public class ClientAuthProps {

  private String username;

  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}

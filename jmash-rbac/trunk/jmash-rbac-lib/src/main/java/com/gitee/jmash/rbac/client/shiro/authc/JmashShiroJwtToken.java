package com.gitee.jmash.rbac.client.shiro.authc;

import org.apache.shiro.authc.AuthenticationToken;


public class JmashShiroJwtToken implements AuthenticationToken {

  private static final long serialVersionUID = 1L;

  private String authorization;

  public JmashShiroJwtToken(String authorization) {
    super();
    this.authorization = authorization;
  }

  @Override
  public Object getPrincipal() {
    return null;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  public String getAuthorization() {
    return authorization;
  }

  public void setAuthorization(String authorization) {
    this.authorization = authorization;
  }

}


package com.gitee.jmash.rbac.client;

import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.rbac.client.token.SystemAccessToken;
import jmash.rbac.RbacGrpc;

/** Rbac Client . */
public class RbacClient {

  /** RbacBlockingStub. */
  public static RbacGrpc.RbacBlockingStub getRbacBlockingStub() {
    return RbacGrpc.newBlockingStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** RbacBlockingStub. */
  public static RbacGrpc.RbacBlockingStub getRbacBlockingStub(String tenant) {
    return RbacGrpc.newBlockingStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials(tenant));
  }

  /** RbacFutureStub. */
  public static RbacGrpc.RbacFutureStub getRbacFutureStub() {
    return RbacGrpc.newFutureStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** RbacFutureStub. */
  public static RbacGrpc.RbacFutureStub getRbacFutureStub(String tenant) {
    return RbacGrpc.newFutureStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials(tenant));
  }

  /** RbacStub. */
  public static RbacGrpc.RbacStub getRbacStub() {
    return RbacGrpc.newStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials());
  }

  /** RbacStub. */
  public static RbacGrpc.RbacStub getRbacStub(String tenant) {
    return RbacGrpc.newStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(GrpcChannel.getCallCredentials(tenant));
  }

  /** 获取系统访问凭证. */
  public static OAuth2Credentials getSystemCredentials(String tenant) {
    String accessToken = SystemAccessToken.getAccessToken(tenant);
    return new OAuth2Credentials(accessToken);
  }

  /** 获取系统访问凭证. */
  public static OAuth2Credentials getSystemCredentials(String loginTenant, String accessTenant) {
    String accessToken = SystemAccessToken.getAccessToken(loginTenant);
    return new OAuth2Credentials(accessToken, accessTenant);
  }

  /** RbacBlockingStub. */
  public static RbacGrpc.RbacBlockingStub getRbacSystemBlockingStub(String tenant) {
    return RbacGrpc.newBlockingStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(tenant));
  }

  /** RbacBlockingStub. */
  public static RbacGrpc.RbacBlockingStub getRbacSystemBlockingStub(String loginTenant,
      String accessTenant) {
    return RbacGrpc.newBlockingStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(loginTenant, accessTenant));
  }

  /** RbacFutureStub. */
  public static RbacGrpc.RbacFutureStub getRbacSystemFutureStub(String tenant) {
    return RbacGrpc.newFutureStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(tenant));
  }

  /** RbacFutureStub. */
  public static RbacGrpc.RbacFutureStub getRbacSystemFutureStub(String loginTenant,
      String accessTenant) {
    return RbacGrpc.newFutureStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(loginTenant, accessTenant));
  }

  /** RbacStub. */
  public static RbacGrpc.RbacStub getRbacSystemStub(String tenant) {
    return RbacGrpc.newStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(tenant));
  }

  /** RbacStub. */
  public static RbacGrpc.RbacStub getRbacSystemStub(String loginTenant, String accessTenant) {
    return RbacGrpc.newStub(RbacClientConfig.getManagedChannel())
        .withCallCredentials(getSystemCredentials(loginTenant, accessTenant));
  }

}

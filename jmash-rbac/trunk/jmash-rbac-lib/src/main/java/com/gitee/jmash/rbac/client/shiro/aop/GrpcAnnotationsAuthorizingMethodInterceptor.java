package com.gitee.jmash.rbac.client.shiro.aop;

import org.apache.shiro.authz.aop.AnnotationsAuthorizingMethodInterceptor;

/**
 * Shiro 注解拦截.
 * @author cgd
 */
public class GrpcAnnotationsAuthorizingMethodInterceptor extends AnnotationsAuthorizingMethodInterceptor{

}

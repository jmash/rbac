
package com.gitee.jmash.rbac.entity;

import com.gitee.jmash.rbac.entity.RolesDutyEntity.DutyPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;
import jmash.rbac.protobuf.DutyType;


/**
 * 本代码为自动生成工具生成 职责分离表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_roles_duty")
@IdClass(DutyPk.class)
public class RolesDutyEntity  implements Serializable  {

  private static final long serialVersionUID = 1L;
  
  /** 职责1. */
  private UUID srcRoleId= UUID.randomUUID()  ;
  /** 职责2. */
  private UUID descRoleId= UUID.randomUUID()  ;
  /** 职责分离类型. */
  private DutyType dutyType ;

  /**
  * 默认构造函数.
  */
  public RolesDutyEntity(){
    super();
  }

  public RolesDutyEntity(UUID srcRoleId,UUID descRoleId,DutyType dutyType) {
    super();
    this.srcRoleId = srcRoleId;
    this.descRoleId = descRoleId;
    this.dutyType = dutyType;
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public DutyPk  getEntityPk(){
    DutyPk pk=new DutyPk();
    pk.setSrcRoleId(this.srcRoleId);
    pk.setDescRoleId(this.descRoleId);
    return pk;
  }
  /** 实体主键. */
  public void setEntityPk(DutyPk  pk){
    this.srcRoleId=pk.getSrcRoleId();
    this.descRoleId=pk.getDescRoleId();
  }
  
   /** 职责1(SrcRoleId). */
  @Id
  @Column(name="src_role_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getSrcRoleId() {
    return srcRoleId;
  }
  /** 职责1(SrcRoleId). */
  public void setSrcRoleId(UUID srcRoleId) {
    this.srcRoleId = srcRoleId;
  }
   /** 职责2(DescRoleId). */
  @Id
  @Column(name="desc_role_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getDescRoleId() {
    return descRoleId;
  }
  /** 职责2(DescRoleId). */
  public void setDescRoleId(UUID descRoleId) {
    this.descRoleId = descRoleId;
  }
   /** 职责分离类型(DutyType). */
  @Column(name="duty_type")
  @Enumerated(EnumType.STRING)
  public DutyType getDutyType() {
    return dutyType;
  }
  /** 职责分离类型(DutyType). */
  public void setDutyType(DutyType dutyType) {
    this.dutyType = dutyType;
  }
	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final RolesDutyEntity other = (RolesDutyEntity) obj;
	if (getEntityPk() == null) {
		if (other.getEntityPk() != null){
			return false;
		}
	} else if (!getEntityPk().equals(other.getEntityPk())){
		return false;
	}
    return true;
  }
  
  
  /**
   * DutyPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class DutyPk implements Serializable{
  
    private static final long serialVersionUID = 1L;
    
    /** 职责1. */
    @NotNull
    private UUID srcRoleId;
    /** 职责2. */
    @NotNull
    private UUID descRoleId;
    
    /** 职责1(SrcRoleId). */
	@Column(name="src_role_id",nullable = false)
    public UUID getSrcRoleId() {
      return srcRoleId;
    }
    
    /** 职责1(SrcRoleId). */
    public void setSrcRoleId(UUID srcRoleId) {
      this.srcRoleId = srcRoleId;
    }
    /** 职责2(DescRoleId). */
	@Column(name="desc_role_id",nullable = false)
    public UUID getDescRoleId() {
      return descRoleId;
    }
    
    /** 职责2(DescRoleId). */
    public void setDescRoleId(UUID descRoleId) {
      this.descRoleId = descRoleId;
    }
      
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result
          + ((srcRoleId == null) ? 0 : srcRoleId.hashCode());
      result = prime * result
          + ((descRoleId == null) ? 0 : descRoleId.hashCode());
      return result;
    }
  
    @Override
    public boolean equals(Object obj) {
      if (this == obj){
        return true;
      }
      if (obj == null){
        return false;
      }
      if (getClass() != obj.getClass()){
        return false;
      }
      final DutyPk other = (DutyPk) obj;
      
      if (srcRoleId == null) {
        if (other.srcRoleId != null){
          return false;
        }
      } else if (!srcRoleId.equals(other.srcRoleId)){
        return false;
      }
      if (descRoleId == null) {
        if (other.descRoleId != null){
          return false;
        }
      } else if (!descRoleId.equals(other.descRoleId)){
        return false;
      }
      return true;
    }
  }

}

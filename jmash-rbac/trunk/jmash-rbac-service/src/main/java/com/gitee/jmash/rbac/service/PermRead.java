package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.PermEntity;
import com.gitee.jmash.rbac.model.PermTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.PermReq;

/**
 * 权限表 rbac_perm服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface PermRead extends TenantService {

  /** 根据主键查询. */
  public PermEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<PermEntity, PermTotal> findPageByReq(@NotNull @Valid PermReq req);

  /** 综合查询. */
  public List<PermEntity> findListByReq(@NotNull @Valid PermReq req);

  /** 获取用户权限编码列表. */
  public Set<String> findUserPerms(Set<UUID> roleIds);

  /** 获取用户权限编码列表. */
  public List<String> findRolePerms(UUID roleId);

  /** 权限编码唯一性校验. */
  public boolean checkPermCodeUnique(@NotNull String permCode);
}


package com.gitee.jmash.rbac.enums;

/**
 * Token Type.
 *
 * @author CGD
 *
 */
public enum TokenType {

  Bearer("Bearer"), MAC("MAC");

  private String tokenType;

  TokenType(String grantType) {
    this.tokenType = grantType;
  }

  @Override
  public String toString() {
    return tokenType;
  }
}

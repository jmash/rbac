
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.OperationEntity;
import com.gitee.jmash.rbac.model.OperationTotal;
import java.util.List;
import jmash.rbac.protobuf.OperationCreateReq;
import jmash.rbac.protobuf.OperationModel;
import jmash.rbac.protobuf.OperationPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * OperationMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface OperationMapper extends BeanMapper,ProtoMapper {

  OperationMapper INSTANCE = Mappers.getMapper(OperationMapper.class);
  

  List<OperationModel> listOperation(List<OperationEntity> list);

  OperationPage pageOperation(DtoPage<OperationEntity, OperationTotal> page);
    
  OperationModel model(OperationEntity entity);
  
  
  
  OperationEntity create(OperationCreateReq req);

  OperationEntity clone(OperationEntity entity);
  
  
}

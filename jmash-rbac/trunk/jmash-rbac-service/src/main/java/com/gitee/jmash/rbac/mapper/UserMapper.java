
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.UserEntity;
import com.gitee.jmash.rbac.model.UserTotal;
import java.util.List;
import jmash.rbac.protobuf.UserCreateReq;
import jmash.rbac.protobuf.UserInfoPage;
import jmash.rbac.protobuf.UserModel;
import jmash.rbac.protobuf.UserPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * UserMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface UserMapper extends BeanMapper, ProtoMapper {

  UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);


  List<UserModel> listUser(List<UserEntity> list);

  UserPage pageUser(DtoPage<UserEntity, UserTotal> page);

  UserModel model(UserEntity entity);

  UserEntity create(UserCreateReq req);

  UserEntity clone(UserEntity entity);

  UserInfoPage pageUserInfo(DtoPage<UserEntity, UserTotal> page);

}

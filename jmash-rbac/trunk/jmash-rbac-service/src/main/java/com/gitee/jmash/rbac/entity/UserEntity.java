
package com.gitee.jmash.rbac.entity;

import com.gitee.jmash.common.utils.DesensitizeUtil;
import com.gitee.jmash.core.orm.jpa.CryptoConverter;
import com.gitee.jmash.core.orm.jpa.entity.FullEntity;
import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;
import java.util.UUID;
import jmash.protobuf.Gender;
import jmash.rbac.protobuf.UserStatus;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.TenantId;

/**
 * 本代码为自动生成工具生成 用户表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_user")
public class UserEntity extends FullEntity<UUID> implements JpaTenantIdentifier {

  private static final long serialVersionUID = 1L;

  /** 租户. */
  private TenantIdentifier tenantId;
  /** 账号. */
  private UUID userId = UUID.randomUUID();
  /** 统一身份ID. */
  private String unifiedId = "";
  /** 目录ID. */
  private String directoryId;
  /** 用户名/登录名. */
  private String loginName;
  /** 手机号. */
  private String mobilePhone;
  /** 电子邮件. */
  private String email;
  /** 姓名. */
  private String realName;
  /** 昵称. */
  private String nickName;
  /** 头像. */
  private String avatar = "";
  /** 出生日期. */
  private LocalDate birthDate;
  /** 性别. */
  private Gender gender = Gender.unknown;
  /** 手机通过审核. */
  private Boolean phoneApproved = false;
  /** 邮箱通过审核. */
  private Boolean emailApproved = false;
  /** 用户通过审核. */
  private Boolean approved = false;
  /** 用户状态. */
  private UserStatus status = UserStatus.enabled;
  /** 个人信息存储区. */
  private String storage = "p02";
  /** 上次被锁/禁用时间. */
  private LocalDateTime lastLockoutTime = LocalDateTime.of(LocalDate.EPOCH, LocalTime.MAX);
  /** 登录失败时间. */
  private LocalDateTime failedTime = LocalDateTime.of(LocalDate.EPOCH, LocalTime.MAX);
  /** 登录失败次数. */
  private Integer failedCount = 0;
  /** 上次登录时间. */
  private LocalDateTime lastLoginTime = LocalDateTime.of(LocalDate.EPOCH, LocalTime.MAX);

  /**
   * 默认构造函数.
   */
  public UserEntity() {
    super();
  }

  /** 插入前. */
  @PrePersist
  @Override
  public void onPersist() {
    if (!StringUtils.equals(tenantId.getTenantIdentifier(), TenantIdentifier.DEFAULT)) {
      this.storage = "";
    }
    super.onPersist();
  }

  /**
   * 有参构造函数
   * 
   * @param tenant 租户
   * @param mobilePhone 手机号
   */
  public UserEntity(String tenant, String mobilePhone) {
    super();
    this.directoryId = tenant;
    this.mobilePhone = mobilePhone;
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.userId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.userId = pk;
  }

  @TenantId
  @Column(name = "tenant_id")
  public TenantIdentifier getTenantId() {
    return tenantId;
  }

  public void setTenantId(TenantIdentifier tenantId) {
    this.tenantId = tenantId;
  }

  /** 账号(UserId). */
  @Id
  @Column(name = "user_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }

  /** 账号(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /** 统一身份ID. */
  @Column(name = "unified_id", nullable = false)
  public String getUnifiedId() {
    return unifiedId;
  }

  public void setUnifiedId(String unifiedId) {
    this.unifiedId = unifiedId;
  }

  /** 目录ID(DirectoryId). */

  @Column(name = "directory_id", nullable = false)
  public String getDirectoryId() {
    return directoryId;
  }

  /** 目录ID(DirectoryId). */
  public void setDirectoryId(String directoryId) {
    this.directoryId = directoryId;
  }

  /** 用户名/登录名(LoginName). */
  @Column(name = "login_name")
  public String getLoginName() {
    return loginName;
  }

  /** 用户名/登录名(LoginName). */
  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }

  /** 手机号(MobilePhone). */
  @Column(name = "mobile_phone")
  @Convert(converter = CryptoConverter.class)
  public String getMobilePhone() {
    return mobilePhone;
  }

  /** 手机号(MobilePhone). */
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  @Column(name = "mobile_phone_ins")
  public String getMobilePhoneIns() {
    return DesensitizeUtil.desensitizedPhone(mobilePhone);
  }

  public void setMobilePhoneIns(String mobilePhoneIns) {}

  /** 电子邮件(Email). */
  @Column(name = "email_")
  @Convert(converter = CryptoConverter.class)
  public String getEmail() {
    return email;
  }

  /** 电子邮件(Email). */
  public void setEmail(String email) {
    this.email = email;
  }

  @Column(name = "email_ins")
  public String getEmailIns() {
    return DesensitizeUtil.desensitizeEmail(this.email);
  }

  public void setEmailIns(String emailIns) {}

  /** 姓名(RealName). */
  @Column(name = "real_name")
  @Convert(converter = CryptoConverter.class)
  public String getRealName() {
    return realName;
  }

  /** 姓名(RealName). */
  public void setRealName(String realName) {
    this.realName = realName;
  }

  @Column(name = "real_name_ins")
  public String getRealNameIns() {
    return DesensitizeUtil.desensitizedName(this.realName);
  }

  public void setRealNameIns(String realNameIns) {}

  /** 昵称(NickName). */
  @Column(name = "nick_name")
  public String getNickName() {
    return nickName;
  }

  /** 昵称(NickName). */
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  /** 头像(Avatar). */
  @Column(name = "avatar_")
  public String getAvatar() {
    return avatar == null ? "" : avatar;
  }

  /** 头像(Avatar). */
  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  /** 出生日期(BirthDate). */
  @Column(name = "birth_date")
  public LocalDate getBirthDate() {
    return birthDate;
  }

  /** 出生日期(BirthDate). */
  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  /** 性别(Gender). */
  @Column(name = "gender_")
  @Enumerated
  public Gender getGender() {
    if (gender == null) {
      gender = Gender.unknown;
    }
    return gender;
  }

  /** 性别(Gender). */
  public void setGender(Gender gender) {
    this.gender = gender;
  }

  /** 手机通过审核(PhoneApproved). */

  @Column(name = "phone_approved", nullable = false)
  public Boolean getPhoneApproved() {
    return phoneApproved;
  }

  /** 手机通过审核(PhoneApproved). */
  public void setPhoneApproved(Boolean phoneApproved) {
    this.phoneApproved = phoneApproved;
  }

  /** 邮箱通过审核(EmailApproved). */

  @Column(name = "email_approved", nullable = false)
  public Boolean getEmailApproved() {
    return emailApproved;
  }

  /** 邮箱通过审核(EmailApproved). */
  public void setEmailApproved(Boolean emailApproved) {
    this.emailApproved = emailApproved;
  }

  /** 用户通过审核(Approved). */

  @Column(name = "approved_", nullable = false)
  public Boolean getApproved() {
    return approved;
  }

  /** 用户通过审核(Approved). */
  public void setApproved(Boolean approved) {
    this.approved = approved;
  }

  /** 用户状态(Status). */

  @Column(name = "status_", nullable = false)
  @Enumerated(EnumType.STRING)
  public UserStatus getStatus() {
    return status;
  }

  /** 用户状态(Status). */
  public void setStatus(UserStatus status) {
    this.status = status;
  }

  @Column(name = "storage_")
  public String getStorage() {
    return storage;
  }

  public void setStorage(String storage) {
    this.storage = storage;
  }

  /** 上次被锁/禁用时间(LastLockoutTime). */

  @Column(name = "last_lockout_time", nullable = false)
  public LocalDateTime getLastLockoutTime() {
    return lastLockoutTime;
  }

  /** 上次被锁/禁用时间(LastLockoutTime). */
  public void setLastLockoutTime(LocalDateTime lastLockoutTime) {
    this.lastLockoutTime = lastLockoutTime;
  }

  /** 登录失败时间(FailedTime). */

  @Column(name = "failed_time", nullable = false)
  public LocalDateTime getFailedTime() {
    return failedTime;
  }

  /** 登录失败时间(FailedTime). */
  public void setFailedTime(LocalDateTime failedTime) {
    this.failedTime = failedTime;
  }

  /** 登录失败次数(FailedCount). */

  @Column(name = "failed_count", nullable = false)
  public Integer getFailedCount() {
    return failedCount;
  }

  /** 登录失败次数(FailedCount). */
  public void setFailedCount(Integer failedCount) {
    this.failedCount = failedCount;
  }

  /** 上次登录时间(LastLoginTime). */

  @Column(name = "last_login_time", nullable = false)
  public LocalDateTime getLastLoginTime() {
    return lastLoginTime;
  }

  /** 上次登录时间(LastLoginTime). */
  public void setLastLoginTime(LocalDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    UserEntity other = (UserEntity) obj;
    return Objects.equals(userId, other.userId);
  }

}

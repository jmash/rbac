
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.model.ModuleTotal;
import java.util.List;
import jmash.rbac.protobuf.ModuleCreateReq;
import jmash.rbac.protobuf.ModuleModel;
import jmash.rbac.protobuf.ModulePage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * ModuleMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface ModuleMapper extends BeanMapper, ProtoMapper {

  ModuleMapper INSTANCE = Mappers.getMapper(ModuleMapper.class);


  List<ModuleModel> listModule(List<ModuleEntity> list);

  ModulePage pageModule(DtoPage<ModuleEntity, ModuleTotal> page);

  ModuleModel model(ModuleEntity entity);

  ModuleEntity create(ModuleCreateReq req);

  ModuleEntity clone(ModuleEntity entity);


}

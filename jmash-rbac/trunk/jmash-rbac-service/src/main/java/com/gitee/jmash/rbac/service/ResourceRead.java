package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.ResourceEntity;
import com.gitee.jmash.rbac.model.ResourcePermResult;
import com.gitee.jmash.rbac.model.TreeResult;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import jmash.rbac.protobuf.ResourceExportReq;
import jmash.rbac.protobuf.ResourceReq;

/**
 * 资源表 rbac_resource服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface ResourceRead extends TenantService {

  /** 根据主键查询. */
  public ResourceEntity findById(@NotNull UUID entityId);

  /** 综合查询. */
  public List<ResourceEntity> findListByReq(@NotNull @Valid ResourceReq req);
  
  /** 树. */
  public Tree<ResourceEntity,UUID> findTreeByReq(@NotNull @Valid ResourceReq req);

  public List<TreeResult> findTreeList();

  /** 获取资源对应的操作编码 */
  public List<String> findResourcePerm(ResourceEntity entity);

  public boolean checkResourceUrl(@NotNull String url);

  public String downloadResourceTemplate(ResourceExportReq request)
      throws FileNotFoundException, IOException;

  public String exportResource(ResourceExportReq request) throws FileNotFoundException, IOException;
  
  /** 资源权限列表. */
  public List<ResourcePermResult> findListByResource(ResourceReq resourseReq);
}


package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.TokenEntity;
import com.gitee.jmash.rbac.entity.TokenEntity.TokenPk;
import jakarta.validation.constraints.NotNull;

/**
 * Token实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class TokenDao extends BaseDao<TokenEntity, TokenPk> {

  public TokenDao() {
    super();
  }

  public TokenDao(TenantEntityManager tem) {
    super(tem);
  }

  public TokenEntity findByAuthzCode(String clientId, String authorizationCode) {
    String query = "select s from TokenEntity s where s.clientId =?1 and s.authorizationCode =?2 ";
    return this.findSingle(query, clientId, authorizationCode);
  }

  /** refresh_token 查询. */
  public TokenEntity findByRefreshToken(@NotNull String clientId, @NotNull String refreshToken) {
    String query = "select s from TokenEntity s where s.clientId =?1 and s.refreshToken =?2 ";
    return this.findSingle(query, clientId, refreshToken);
  }

  /** access_token 查询. */
  public TokenEntity findByAccessToken(@NotNull String clientId, @NotNull String accessToken) {
    String query = "select s from TokenEntity s where s.clientId =?1 and s.accessToken =?2 ";
    return this.findSingle(query, clientId, accessToken);
  }


}

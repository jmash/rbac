
package com.gitee.jmash.rbac.entity;

import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;


/**
 * 本代码为自动生成工具生成 安全日志表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_user_log")
@IdClass(UserLogPk.class)
public class UserLogEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 日志. */
  private UUID logId = UUID.randomUUID();
  /** 表分区. */
  private Integer partition;
  /** 操作类型. */
  private String logName;
  /** 操作内容/日志信息. */
  private String logMsg;
  /** 环境参数. */
  private String envProps;
  /** 设备标识ID. */
  private String deviceId;
  /** 用户IP. */
  private String userIp;
  /** 代理IP. */
  private String proxyIp;
  /** 创建人. */
  private UUID createBy;
  /** 创建时间. */
  private LocalDateTime createTime;

  /**
   * 默认构造函数.
   */
  public UserLogEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UserLogPk getEntityPk() {
    UserLogPk pk = new UserLogPk();
    pk.setPartition(this.partition);
    pk.setLogId(this.logId);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(UserLogPk pk) {
    this.partition = pk.getPartition();
    this.logId = pk.getLogId();
  }

  /** 日志(LogId). */
  @Id
  @Column(name = "log_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getLogId() {
    return logId;
  }

  /** 日志(LogId). */
  public void setLogId(UUID logId) {
    this.logId = logId;
  }

  /** 表分区(Partition). */
  @Id
  @Column(name = "partition_", nullable = false)
  public Integer getPartition() {
    return partition;
  }

  /** 表分区(Partition). */
  public void setPartition(Integer partition) {
    this.partition = partition;
  }

  /** 操作类型(LogName). */
  @Column(name = "log_name", nullable = false)
  public String getLogName() {
    return logName;
  }

  /** 操作类型(LogName). */
  public void setLogName(String logName) {
    this.logName = logName;
  }

  /** 操作内容/日志信息(LogMsg). */
  @Column(name = "log_msg", nullable = false)
  public String getLogMsg() {
    return logMsg;
  }

  /** 操作内容/日志信息(LogMsg). */
  public void setLogMsg(String logMsg) {
    this.logMsg = logMsg;
  }

  /** 环境参数(EnvProps). */
  @Column(name = "env_props")
  @Lob
  public String getEnvProps() {
    return envProps;
  }

  /** 环境参数(EnvProps). */
  public void setEnvProps(String envProps) {
    this.envProps = envProps;
  }

  /** 设备标识ID(DeviceId). */

  @Column(name = "device_id", nullable = false)
  public String getDeviceId() {
    return deviceId;
  }

  /** 设备标识ID(DeviceId). */
  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  /** 用户IP(UserIp). */
  @Column(name = "user_ip", nullable = false)
  public String getUserIp() {
    return userIp;
  }

  /** 用户IP(UserIp). */
  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }

  /** 代理IP(ProxyIp). */
  @Column(name = "proxy_ip", nullable = false)
  public String getProxyIp() {
    return proxyIp;
  }

  /** 代理IP(ProxyIp). */
  public void setProxyIp(String proxyIp) {
    this.proxyIp = proxyIp;
  }

  /**
   * 创建人(CreateBy).
   */
  @Column(name = "create_by", columnDefinition = "BINARY(16)")
  @NotNull
  public UUID getCreateBy() {
    return createBy;
  }

  /**
   * 创建人(CreateBy).
   */
  public void setCreateBy(UUID createBy) {
    this.createBy = createBy;
  }

  /**
   * 创建时间(CreateTime).
   */
  @Column(name = "create_time", nullable = false)
  @NotNull
  public LocalDateTime getCreateTime() {
    return createTime;
  }

  /**
   * 创建时间(CreateTime).
   */
  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final UserLogEntity other = (UserLogEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * UserLogPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class UserLogPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 表分区. */
    @NotNull
    private Integer partition;
    /** 日志. */
    @NotNull
    private UUID logId;

    /** 表分区(Partition). */
    @Column(name = "partition_", nullable = false)
    public Integer getPartition() {
      return partition;
    }

    /** 表分区(Partition). */
    public void setPartition(Integer partition) {
      this.partition = partition;
    }

    /** 日志(LogId). */
    @Column(name = "log_id", nullable = false)
    public UUID getLogId() {
      return logId;
    }

    /** 日志(LogId). */
    public void setLogId(UUID logId) {
      this.logId = logId;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((partition == null) ? 0 : partition.hashCode());
      result = prime * result + ((logId == null) ? 0 : logId.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final UserLogPk other = (UserLogPk) obj;

      if (partition == null) {
        if (other.partition != null) {
          return false;
        }
      } else if (!partition.equals(other.partition)) {
        return false;
      }
      if (logId == null) {
        if (other.logId != null) {
          return false;
        }
      } else if (!logId.equals(other.logId)) {
        return false;
      }
      return true;
    }
  }

}

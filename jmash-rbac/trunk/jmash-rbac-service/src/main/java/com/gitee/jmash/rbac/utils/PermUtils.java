package com.gitee.jmash.rbac.utils;

import com.gitee.jmash.common.grpc.GrpcContext;
import org.apache.shiro.subject.Subject;

public class PermUtils {
  /** 检查是否登录认证. */
  public static boolean isAuthenticated() {
    Subject subject = (Subject) GrpcContext.USER_SUBJECT.get();
    return subject != null && subject.isAuthenticated();
  }

}

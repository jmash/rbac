package com.gitee.jmash.rbac.event;

import com.gitee.jmash.common.event.OperEvent;
import com.gitee.jmash.rbac.RbacFactory;
import com.gitee.jmash.rbac.service.LogWrite;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.ObservesAsync;
import jakarta.enterprise.inject.spi.EventMetadata;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** 安全事件日志处理. */
@ApplicationScoped
public class OperEventDBLogHandle {

  private static Log log = LogFactory.getLog(OperEventDBLogHandle.class);


  /** 异步安全日志. */
  public void handleAsyncEvent(@ObservesAsync OperEvent event, EventMetadata metadata) {
    if (StringUtils.isBlank(event.getTenant())) {
      log.debug("---" + event + "---");
      return;
    }
    log.trace("---" + event.getTenant() + "---" + event.getLogLevel() + "---");
    try (LogWrite logWrite = RbacFactory.getLogWrite(event.getTenant())){
      logWrite.insert(event, metadata);
    } catch (Exception ex) {
      log.error("保存操作日志错误:", ex);
    } 
  }
  
}


package com.gitee.jmash.rbac.entity;

import com.gitee.jmash.core.orm.jpa.entity.OrderBy;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;


/**
 * 本代码为自动生成工具生成 操作表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_operation")
public class OperationEntity implements OrderBy<UUID> {

  private static final long serialVersionUID = 1L;

  /** 操作ID. */
  private UUID operationId = UUID.randomUUID();
  /** 操作编码. */
  private String operationCode;
  /** 操作名称. */
  private String operationName;
  /** 排序. */
  private Integer orderBy;

  /**
   * 默认构造函数.
   */
  public OperationEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.operationId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.operationId = pk;
  }

  /** 操作ID(OperationId). */
  @Id
  @Column(name = "operation_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getOperationId() {
    return operationId;
  }

  /** 操作ID(OperationId). */
  public void setOperationId(UUID operationId) {
    this.operationId = operationId;
  }

  /** 操作编码(OperationCode). */

  @Column(name = "operation_code", nullable = false)
  public String getOperationCode() {
    return operationCode;
  }

  /** 操作编码(OperationCode). */
  public void setOperationCode(String operationCode) {
    this.operationCode = operationCode;
  }

  /** 操作名称(OperationName). */

  @Column(name = "operation_name", nullable = false)
  public String getOperationName() {
    return operationName;
  }

  /** 操作名称(OperationName). */
  public void setOperationName(String operationName) {
    this.operationName = operationName;
  }

  /** 排序(OrderBy). */
  @Column(name = "order_by", nullable = false)
  public Integer getOrderBy() {
    return orderBy;
  }

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((operationId == null) ? 0 : operationId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OperationEntity other = (OperationEntity) obj;
    if (operationId == null) {
      if (other.operationId != null) {
        return false;
      }
    } else if (!operationId.equals(other.operationId)) {
      return false;
    }
    return true;
  }

}

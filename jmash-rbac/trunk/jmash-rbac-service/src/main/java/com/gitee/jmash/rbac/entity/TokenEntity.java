
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.BasicEntity;
import com.gitee.jmash.rbac.entity.TokenEntity.TokenPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 本代码为自动生成工具生成 令牌表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_token")
@IdClass(TokenPk.class)
public class TokenEntity extends BasicEntity<TokenPk> {

  private static final long serialVersionUID = 1L;

  /** 账号ID. */
  private UUID userId = UUID.randomUUID();
  /** 客户端ID. */
  private String clientId;
  /** 重定向URI. */
  private String redirectUri;
  /** 发行机构. */
  private String issuer;
  /** 认证主体. */
  private String subject;
  /** 授权码. */
  private String authorizationCode;
  /** 授权码过期时间. */
  private LocalDateTime codeExpireTime;
  /** 过期秒数. */
  private Integer expiresIn;
  /** 过期时间. */
  private LocalDateTime expireTime;
  /** 访问Token. */
  private String accessToken;
  /** 刷新Token. */
  private String refreshToken;
  /** Token类型. */
  private String tokenType;
  /** 授权范围. */
  private String scope;


  /**
   * 默认构造函数.
   */
  public TokenEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public TokenPk getEntityPk() {
    TokenPk pk = new TokenPk();
    pk.setClientId(this.clientId);
    pk.setUserId(this.userId);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(TokenPk pk) {
    this.clientId = pk.getClientId();
    this.userId = pk.getUserId();
  }

  /** 账号ID(UserId). */
  @Id
  @Column(name = "user_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }

  /** 账号ID(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /** 客户端ID(ClientId). */
  @Id
  @Column(name = "client_id", nullable = false)
  public String getClientId() {
    return clientId;
  }

  /** 客户端ID(ClientId). */
  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  /** 重定向URI(RedirectUri). */
  @Column(name = "redirect_uri")
  public String getRedirectUri() {
    return redirectUri;
  }

  /** 重定向URI(RedirectUri). */
  public void setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
  }

  /** 发行机构(Issuer). */
  @Column(name = "issuer_")
  public String getIssuer() {
    return issuer;
  }

  /** 发行机构(Issuer). */
  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  /** 认证主体(Subject). */
  @Column(name = "subject_")
  public String getSubject() {
    return subject;
  }

  /** 认证主体(Subject). */
  public void setSubject(String subject) {
    this.subject = subject;
  }

  /** 授权码(AuthorizationCode). */
  @Column(name = "authorization_code")
  public String getAuthorizationCode() {
    return authorizationCode;
  }

  /** 授权码(AuthorizationCode). */
  public void setAuthorizationCode(String authorizationCode) {
    this.authorizationCode = authorizationCode;
  }

  /** 授权码过期时间(CodeExpireTime). */

  @Column(name = "code_expire_time", nullable = false)
  public LocalDateTime getCodeExpireTime() {
    return codeExpireTime;
  }

  /** 授权码过期时间(CodeExpireTime). */
  public void setCodeExpireTime(LocalDateTime codeExpireTime) {
    this.codeExpireTime = codeExpireTime;
  }

  /** 过期秒数(ExpiresIn). */
  @Column(name = "expires_in")
  public Integer getExpiresIn() {
    return expiresIn;
  }

  /** 过期秒数(ExpiresIn). */
  public void setExpiresIn(Integer expiresIn) {
    this.expiresIn = expiresIn;
  }

  /** 过期时间(ExpireTime). */

  @Column(name = "expire_time", nullable = false)
  public LocalDateTime getExpireTime() {
    return expireTime;
  }

  /** 过期时间(ExpireTime). */
  public void setExpireTime(LocalDateTime expireTime) {
    this.expireTime = expireTime;
  }

  /** 访问Token(AccessToken). */
  @Column(name = "access_token")
  public String getAccessToken() {
    return accessToken;
  }

  /** 访问Token(AccessToken). */
  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  /** 刷新Token(RefreshToken). */
  @Column(name = "refresh_token")
  public String getRefreshToken() {
    return refreshToken;
  }

  /** 刷新Token(RefreshToken). */
  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  /** Token类型(TokenType). */
  @Column(name = "token_type")
  public String getTokenType() {
    return tokenType;
  }

  /** Token类型(TokenType). */
  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  /** 授权范围(Scope). */
  @Column(name = "scope_")
  public String getScope() {
    return scope;
  }

  /** 授权范围(Scope). */
  public void setScope(String scope) {
    this.scope = scope;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TokenEntity other = (TokenEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * TokenPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class TokenPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 客户端ID. */
    @NotNull
    private String clientId;
    /** 账号ID. */
    @NotNull
    private UUID userId;

    public TokenPk() {
      super();
    }

    public TokenPk(@NotNull UUID userId, @NotNull String clientId) {
      super();
      this.clientId = clientId;
      this.userId = userId;
    }

    /** 客户端ID(ClientId). */
    @Column(name = "client_id", nullable = false)
    public String getClientId() {
      return clientId;
    }

    /** 客户端ID(ClientId). */
    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    /** 账号ID(UserId). */
    @Column(name = "user_id", nullable = false)
    public UUID getUserId() {
      return userId;
    }

    /** 账号ID(UserId). */
    public void setUserId(UUID userId) {
      this.userId = userId;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
      result = prime * result + ((userId == null) ? 0 : userId.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final TokenPk other = (TokenPk) obj;

      if (clientId == null) {
        if (other.clientId != null) {
          return false;
        }
      } else if (!clientId.equals(other.clientId)) {
        return false;
      }
      if (userId == null) {
        if (other.userId != null) {
          return false;
        }
      } else if (!userId.equals(other.userId)) {
        return false;
      }
      return true;
    }
  }

}

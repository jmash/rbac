
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.LogEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import com.gitee.jmash.rbac.model.LogTotal;
import java.util.List;
import jmash.rbac.protobuf.LogKey;
import jmash.rbac.protobuf.LogModel;
import jmash.rbac.protobuf.LogPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * LogMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface LogMapper extends BeanMapper,ProtoMapper {

  LogMapper INSTANCE = Mappers.getMapper(LogMapper.class);
  

  List<LogModel> listLog(List<LogEntity> list);

  LogPage pageLog(DtoPage<LogEntity, LogTotal> page);
    
  LogModel model(LogEntity entity);
  
  LogPk pk(LogKey key);  
  
  LogKey key(LogModel model);  

  LogEntity clone(LogEntity entity);
  
  
}

package com.gitee.jmash.rbac.excel;

import com.gitee.jmash.common.excel.read.HeaderField;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DeptHeaderImport {

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderField> getHeaderImports() {
    List<HeaderField> list = new ArrayList<HeaderField>();
    list.add(HeaderField.field("部门ID", "deptId", true));
    list.add(HeaderField.field("部门编码", "deptCode", true));
    list.add(HeaderField.field("部门名称", "deptName", true));
    list.add(HeaderField.field("部门类型", "deptType", true));
    list.add(HeaderField.field("父部门", "parentId", true));
    list.add(HeaderField.field("部门描述", "description", true));
    list.add(HeaderField.dict("状态", "status", true,getStatusMap()));
    return list;
  }

  /** 部门状态Map. */
  public static Map<String, String> getStatusMap() {
    Map<String, String> statusMap = new LinkedHashMap<>();
    statusMap.put("启用", "true");
    statusMap.put("禁用", "false");
    return statusMap;
  }

}

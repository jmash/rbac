package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OpensEntity;
import com.gitee.jmash.rbac.entity.OpensEntity.OpensPk;
import com.gitee.jmash.rbac.entity.TokenEntity;
import com.gitee.jmash.rbac.entity.UserEntity;
import com.gitee.jmash.rbac.model.UserOpenCreateReq;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.LoginQrcodeReq;
import jmash.rbac.protobuf.LoginReq;
import jmash.rbac.protobuf.LogoutReq;
import jmash.rbac.protobuf.OpensType;
import jmash.rbac.protobuf.OrganUserCreateReq;
import jmash.rbac.protobuf.RefreshTokenReq;
import jmash.rbac.protobuf.UpdateUserReq;
import jmash.rbac.protobuf.UserCreateReq;
import jmash.rbac.protobuf.UserEnableKey;
import jmash.rbac.protobuf.UserImportReq;
import jmash.rbac.protobuf.UserStatus;
import jmash.rbac.protobuf.UserUpdateReq;
import jmash.rbac.protobuf.ValidCodeLoginReq;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * 用户 rbac_user服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface UserWrite extends TenantService {

  /** 插入实体. */
  public UserEntity insert(@NotNull @Valid UserCreateReq user);

  /** update 实体. */
  public UserEntity update(@NotNull @Valid UserUpdateReq user);

  /** 根据主键删除. */
  public UserEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

  /** 登录. */
  public TokenEntity login(@NotNull @Valid LoginReq req);

  /** 扫码登录. */
  public TokenEntity loginByQrcode(@NotNull @Valid LoginQrcodeReq req);

  /** 验证码登录. */
  public TokenEntity loginByValidCode(@NotNull @Valid ValidCodeLoginReq req);

  /** 登出. */
  public boolean logout(@NotNull @Valid LogoutReq req);

  /** 锁定用户. */
  public Integer lockUser(@NotNull Set<UUID> userIds, @NotNull UserStatus userStatus);

  /** 审核/取消审核用户. */
  public Integer approvedUser(@NotNull Set<UUID> userIds, boolean approved);

  /** 启用/禁用用户. */
  public boolean enableUser(@NotNull UserEnableKey userEnableKey);

  /** 批量启用/禁用用户. */
  public Integer batchEnableUser(Set<UUID> entityIds, boolean enabled);

  /** refresh_token -> access_token,refresh_token. */
  public TokenEntity refreshToken(@NotNull @Valid RefreshTokenReq token);

  /** password --> access_token,refresh_token. */
  public TokenEntity passwordToken(@NotNull JsonWebToken webToken, @NotBlank String clientId,
      String scope);

  /** 清理Token. */
  public TokenEntity clearToken(@NotNull String clientId, @NotNull String accessToken);

  /** 用户修改密码. */
  public boolean changePwd(@NotNull String oldPwd, @NotNull String newPwd);

  /** 重置密码. */
  public boolean resetPwd(@NotNull UUID userId, @NotNull String pwd, @NotNull String repeatPwd);

  /** 导入用户信息. */
  public String importUser(@NotNull @Valid UserImportReq request) throws Exception;

  /** 解绑三方登录信息. */
  public OpensEntity deleteUserOpens(@NotNull @Valid OpensPk pk);

  public UserEntity updateUser(@NotNull @Valid UpdateUserReq user, @NotNull String userId);

  /** 三方登录. */
  public TokenEntity loginByOpenId(@NotNull String tenant, @NotNull OpensType openType,
      @NotNull String appId, String openId, String unionId);

  /** 用户注册、绑定、登录. */
  public TokenEntity createBindLogin(@NotNull @Valid UserOpenCreateReq req);

  /** 更换绑定手机号. */
  public boolean updateBindPhone(@NotNull String mobilePhone);

  /** 创建组织系统用户. */
  public UserEntity createOrganSystemUser();

  /** 登录组织. */
  public TokenEntity loginOrgan(@NotNull String tenant);

  /** 创建组织用户. */
  public UserEntity createOrganUser(@NotNull UserEntity unifiedUser,
      @NotNull OrganUserCreateReq req);

}

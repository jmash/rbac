package com.gitee.jmash.rbac;

import com.gitee.jmash.rbac.service.DeptRead;
import com.gitee.jmash.rbac.service.DeptWrite;
import com.gitee.jmash.rbac.service.LogRead;
import com.gitee.jmash.rbac.service.LogWrite;
import com.gitee.jmash.rbac.service.ModuleRead;
import com.gitee.jmash.rbac.service.ModuleWrite;
import com.gitee.jmash.rbac.service.OpenAppRead;
import com.gitee.jmash.rbac.service.OpenAppWrite;
import com.gitee.jmash.rbac.service.OpensRead;
import com.gitee.jmash.rbac.service.OperationRead;
import com.gitee.jmash.rbac.service.OperationWrite;
import com.gitee.jmash.rbac.service.PermRead;
import com.gitee.jmash.rbac.service.PermWrite;
import com.gitee.jmash.rbac.service.ResourceRead;
import com.gitee.jmash.rbac.service.ResourceWrite;
import com.gitee.jmash.rbac.service.RoleRead;
import com.gitee.jmash.rbac.service.RoleWrite;
import com.gitee.jmash.rbac.service.UserLogRead;
import com.gitee.jmash.rbac.service.UserLogWrite;
import com.gitee.jmash.rbac.service.UserRead;
import com.gitee.jmash.rbac.service.UserWrite;
import jakarta.enterprise.inject.spi.CDI;

/**
 * 模块服务工厂类.
 *
 * @author CGD
 *
 */
public class RbacFactory {

  public static void destroy(Object instance) {
    if (null != instance) {
      CDI.current().destroy(instance);
    }
  }

  public static UserWrite getUserWrite(String tenant) {
    return CDI.current().select(UserWrite.class).get().setTenant(tenant);
  }

  public static UserRead getUserRead(String tenant) {
    return CDI.current().select(UserRead.class).get().setTenant(tenant);
  }

  public static DeptRead getDeptRead(String tenant) {
    return CDI.current().select(DeptRead.class).get().setTenant(tenant);
  }

  public static LogRead getLogRead(String tenant) {
    return CDI.current().select(LogRead.class).get().setTenant(tenant);
  }

  public static ModuleRead getModuleRead(String tenant) {
    return CDI.current().select(ModuleRead.class).get().setTenant(tenant);
  }

  public static OpensRead getOpensRead(String tenant) {
    return CDI.current().select(OpensRead.class).get().setTenant(tenant);
  }

  public static OpenAppRead getOpenAppRead(String tenant) {
    return CDI.current().select(OpenAppRead.class).get().setTenant(tenant);
  }

  public static OperationRead getOperationRead(String tenant) {
    return CDI.current().select(OperationRead.class).get().setTenant(tenant);
  }

  public static PermRead getPermRead(String tenant) {
    return CDI.current().select(PermRead.class).get().setTenant(tenant);
  }

  public static ResourceRead getResourceRead(String tenant) {
    return CDI.current().select(ResourceRead.class).get().setTenant(tenant);
  }

  public static RoleRead getRoleRead(String tenant) {
    return CDI.current().select(RoleRead.class).get().setTenant(tenant);
  }

  public static UserLogRead getUserLogRead(String tenant) {
    return CDI.current().select(UserLogRead.class).get().setTenant(tenant);
  }

  public static DeptWrite getDeptWrite(String tenant) {
    return CDI.current().select(DeptWrite.class).get().setTenant(tenant);
  }

  public static LogWrite getLogWrite(String tenant) {
    return CDI.current().select(LogWrite.class).get().setTenant(tenant);
  }

  public static ModuleWrite getModuleWrite(String tenant) {
    return CDI.current().select(ModuleWrite.class).get().setTenant(tenant);
  }

  public static OperationWrite getOperationWrite(String tenant) {
    return CDI.current().select(OperationWrite.class).get().setTenant(tenant);
  }

  public static PermWrite getPermWrite(String tenant) {
    return CDI.current().select(PermWrite.class).get().setTenant(tenant);
  }

  public static ResourceWrite getResourceWrite(String tenant) {
    return CDI.current().select(ResourceWrite.class).get().setTenant(tenant);
  }

  public static RoleWrite getRoleWrite(String tenant) {
    return CDI.current().select(RoleWrite.class).get().setTenant(tenant);
  }

  public static UserLogWrite getUserLogWrite(String tenant) {
    return CDI.current().select(UserLogWrite.class).get().setTenant(tenant);
  }

  public static OpenAppWrite getOpenAppWrite(String tenant) {
    return CDI.current().select(OpenAppWrite.class).get().setTenant(tenant);
  }

}


package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.BasicEntity;
import com.gitee.jmash.rbac.enums.PwdFormat;
import com.gitee.jmash.rbac.enums.SecretType;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;

/**
 * 本代码为自动生成工具生成 用户密钥表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_user_secret")
public class UserSecretEntity extends BasicEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /** 密钥ID. */
  private UUID secretId = UUID.randomUUID();
  /** 用户. */
  private UUID userId;
  /** 密钥类型. */
  private String secretType = SecretType.Login.name();
  /** 密钥. */
  private String pwdValue;
  /** 密钥格式. */
  private PwdFormat pwdFormat = PwdFormat.SM3;
  /** 随机ALT. */
  private String pwdAlt;
  /** 是否临时密钥. */
  private Boolean isTemp = false;


  /**
   * 默认构造函数.
   */
  public UserSecretEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.secretId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.secretId = pk;
  }

  /** 密钥ID(SecretId). */
  @Id
  @Column(name = "secret_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getSecretId() {
    return secretId;
  }

  /** 密钥ID(SecretId). */
  public void setSecretId(UUID secretId) {
    this.secretId = secretId;
  }

  /** 用户(UserId). */
  @Column(name = "user_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }

  /** 用户(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /** 密钥类型(SecretType). */

  @Column(name = "secret_type", nullable = false)

  public String getSecretType() {
    return secretType;
  }

  /** 密钥类型(SecretType). */
  public void setSecretType(String secretType) {
    this.secretType = secretType;
  }

  /** 密钥(PwdValue). */

  @Column(name = "pwd_value", nullable = false)
  public String getPwdValue() {
    return pwdValue;
  }

  /** 密钥(PwdValue). */
  public void setPwdValue(String pwdValue) {
    this.pwdValue = pwdValue;
  }

  /** 密钥格式(PwdFormat). */

  @Column(name = "pwd_format", nullable = false)
  @Enumerated(EnumType.STRING)
  public PwdFormat getPwdFormat() {
    return pwdFormat;
  }

  /** 密钥格式(PwdFormat). */
  public void setPwdFormat(PwdFormat pwdFormat) {
    this.pwdFormat = pwdFormat;
  }

  /** 随机ALT(PwdAlt). */
  @Column(name = "pwd_alt")
  public String getPwdAlt() {
    return pwdAlt;
  }

  /** 随机ALT(PwdAlt). */
  public void setPwdAlt(String pwdAlt) {
    this.pwdAlt = pwdAlt;
  }

  /** 是否临时密钥(IsTemp). */

  @Column(name = "is_temp", nullable = false)
  public Boolean getIsTemp() {
    return isTemp;
  }

  /** 是否临时密钥(IsTemp). */
  public void setIsTemp(Boolean isTemp) {
    this.isTemp = isTemp;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((secretId == null) ? 0 : secretId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final UserSecretEntity other = (UserSecretEntity) obj;
    if (secretId == null) {
      if (other.secretId != null) {
        return false;
      }
    } else if (!secretId.equals(other.secretId)) {
      return false;
    }
    return true;
  }



}

package com.gitee.jmash.rbac.model;

import jakarta.validation.constraints.NotNull;
import java.util.UUID;

/** 用户日志创建请求. */
public class UserLogCreateReq {

  /** 操作类型. */
  @NotNull
  private String logName;
  /** 操作内容/日志信息. */
  @NotNull
  private String logMsg;
  /** 环境参数. */
  private String envProps;
  /** 设备标识ID. */
  private String deviceId;
  /** 用户IP. */
  private String userIp;
  /** 代理IP. */
  private String proxyIp;
  /** 创建人. */
  private UUID createBy;

  public String getLogName() {
    return logName;
  }

  public void setLogName(String logName) {
    this.logName = logName;
  }

  public String getLogMsg() {
    return logMsg;
  }

  public void setLogMsg(String logMsg) {
    this.logMsg = logMsg;
  }

  public String getEnvProps() {
    return envProps;
  }

  public void setEnvProps(String envProps) {
    this.envProps = envProps;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public String getUserIp() {
    return userIp;
  }

  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }

  public String getProxyIp() {
    return proxyIp;
  }

  public void setProxyIp(String proxyIp) {
    this.proxyIp = proxyIp;
  }

  public UUID getCreateBy() {
    return createBy;
  }

  public void setCreateBy(UUID createBy) {
    this.createBy = createBy;
  }



}

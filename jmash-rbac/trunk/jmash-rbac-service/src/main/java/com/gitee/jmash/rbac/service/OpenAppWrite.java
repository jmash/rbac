package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import jmash.rbac.protobuf.OpenAppCreateReq;
import jmash.rbac.protobuf.OpenAppUpdateReq;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

 /**
 * 开发平台应用 rbac_open_app服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface OpenAppWrite extends TenantService {
		
  /** 插入实体. */
  public OpenAppEntity insert(@NotNull @Valid OpenAppCreateReq  openApp);


  /** update 实体. */
  public OpenAppEntity update(@NotNull @Valid OpenAppUpdateReq  openApp);

  /** 根据主键删除. */
  public OpenAppEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set< @NotNull UUID > entityIds);
	


}

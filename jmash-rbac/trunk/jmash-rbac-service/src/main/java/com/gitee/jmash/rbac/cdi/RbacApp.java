
package com.gitee.jmash.rbac.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.core.orm.module.DbBuild;
import com.gitee.jmash.rbac.shiro.JmashShiroConfig;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

public class RbacApp {

  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 构建数据库.
      DbBuild dbBuild = container.select(DbBuild.class).get();
      dbBuild.build();
      // Shiro 初始化.
      JmashShiroConfig.config();

      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.start(false);
      server.blockUntilShutdown();
    }
  }

}

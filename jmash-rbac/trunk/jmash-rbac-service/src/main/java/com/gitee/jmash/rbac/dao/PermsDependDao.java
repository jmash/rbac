
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.PermsDependEntity;
import com.gitee.jmash.rbac.entity.PermsDependEntity.PermsDependPk;
import com.gitee.jmash.rbac.enums.DependType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * PermsDepend实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class PermsDependDao extends BaseDao<PermsDependEntity, PermsDependPk> {

  public PermsDependDao() {
    super();
  }

  public PermsDependDao(TenantEntityManager tem) {
    super(tem);
  }

  public Map<String, Set<String>> findMap(DependType dependType) {
    List<PermsDependEntity> list = findList(dependType);
    Map<String, Set<String>> map = new HashMap<>();
    for (PermsDependEntity entity : list) {
      if (map.containsKey(entity.getPermCode())) {
        map.get(entity.getPermCode()).add(entity.getDependPermCode());
      } else {
        Set<String> temp = new HashSet<>();
        temp.add(entity.getDependPermCode());
        map.put(entity.getPermCode(), temp);
      }
    }
    return map;
  }

  public List<PermsDependEntity> findList(DependType dependType) {
    if (null == dependType) {
      String sql = "select s from PermsDependEntity s ";
      return this.findList(sql);
    }
    String sql = "select s from PermsDependEntity s where s.dependType = ?1 ";
    return this.findList(sql, dependType);
  }

}

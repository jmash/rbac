
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.rbac.dao.OpenAppDao;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import jakarta.enterprise.inject.spi.CDI;
import jmash.rbac.protobuf.OpenAppReq;
import com.gitee.jmash.rbac.model.OpenAppTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.List;
import java.util.UUID;

/**
 * 开发平台应用 rbac_open_app读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(OpenAppRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class OpenAppReadBean implements OpenAppRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected OpenAppDao openAppDao = new OpenAppDao(tem);

  @PersistenceContext(unitName = "ReadRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager,false);
  }
	
  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }
  
  @Override
  public void setTenantOnly(String tenant) {
     this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }

  @Override
  public OpenAppEntity findById(UUID entityId) {
    return openAppDao.find(entityId);
  }


  @Override
  public DtoPage<OpenAppEntity,OpenAppTotal> findPageByReq(OpenAppReq req) {
    return openAppDao.findPageByReq(req);
  }

  @Override
  public List<OpenAppEntity> findListByReq(OpenAppReq req) {
    return openAppDao.findListByReq(req);
  }

  @Override
  public OpenAppEntity findByAppId(String appId) {
    return openAppDao.findByAppId(appId);
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}

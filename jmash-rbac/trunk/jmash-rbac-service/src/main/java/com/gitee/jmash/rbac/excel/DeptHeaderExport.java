package com.gitee.jmash.rbac.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.HeaderExport;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.protobuf.TableHead;

public class DeptHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "部门信息表";

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("部门ID", "deptId", 4 * 1000));
    list.add(HeaderExport.field("部门编码", "deptCode", 4 * 1000));
    list.add(HeaderExport.field("部门名称", "deptName", 4 * 1000));
    list.add(HeaderExport.field("部门类型", "deptType", 4 * 1000));
    list.add(HeaderExport.field("父部门", "parentId", 4 * 1000));
    list.add(HeaderExport.field("部门描述", "description", 4 * 1000));
    list.add(HeaderExport.dict("状态", "status", 4 * 1000, getStatusMap()));
    return list;
  }

  /** 部门状态Map. */
  public static Map<String, String> getStatusMap() {
    Map<String, String> statusMap = new LinkedHashMap<>();
    statusMap.put("true", "启用");
    statusMap.put("false", "禁用");
    return statusMap;
  }

  /**
   * Map HeaderExport
   * 
   * @return
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }
  
  /** TableHead List. */
  public static List<TableHead> getTableHeadList() {
    List<TableHead> tableHeads = getHeaderExports().stream()
        .map(
            obj -> TableHead.newBuilder().setLabel(obj.getHeader()).setProp(obj.getField()).build())
        .collect(Collectors.toList());
    return tableHeads;
  }

  /** 增加表头. */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }
}

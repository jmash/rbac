
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.CryptoConverter;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.UUID;
import jmash.rbac.protobuf.OpensType;


/**
 * rbac模块 开发平台应用表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_open_app")
public class OpenAppEntity  implements Serializable  {

  private static final long serialVersionUID = 1L;
  
  /** 开放应用ID. */
  private UUID openAppId= UUID.randomUUID()  ;
  /** 应用ID. */
  private String appId ;
  /** 应用名称. */
  private String appName ;
  /** 三方OpenID Type. */
  private OpensType openType ;
  /** 密钥(加密存储). */
  private String appSecret ;
  /** 描述. */
  private String description ;

  /**
  * 默认构造函数.
  */
  public OpenAppEntity(){
    super();
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID  getEntityPk(){
    return this.openAppId;
  }
  /** 实体主键. */
  public void setEntityPk(UUID  pk){
    this.openAppId=pk;
  }
  
   /** 开放应用ID(OpenAppId). */
  @Id
  @Column(name="open_app_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getOpenAppId() {
    return openAppId;
  }
  /** 开放应用ID(OpenAppId). */
  public void setOpenAppId(UUID openAppId) {
    this.openAppId = openAppId;
  }
   /** 应用ID(AppId). */

  @Column(name="app_id",nullable = false)
  public String getAppId() {
    return appId;
  }
  /** 应用ID(AppId). */
  public void setAppId(String appId) {
    this.appId = appId;
  }
   /** 应用名称(AppName). */
  @Column(name="app_name")
  public String getAppName() {
    return appName;
  }
  /** 应用名称(AppName). */
  public void setAppName(String appName) {
    this.appName = appName;
  }
   /** 三方OpenID Type(OpenType). */

  @Column(name="open_type",nullable = false)
  @Enumerated(EnumType.STRING)
  public OpensType getOpenType() {
    return openType;
  }
  /** 三方OpenID Type(OpenType). */
  public void setOpenType(OpensType openType) {
    this.openType = openType;
  }
   /** 密钥(加密存储)(AppSecret). */

  @Column(name="app_secret",nullable = false)
  @Convert(converter = CryptoConverter.class)
  public String getAppSecret() {
    return appSecret;
  }
  /** 密钥(加密存储)(AppSecret). */
  public void setAppSecret(String appSecret) {
    this.appSecret = appSecret;
  }
   /** 描述(Description). */
  @Column(name="description_")
  public String getDescription() {
    return description;
  }
  /** 描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }
	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((openAppId == null) ? 0 : openAppId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OpenAppEntity other = (OpenAppEntity) obj;
			if (openAppId == null) {
				if (other.openAppId != null){
				  return false;
				}
			} else if (!openAppId.equals(other.openAppId)){
			  return false;
			}
    return true;
  }
  
  

}


package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import com.gitee.jmash.rbac.mapper.OpenAppMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.OpenAppCreateReq;
import jmash.rbac.protobuf.OpenAppUpdateReq;

/**
 * 开发平台应用 rbac_open_app写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(OpenAppWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class OpenAppWriteBean extends OpenAppReadBean implements OpenAppWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager,true);
  }

  @Override
  public OpenAppEntity insert(OpenAppCreateReq openApp) {
    OpenAppEntity entity = OpenAppMapper.INSTANCE.create(openApp);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (openApp.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(openApp.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    openAppDao.persist(entity);
    return entity;
  }
  
  @Override
  public OpenAppEntity update(OpenAppUpdateReq req) {
    UUID id= UUIDUtil.fromString(req.getOpenAppId());
    OpenAppEntity entity=openAppDao.find(id,req.getValidateOnly());
    if (null==entity) {
        throw new ValidationException("找不到实体:"+req.getOpenAppId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务   
    openAppDao.merge(entity);
    return entity;
  }

  @Override
  public OpenAppEntity delete(UUID entityId) {
    OpenAppEntity entity = openAppDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      openAppDao.removeById(entityId);
      i++;
    }
    return i;
  }

}

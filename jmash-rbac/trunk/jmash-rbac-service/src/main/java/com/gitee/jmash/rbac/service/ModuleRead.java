package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.model.ModuleTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.rbac.protobuf.ModuleReq;

 /**
 * 系统模块 rbac_module服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface ModuleRead extends TenantService {

  /** 根据主键查询. */
  public ModuleEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<ModuleEntity,ModuleTotal> findPageByReq(@NotNull @Valid ModuleReq req);

  /** 综合查询. */
  public List<ModuleEntity> findListByReq(@NotNull @Valid ModuleReq req);

  /** 查询唯一. */
  public boolean checkModuleCodeUnique(String moduleCode);
 }

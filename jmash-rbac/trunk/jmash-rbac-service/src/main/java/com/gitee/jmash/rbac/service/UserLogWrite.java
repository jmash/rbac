package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.UserLogEntity;
import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import com.gitee.jmash.rbac.model.UserLogCreateReq;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;


/**
 * 安全日志 rbac_user_log服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface UserLogWrite extends TenantService {


  /** 插入日志. */
  public UserLogEntity insert(@NotNull @Valid UserLogCreateReq logReq);

  /** 根据主键删除. */
  public UserLogEntity delete(@NotNull UserLogPk entityId);

  /** 根据N年前删除. */
  public Integer batchDelete(@NotNull int number);



}

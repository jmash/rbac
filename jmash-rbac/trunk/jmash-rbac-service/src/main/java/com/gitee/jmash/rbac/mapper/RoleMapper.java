
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.RolesDutyEntity;
import com.gitee.jmash.rbac.entity.RoleEntity;
import com.gitee.jmash.rbac.model.RoleTotal;

import java.util.*;

import jmash.rbac.protobuf.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * RoleMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface RoleMapper extends BeanMapper, ProtoMapper {

  RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);


  default RoleList listRole(List<RoleEntity> list) {
    List<String> roots = new ArrayList<>();
    Map<String, RoleModel.Builder> map = new LinkedHashMap<>();
    // 数据建立父子关系
    for (RoleEntity entity : list) {
      RoleModel model = RoleMapper.INSTANCE.model(entity);
      map.put(model.getRoleId(), model.toBuilder());
      RoleModel.Builder parent = map.get(model.getParentId());
      if (parent != null) {
        parent.addChildren(model);
      } else {
        roots.add(model.getRoleId());
      }
    }
    List<String> idList = new ArrayList<>(roots);
    Collections.reverse(idList);
    // 根列表
    RoleList.Builder builder = RoleList.newBuilder();
    for (String root : idList) {
      builder.addResults(0,map.get(root));
    }
    return builder.build();
  }

  default RoleModel model(RoleEntity entity, List<String> permCodes, List<RolesDutyEntity> dutyList) {
    RoleModel model = this.model(entity);
    if (permCodes != null && permCodes.size() > 0) {
      model = model.toBuilder().addAllPermCodes(permCodes).build();
    }
    if (dutyList != null && dutyList.size() > 0) {
      List<RoleDutyModel> dutyModelList = new ArrayList<>();
      for (RolesDutyEntity duty : dutyList) {
        RoleDutyModel.Builder dutyModel = RoleDutyModel.newBuilder();
        dutyModel.setRoleId(toProtoString(duty.getDescRoleId()));
        dutyModel.setRoleName("");
        dutyModel.setDutyType(duty.getDutyType());
        dutyModelList.add(dutyModel.build());
      }
      model = model.toBuilder().addAllRoleDutyModel(dutyModelList).build();
    }
    return model;
  }

  RolePage pageRole(DtoPage<RoleEntity, RoleTotal> page);

  RoleModel model(RoleEntity entity);

  RoleEntity create(RoleCreateReq req);

  RoleEntity clone(RoleEntity entity);

  default DsdRoleListResp getDsdRoleList(List<RoleEntity> list){
    List<RoleModel> roleModelList = new ArrayList<>();
    list.forEach(item -> {
      RoleModel model = INSTANCE.model(item);
      roleModelList.add(model);
    });
    return DsdRoleListResp.newBuilder().addAllRoleList(roleModelList).build();
  }


}

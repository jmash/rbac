package com.gitee.jmash.rbac.model;

import jakarta.persistence.Column;
import java.util.UUID;

public class ResourceResult {

  /** 资源ID. */
  private UUID resourceId;
  /** 模块ID. */
  private UUID moduleId ;
  /** 模块名称. */
  private String moduleName;
  /** 资源编码. */
  private String resourceCode ;
  /** 资源名称. */
  private String resourceName ;
  /** 资源类型. */
  private String resourceType ;
  /** 父资源ID. */
  private UUID parentId ;
  /** 深度. */
  private Integer depth ;
  /** 排序. */
  private Integer orderBy ;
  /** 目标. */
  private String target ;
  /** URL. */
  private String url ;
  /** 是否隐藏. */
  private Boolean hidden= false  ;

  /** 图标. */
  private String icon;

  @Column(name="resource_id")
  public UUID getResourceId() {
    return resourceId;
  }

  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }
  @Column(name="module_id")
  public UUID getModuleId() {
    return moduleId;
  }

  public void setModuleId(UUID moduleId) {
    this.moduleId = moduleId;
  }

  @Column(name="module_name")
  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  @Column(name="resource_code")
  public String getResourceCode() {
    return resourceCode;
  }

  public void setResourceCode(String resourceCode) {
    this.resourceCode = resourceCode;
  }

  @Column(name="resource_name")
  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  @Column(name="resourceType")
  public String getResourceType() {
    return resourceType;
  }

  public void setResourceType(String resourceType) {
    this.resourceType = resourceType;
  }

  @Column(name="parent_id")
  public UUID getParentId() {
    return parentId;
  }

  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  @Column(name="depth_")
  public Integer getDepth() {
    return depth;
  }

  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  @Column(name="order_by")
  public Integer getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  @Column(name="target_")
  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  @Column(name="url_")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Column(name="hidden_")
  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  @Column(name="icon_")
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

}

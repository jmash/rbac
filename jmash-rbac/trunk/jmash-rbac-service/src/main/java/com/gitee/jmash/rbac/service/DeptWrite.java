package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.DeptEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.DeptCreateReq;
import jmash.rbac.protobuf.DeptEnableKey;
import jmash.rbac.protobuf.DeptImportReq;
import jmash.rbac.protobuf.DeptMoveKey;
import jmash.rbac.protobuf.DeptUpdateReq;

 /**
 * 组织机构 rbac_dept服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DeptWrite extends TenantService{
		
  /** 插入实体. */
  public DeptEntity insert(@NotNull @Valid DeptCreateReq  dept);


  /** update 实体. */
  public DeptEntity update(@NotNull @Valid DeptUpdateReq  dept);


  /** 根据主键删除. */
  public DeptEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set< @NotNull UUID > entityIds);

  /** 排序调整. */
  public boolean moveOrderBy(@NotNull DeptMoveKey deptMoveKey);

  /** 批量启禁用 */
  public Integer lock(Set<UUID> entityIds, boolean enable);

  /** 启用禁用 */
  public boolean enableDept(@NotNull DeptEnableKey deptEnableKey);

  /** 导入 */
  public String importDept(DeptImportReq req) throws Exception;
 }

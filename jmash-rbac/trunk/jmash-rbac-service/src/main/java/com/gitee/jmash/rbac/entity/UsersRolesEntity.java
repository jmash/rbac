
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import com.gitee.jmash.rbac.entity.UsersRolesEntity.UsersRolesPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import org.hibernate.annotations.TenantId;


/**
 * 本代码为自动生成工具生成 用户角色表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_users_roles")
@IdClass(UsersRolesPk.class)
public class UsersRolesEntity implements Serializable, JpaTenantIdentifier {

  private static final long serialVersionUID = 1L;

  /** 租户. */
  private TenantIdentifier tenantId;
  /** 用户ID. */
  private UUID userId = UUID.randomUUID();
  /** 角色ID. */
  private UUID roleId = UUID.randomUUID();

  /**
   * 默认构造函数.
   */
  public UsersRolesEntity() {
    super();
  }

  public UsersRolesEntity(UUID userId, UUID roleId) {
    super();
    this.userId = userId;
    this.roleId = roleId;
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UsersRolesPk getEntityPk() {
    UsersRolesPk pk = new UsersRolesPk();
    pk.setUserId(this.userId);
    pk.setRoleId(this.roleId);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(UsersRolesPk pk) {
    this.userId = pk.getUserId();
    this.roleId = pk.getRoleId();
  }

  @TenantId
  @Column(name = "tenant_id")
  public TenantIdentifier getTenantId() {
    return tenantId;
  }

  public void setTenantId(TenantIdentifier tenantId) {
    this.tenantId = tenantId;
  }

  /** 用户ID(UserId). */
  @Id
  @Column(name = "user_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }

  /** 用户ID(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /** 角色ID(RoleId). */
  @Id
  @Column(name = "role_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getRoleId() {
    return roleId;
  }

  /** 角色ID(RoleId). */
  public void setRoleId(UUID roleId) {
    this.roleId = roleId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final UsersRolesEntity other = (UsersRolesEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * UsersRolesPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class UsersRolesPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户ID. */
    @NotNull
    private UUID userId;
    /** 角色ID. */
    @NotNull
    private UUID roleId;

    public UsersRolesPk() {
      super();
    }

    public UsersRolesPk(UUID userId, UUID roleId) {
      super();
      this.userId = userId;
      this.roleId = roleId;
    }

    /** 用户ID(UserId). */
    @Column(name = "user_id", nullable = false)
    public UUID getUserId() {
      return userId;
    }

    /** 用户ID(UserId). */
    public void setUserId(UUID userId) {
      this.userId = userId;
    }

    /** 角色ID(RoleId). */
    @Column(name = "role_id", nullable = false)
    public UUID getRoleId() {
      return roleId;
    }

    /** 角色ID(RoleId). */
    public void setRoleId(UUID roleId) {
      this.roleId = roleId;
    }

    @Override
    public int hashCode() {
      return Objects.hash(roleId, userId);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      UsersRolesPk other = (UsersRolesPk) obj;
      return Objects.equals(roleId, other.roleId) && Objects.equals(userId, other.userId);
    }

  }
}

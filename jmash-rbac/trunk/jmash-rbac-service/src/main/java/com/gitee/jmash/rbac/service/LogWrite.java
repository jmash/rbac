package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.event.OperEvent;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.LogEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import jakarta.enterprise.inject.spi.EventMetadata;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

/**
 * 操作日志 rbac_log服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface LogWrite extends TenantService {

  /** 插入日志. */
  public LogEntity insert(OperEvent event, EventMetadata metadata);

  /** 根据主键删除. */
  public LogEntity delete(@NotNull LogPk entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid int number);



}


package com.gitee.jmash.rbac.enums;

/**
 * 用户密码格式.
 *
 * @author cgd
 */
public enum PwdFormat {

  /** 不加密. */
  NONE("不加密"),

  /** 国密SM3算法. */
  SM3("国密SM3"),

  /** md5加密. */
  MD5("md5加密"),

  /** sha1加密. */
  SHA1("sha1加密 "),

  /** SHA256加密. */
  SHA256("SHA-256加密 ");

  private String displayName;

  private PwdFormat(String displayName) {
    this.displayName = displayName;
  }

  public String getDisplayName() {
    return displayName;
  }
}

package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OperationEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.OperationCreateReq;
import jmash.rbac.protobuf.OperationMoveKey;
import jmash.rbac.protobuf.OperationUpdateReq;

 /**
 * 操作表 rbac_operation服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface OperationWrite extends TenantService {
		
  /** 插入实体. */
  public OperationEntity insert(@NotNull @Valid OperationCreateReq  operation);


  /** update 实体. */
  public OperationEntity update(@NotNull @Valid OperationUpdateReq  operation);


  /** 根据主键删除. */
  public OperationEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set< @NotNull UUID > entityIds);


   boolean moveOrderBy(@NotNull OperationMoveKey operationMoveKey);
 }

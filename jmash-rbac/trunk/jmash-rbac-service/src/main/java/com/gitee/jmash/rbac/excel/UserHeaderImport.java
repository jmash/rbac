package com.gitee.jmash.rbac.excel;

import com.gitee.jmash.common.excel.read.CellLocalDateReader;
import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import jmash.protobuf.Entry;
import jmash.protobuf.Gender;
import jmash.rbac.protobuf.UserStatus;

// 用户导入
public class UserHeaderImport {

  /** 规则Header. */
  public static List<HeaderField> getHeaderImports() {
    List<HeaderField> list = new ArrayList<HeaderField>();
    list.add(HeaderField.field("用户ID", "userId", true));
    list.add(HeaderField.field("目录ID", "directoryId", true));
    list.add(HeaderField.field("用户名", "loginName", true));
    list.add(HeaderField.field("手机号", "mobilePhone", true));
    list.add(HeaderField.field("电子邮件", "email", true));
    list.add(HeaderField.field("姓名", "realName", true));
    list.add(HeaderField.field("昵称", "nickName", true));
    list.add(HeaderField.field("出生日期", "birthDate", true, new CellLocalDateReader()));
    list.add(HeaderField.dict("性别", "gender", true, getGenderCodeMap()));
    list.add(HeaderField.dict("审核状态", "approved", true, getApprovedCodeMap()));
    list.add(HeaderField.dict("用户状态", "status", true, getUserStatusCodeMap()));
    return list;
  }



  /** 审核状态Map Name,Code. */
  public static Map<String, String> getApprovedCodeMap() {
    return UserHeaderExport.getApproved().stream()
        .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
  }

  /** 性别Map Name,Code. */
  public static Map<String, String> getGenderCodeMap() {
    return ProtoEnumUtil.getEnumCodeMap(Gender.class, -1);
  }

  /** 用户状态Map Name,Code.. */
  public static Map<String, String> getUserStatusCodeMap() {
    return ProtoEnumUtil.getEnumCodeMap(UserStatus.class, -1);
  }



}

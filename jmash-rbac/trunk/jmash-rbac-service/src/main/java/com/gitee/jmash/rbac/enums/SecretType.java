
package com.gitee.jmash.rbac.enums;

import java.io.Serializable;

/**
 * 密钥类型 登录.
 *
 * @author cgd
 *
 */
public enum SecretType implements Serializable {

  /** 登录. */
  Login("登录"),

  /** 支付. */
  Pay("支付");

  private String displayName;

  private SecretType(String displayName) {
    this.displayName = displayName;
  }

  public String getDisplayName() {
    return displayName;
  }
}

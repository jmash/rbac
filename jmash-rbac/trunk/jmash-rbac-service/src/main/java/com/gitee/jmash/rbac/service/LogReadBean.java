
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.rbac.dao.LogDao;
import com.gitee.jmash.rbac.entity.LogEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import com.gitee.jmash.rbac.excel.LogHeaderExport;
import com.gitee.jmash.rbac.model.LogTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import jmash.protobuf.TableHead;
import jmash.rbac.protobuf.LogExportReq;
import jmash.rbac.protobuf.LogReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 操作日志 rbac_log读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(LogRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class LogReadBean implements LogRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();

  protected LogDao logDao = new LogDao(tem);

  @PersistenceContext(unitName = "ReadRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, false);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    if (StringUtils.isNotBlank(tenant)) {
      this.tem.setTenant(tenant);
    }
    return (T) this;
  }

  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }
  
  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }

  @Override
  public String getTenant() {
    return this.tem.getTenant();
  }
  
  @Override
  public LogEntity findById(LogPk entityId) {
    return logDao.find(entityId);
  }


  @Override
  public DtoPage<LogEntity, LogTotal> findPageByReq(LogReq req) {
    return logDao.findPageByReq(req);
  }

  @Override
  public List<LogEntity> findListByReq(LogReq req) {
    return logDao.findListByReq(req);
  }

  @Override
  public String exportLog(LogExportReq request) throws FileNotFoundException, IOException {
    List<LogEntity> list = logDao.findListByReq(request.getReq());
    return exportLog(list, request.getTitle(), request.getTableHeadsList(), request.getFileName());
  }

  private String exportLog(List<LogEntity> logList, String title, List<TableHead> tableHeads,
      String fileName) throws FileNotFoundException, IOException {
    ExcelExport excelExport = new ExcelExport();
    // 页签
    excelExport.createSheet(LogHeaderExport.TITLE);
    // 增加表头
    LogHeaderExport.addHeaderExport(excelExport, tableHeads);
    // 写标题
    title = StringUtils.isBlank(title) ? LogHeaderExport.TITLE : title;
    excelExport.writeTitle(title);
    // 写表头
    excelExport.writeHeader();
    // 写数据
    excelExport.writeListData(logList);
    // 增加数据校验
    excelExport.dictDataValidation(logList.size(), 3000);
    // 写入文件
    fileName = StringUtils.isBlank(fileName) ? title : fileName;
    String fileSrc = FilePathUtil.createNewTempFile(fileName + ".xlsx", "excel");
    String realFileSrc = FilePathUtil.realPath(fileSrc);
    excelExport.getWb().write(new FileOutputStream(realFileSrc));
    return realFileSrc;
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}

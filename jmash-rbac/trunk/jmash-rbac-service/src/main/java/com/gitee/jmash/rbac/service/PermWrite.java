package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.PermEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.PermCreateReq;
import jmash.rbac.protobuf.PermUpdateReq;

/**
 * 权限表 rbac_perm服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface PermWrite extends TenantService {

  /** 插入实体. */
  public PermEntity insert(@NotNull @Valid PermCreateReq perm);

  /** update 实体. */
  public PermEntity update(@NotNull @Valid PermUpdateReq perm);

  /** 根据主键删除. */
  public PermEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

  /** 角色授予权限. */
  public Integer grantPerm(@NotNull UUID roleId, @Valid Set<@NotNull String> permCodes);

  /** 取消角色授予权限. */
  public Integer revokePerm(@NotNull UUID roleId, @Valid Set<@NotNull String> permCodes);

}

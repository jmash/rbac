
package com.gitee.jmash.rbac.entity;


import java.io.Serializable;

import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import com.gitee.jmash.rbac.entity.PermsDependEntity.PermsDependPk;
import com.gitee.jmash.rbac.enums.DependType;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;


 /**
 * 本代码为自动生成工具生成 权限依赖表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_perms_depend")
@IdClass(PermsDependPk.class)
public class PermsDependEntity  extends CreateEntity<PermsDependPk>  {

  private static final long serialVersionUID = 1L;
  
  /** 权限编码. */
  private String permCode ;
  /** 依赖权限编码. */
  private String dependPermCode ;
  /** 依赖类型. */
  private DependType dependType ;

  /**
  * 默认构造函数.
  */
  public PermsDependEntity(){
    super();
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public PermsDependPk  getEntityPk(){
    PermsDependPk pk=new PermsDependPk();
    pk.setDependPermCode(this.dependPermCode);
    pk.setPermCode(this.permCode);
    return pk;
  }
  /** 实体主键. */
  public void setEntityPk(PermsDependPk  pk){
    this.dependPermCode=pk.getDependPermCode();
    this.permCode=pk.getPermCode();
  }
  
   /** 权限编码(PermCode). */
  @Id

  @Column(name="perm_code",nullable = false)
  public String getPermCode() {
    return permCode;
  }
  /** 权限编码(PermCode). */
  public void setPermCode(String permCode) {
    this.permCode = permCode;
  }
   /** 依赖权限编码(DependPermCode). */
  @Id

  @Column(name="depend_perm_code",nullable = false)
  public String getDependPermCode() {
    return dependPermCode;
  }
  /** 依赖权限编码(DependPermCode). */
  public void setDependPermCode(String dependPermCode) {
    this.dependPermCode = dependPermCode;
  }
   /** 依赖类型(DependType). */

  @Column(name="depend_type",nullable = false)
  @Enumerated(EnumType.STRING)
  public DependType getDependType() {
    return dependType;
  }
  /** 依赖类型(DependType). */
  public void setDependType(DependType dependType) {
    this.dependType = dependType;
  }
	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PermsDependEntity other = (PermsDependEntity) obj;
	if (getEntityPk() == null) {
		if (other.getEntityPk() != null){
			return false;
		}
	} else if (!getEntityPk().equals(other.getEntityPk())){
		return false;
	}
    return true;
  }
  
  
  /**
   * PermsDependPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class PermsDependPk implements Serializable{
  
    private static final long serialVersionUID = 1L;
    
    /** 依赖权限编码. */
    @NotNull
    private String dependPermCode;
    /** 权限编码. */
    @NotNull
    private String permCode;
    
    /** 依赖权限编码(DependPermCode). */
	@Column(name="depend_perm_code",nullable = false)
    public String getDependPermCode() {
      return dependPermCode;
    }
    
    /** 依赖权限编码(DependPermCode). */
    public void setDependPermCode(String dependPermCode) {
      this.dependPermCode = dependPermCode;
    }
    /** 权限编码(PermCode). */
	@Column(name="perm_code",nullable = false)
    public String getPermCode() {
      return permCode;
    }
    
    /** 权限编码(PermCode). */
    public void setPermCode(String permCode) {
      this.permCode = permCode;
    }
      
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result
          + ((dependPermCode == null) ? 0 : dependPermCode.hashCode());
      result = prime * result
          + ((permCode == null) ? 0 : permCode.hashCode());
      return result;
    }
  
    @Override
    public boolean equals(Object obj) {
      if (this == obj){
        return true;
      }
      if (obj == null){
        return false;
      }
      if (getClass() != obj.getClass()){
        return false;
      }
      final PermsDependPk other = (PermsDependPk) obj;
      
      if (dependPermCode == null) {
        if (other.dependPermCode != null){
          return false;
        }
      } else if (!dependPermCode.equals(other.dependPermCode)){
        return false;
      }
      if (permCode == null) {
        if (other.permCode != null){
          return false;
        }
      } else if (!permCode.equals(other.permCode)){
        return false;
      }
      return true;
    }
  }

}


package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.TreeEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;
import jmash.rbac.protobuf.ResourceType;


/**
 * 本代码为自动生成工具生成 资源表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_resource")
public class ResourceEntity implements TreeEntity<UUID> {

  private static final long serialVersionUID = 1L;

  /** 资源ID. */
  private UUID resourceId = UUID.randomUUID();
  /** 模块ID. */
  private UUID moduleId;
  /** 资源编码. */
  private String resourceCode;
  /** 资源名称. */
  private String resourceName;
  /** 资源类型. */
  private ResourceType resourceType;
  /** 父资源ID. */
  private UUID parentId;
  /** 深度. */
  private Integer depth;
  /** 排序. */
  private Integer orderBy;
  /** 目标. */
  private String target;
  /** URL. */
  private String url;
  /** 是否隐藏. */
  private Boolean hidden = false;
  /** 图标. */
  private String icon;

  /**
   * 默认构造函数.
   */
  public ResourceEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.resourceId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.resourceId = pk;
  }

  /** 资源ID(ResourceId). */
  @Id
  @Column(name = "resource_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getResourceId() {
    return resourceId;
  }

  /** 资源ID(ResourceId). */
  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }

  /** 模块ID(ModuleId). */
  @Column(name = "module_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getModuleId() {
    return moduleId;
  }

  /** 模块ID(ModuleId). */
  public void setModuleId(UUID moduleId) {
    this.moduleId = moduleId;
  }

  /** 资源编码(ResourceCode). */

  @Column(name = "resource_code", nullable = false)
  public String getResourceCode() {
    return resourceCode;
  }

  /** 资源编码(ResourceCode). */
  public void setResourceCode(String resourceCode) {
    this.resourceCode = resourceCode;
  }

  /** 资源名称(ResourceName). */

  @Column(name = "resource_name", nullable = false)
  public String getResourceName() {
    return resourceName;
  }

  /** 资源名称(ResourceName). */
  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  /** 资源类型(ResourceType). */

  @Column(name = "resource_type", nullable = false)
  @Enumerated(EnumType.STRING)
  public ResourceType getResourceType() {
    return resourceType;
  }

  /** 资源类型(ResourceType). */
  public void setResourceType(ResourceType resourceType) {
    this.resourceType = resourceType;
  }

  /** 父资源ID(ParentId). */
  @Column(name = "parent_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getParentId() {
    return parentId;
  }

  /** 父资源ID(ParentId). */
  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  /** 深度(Depth). */

  @Column(name = "depth_", nullable = false)
  public Integer getDepth() {
    return depth;
  }

  /** 深度(Depth). */
  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  /** 排序(OrderBy). */

  @Column(name = "order_by", nullable = false)
  public Integer getOrderBy() {
    return orderBy;
  }

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  /** 目标(Target). */
  @Column(name = "target_")
  public String getTarget() {
    return target;
  }

  /** 目标(Target). */
  public void setTarget(String target) {
    this.target = target;
  }

  /** URL(Url). */

  @Column(name = "url_", nullable = false)
  public String getUrl() {
    return url;
  }

  /** URL(Url). */
  public void setUrl(String url) {
    this.url = url;
  }

  /** 是否隐藏(Hidden). */

  @Column(name = "hidden_", nullable = false)
  public Boolean getHidden() {
    return hidden;
  }

  /** 是否隐藏(Hidden). */
  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  /** 图标. */
  @Column(name = "icon_", nullable = false)
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((resourceId == null) ? 0 : resourceId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ResourceEntity other = (ResourceEntity) obj;
    if (resourceId == null) {
      if (other.resourceId != null) {
        return false;
      }
    } else if (!resourceId.equals(other.resourceId)) {
      return false;
    }
    return true;
  }

}

package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OperationEntity;
import com.gitee.jmash.rbac.model.OperationTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import jmash.rbac.protobuf.OperationReq;

 /**
 * 操作表 rbac_operation服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface OperationRead extends TenantService {

  /** 根据主键查询. */
  public OperationEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<OperationEntity,OperationTotal> findPageByReq(@NotNull @Valid OperationReq req);

  /** 综合查询. */
  public List<OperationEntity> findListByReq(@NotNull @Valid OperationReq req);


  /** 校验操作编码是否唯一. */
  public boolean checkOperationCodeUnique(String operationCode);
 }

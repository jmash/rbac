
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.UserLogEntity;
import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import com.gitee.jmash.rbac.model.UserLogCreateReq;
import com.gitee.jmash.rbac.model.UserLogTotal;
import java.util.List;
import jmash.rbac.protobuf.UserLogKey;
import jmash.rbac.protobuf.UserLogModel;
import jmash.rbac.protobuf.UserLogPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * UserLogMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface UserLogMapper extends BeanMapper, ProtoMapper {

  UserLogMapper INSTANCE = Mappers.getMapper(UserLogMapper.class);


  List<UserLogModel> listUserLog(List<UserLogEntity> list);

  UserLogPage pageUserLog(DtoPage<UserLogEntity, UserLogTotal> page);

  UserLogModel model(UserLogEntity entity);

  UserLogPk pk(UserLogKey key);

  UserLogKey key(UserLogModel model);
  
  UserLogEntity create(UserLogCreateReq req);

  UserLogEntity clone(UserLogEntity entity);
}

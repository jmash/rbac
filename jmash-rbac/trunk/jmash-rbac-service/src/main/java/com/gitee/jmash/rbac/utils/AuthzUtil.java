package com.gitee.jmash.rbac.utils;

import com.gitee.jmash.common.grpc.GrpcContext;
import com.gitee.jmash.common.security.JmashPrincipal;
import com.gitee.jmash.common.tree.Node;
import com.gitee.jmash.common.tree.Tree;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.rbac.RbacFactory;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.entity.PermEntity;
import com.gitee.jmash.rbac.entity.ResourceEntity;
import com.gitee.jmash.rbac.entity.RoleEntity;
import com.gitee.jmash.rbac.service.ModuleRead;
import com.gitee.jmash.rbac.service.PermRead;
import com.gitee.jmash.rbac.service.RoleRead;
import com.gitee.jmash.rbac.service.UserRead;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import jmash.rbac.protobuf.ResourceType;
import jmash.rbac.protobuf.RolePermSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.eclipse.microprofile.jwt.JsonWebToken;

/** 用户授权 */
public class AuthzUtil {

  /** 获取用户拥有的菜单资源。 */
  public static Tree<ResourceEntity, UUID> userMenuResource(Tree<ResourceEntity, UUID> tree) {
    Subject subject = (Subject) GrpcContext.USER_SUBJECT.get();
    Map<UUID, ModuleEntity> map = new HashMap<>();
    Node<ResourceEntity, UUID> root = tree.getRoot();
    JsonWebToken webToken = (JsonWebToken) subject.getPrincipal();
    filter(webToken.getIssuer(), subject, map, root);
    return tree;
  }

  /** 获取用户拥有的菜单资源。 */
  public static void filter(String tenant, Subject subject, Map<UUID, ModuleEntity> map,
      Node<ResourceEntity, UUID> parent) {
    if (parent.isLeaf()) {
      return;
    }
    // 检查孩子权限
    List<Node<ResourceEntity, UUID>> childs = parent.getChilds();
    for (int i = childs.size() - 1; i >= 0; i--) {
      Node<ResourceEntity, UUID> node = childs.get(i);
      ResourceEntity entity = node.getEntity();
      // 移除其他
      if (ResourceType.other.equals(entity.getResourceType())) {
        node.remove();
        continue;
      }
      // 菜单检查权限.
      if (ResourceType.menu.equals(entity.getResourceType())) {
        String permCode = getReourcePermCode(tenant, map, entity);
        // 没有权限.
        if (!subject.isPermitted(permCode)) {
          node.remove();
          continue;
        }
      }
      // 深度遍历，检查孩子的孩子
      filter(tenant, subject, map, node);
      // 检查孩子.
      if (ResourceType.catalog.equals(entity.getResourceType())) {
        if (node.isLeaf()) {
          node.remove();
        }
      }
    }
  }

  /** 获取资源权限。 */
  public static String getReourcePermCode(String tenant, Map<UUID, ModuleEntity> map,
      ResourceEntity entity) {
    ModuleEntity module = null;
    if (map.containsKey(entity.getModuleId())) {
      module = map.get(entity.getModuleId());
    } else {
      try (ModuleRead moduleRead = RbacFactory.getModuleRead(tenant)) {
        module = moduleRead.findById(entity.getModuleId());
        map.put(module.getModuleId(), module);
      } catch (Exception ex) {
        return "*";
      }
    }
    String codePrefix = PermEntity.permCodePrefix(module.getModuleCode(), entity.getResourceCode());
    return codePrefix + "list";
  }


  /** 获取当前登录用户授权. */
  public static RolePermSet userRolesPerms(String tenant, JmashPrincipal principal)
      throws Exception {
    // 系统用户
    if (StringUtils.equals(principal.getName(), UUIDUtil.emptyUUID32())) {
      return RolePermSet.newBuilder().build();
    }
    // 其他用户.
    try (UserRead userRead = RbacFactory.getUserRead(tenant);
        RoleRead roleRead = RbacFactory.getRoleRead(tenant);
        PermRead permRead = RbacFactory.getPermRead(tenant)) {
      RolePermSet.Builder rolePermSet = RolePermSet.newBuilder();
      Set<String> scopes = principal.getScope();
      Map<UUID, String> roleMap = roleRead.findUserRoleMap(principal.getNameUUID(), scopes);

      rolePermSet.addAllRoleCodes(roleMap.values());
      Set<String> perms = permRead.findUserPerms(roleMap.keySet());
      if (roleMap.values().stream().collect(Collectors.toSet()).contains(RoleEntity.ADMIN)
          || roleMap.values().stream().collect(Collectors.toSet())
              .contains(RoleEntity.ADMINISTRATOR)) {
        perms.add("*");
      }
      rolePermSet.addAllPermCodes(perms);
      return rolePermSet.build();
    }
  }



}

package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.LogEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import com.gitee.jmash.rbac.model.LogTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import jmash.rbac.protobuf.LogExportReq;
import jmash.rbac.protobuf.LogReq;

 /**
 * 操作日志 rbac_log服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface LogRead extends TenantService{

  /** 根据主键查询. */
  public LogEntity findById(@NotNull LogPk entityId);

  /** 查询页信息. */
  public DtoPage<LogEntity,LogTotal> findPageByReq(@NotNull @Valid LogReq req);

  /** 综合查询. */
  public List<LogEntity> findListByReq(@NotNull @Valid LogReq req);

  /** 导出日志. */
  public String exportLog(LogExportReq request) throws FileNotFoundException, IOException;
 }

/*!40101 SET NAMES utf8 */;

CREATE TABLE rbac_user(
	`tenant_id` VARCHAR(32) NOT NULL COMMENT '租户' ,
    `user_id` BINARY(16) NOT NULL   COMMENT '账号' ,
    `unified_id`  VARCHAR(32) NULL COMMENT '统一身份ID',
    `directory_id` VARCHAR(30) NOT NULL   COMMENT '目录ID' ,
    `login_name` VARCHAR(60)    COMMENT '用户名/登录名' ,
    `mobile_phone` VARCHAR(120)    COMMENT '手机号' ,
    `mobile_phone_ins` VARCHAR(15)    COMMENT '手机号脱敏' ,
    `email_` VARCHAR(120)    COMMENT '电子邮件' ,
    `email_ins` VARCHAR(50)    COMMENT '电子邮件脱敏' ,
    `real_name` VARCHAR(120)    COMMENT '姓名' ,
    `real_name_ins` VARCHAR(30)    COMMENT '姓名脱敏' ,
    `nick_name` VARCHAR(60)    COMMENT '昵称' ,
    `avatar_` VARCHAR(254)    COMMENT '头像' ,
    `birth_date` DATE    COMMENT '出生日期' ,
    `gender_` INT    COMMENT '性别' ,
    `phone_approved` TINYINT NOT NULL   COMMENT '手机通过审核' ,
    `email_approved` TINYINT NOT NULL   COMMENT '邮箱通过审核' ,
    `approved_` TINYINT NOT NULL   COMMENT '用户通过审核' ,
    `status_` VARCHAR(30) NOT NULL   COMMENT '用户状态$启用、禁用、锁定' ,
    `storage_` varchar(30) NULL COMMENT '个人信息存储区',
    `last_lockout_time` TIMESTAMP    COMMENT '上次被锁/禁用时间' ,
    `failed_time` TIMESTAMP    COMMENT '登录失败时间' ,
    `failed_count` INT NOT NULL   COMMENT '登录失败次数' ,
    `last_login_time` TIMESTAMP    COMMENT '上次登录时间' ,
    `version_` INT NOT NULL   COMMENT '乐观锁' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_by` BINARY(16)    COMMENT '更新人' ,
    `update_time` TIMESTAMP   COMMENT '更新时间' ,
    `delete_by` BINARY(16)    COMMENT '删除人' ,
    `deleted_` TINYINT NOT NULL   COMMENT '删除状态' ,
    `delete_time` TIMESTAMP    COMMENT '删除时间' ,
    PRIMARY KEY (user_id)
)  COMMENT = '用户' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_user_secret(
    `secret_id` BINARY(16) NOT NULL   COMMENT '密钥ID' ,
    `user_id` BINARY(16) NOT NULL   COMMENT '用户' ,
    `secret_type` VARCHAR(30) NOT NULL   COMMENT '密钥类型' ,
    `pwd_value` VARCHAR(254) NOT NULL   COMMENT '密钥' ,
    `pwd_format` VARCHAR(30) NOT NULL   COMMENT '密钥格式' ,
    `pwd_alt` VARCHAR(32)    COMMENT '随机ALT' ,
    `is_temp` TINYINT NOT NULL   COMMENT '是否临时密钥' ,
    `version_` INT NOT NULL   COMMENT '乐观锁' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_by` BINARY(16)    COMMENT '更新人' ,
    `update_time` TIMESTAMP   COMMENT '更新时间' ,
    PRIMARY KEY (secret_id)
)  COMMENT = '用户密钥' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_user_log(
    `log_id` BINARY(16) NOT NULL   COMMENT '日志' ,
    `partition_` INT NOT NULL   COMMENT '表分区' ,
    `log_name` VARCHAR(30) NOT NULL   COMMENT '操作类型$登录、注销、授权' ,
    `log_msg` VARCHAR(254) NOT NULL   COMMENT '操作内容/日志信息' ,
    `env_props` TEXT    COMMENT '环境参数' ,
    `device_id` VARCHAR(60) NOT NULL   COMMENT '设备标识ID' ,
    `user_ip` VARCHAR(30) NOT NULL   COMMENT '用户IP' ,
    `proxy_ip` VARCHAR(126) NOT NULL   COMMENT '代理IP' ,
    `create_by` BINARY(16) NOT NULL   COMMENT '创建人/用户ID' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间/操作时间' ,
    PRIMARY KEY (log_id,partition_)
)  COMMENT = '安全日志' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_opens(
    `open_id` VARCHAR(120) NOT NULL   COMMENT 'OpenID' ,
    `user_id` BINARY(16) NOT NULL   COMMENT '账号ID' ,
    `app_id` VARCHAR(60) NOT NULL   COMMENT '应用ID' ,
    `union_id` VARCHAR(60)    COMMENT '唯一ID' ,
    `nick_name` VARCHAR(120)    COMMENT '昵称' ,
    `open_type` VARCHAR(30)    COMMENT '三方OpenID Type' ,
    `version_` INT NOT NULL   COMMENT '乐观锁' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_by` BINARY(16)    COMMENT '更新人' ,
    `update_time` TIMESTAMP   COMMENT '更新时间' ,
    PRIMARY KEY (open_id,user_id,app_id)
)  COMMENT = '三方登录' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_open_app
(
    `open_app_id`  BINARY(16) NOT NULL COMMENT '开放应用ID',
    `app_id`       VARCHAR(60)  NOT NULL COMMENT '应用ID',
    `app_name`     VARCHAR(30) COMMENT '应用名称',
    `open_type`    VARCHAR(30)  NOT NULL COMMENT '三方OpenID Type',
    `app_secret`   VARCHAR(510) NOT NULL COMMENT '密钥(加密存储)',
    `description_` VARCHAR(1022) COMMENT '描述',
    PRIMARY KEY (open_app_id)
) COMMENT = '开发平台应用' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_log(
    `log_id` BINARY(16) NOT NULL   COMMENT '日志' ,
    `partition_` INT NOT NULL   COMMENT '表分区' ,
    `log_name` VARCHAR(120) NOT NULL   COMMENT '日志名称' ,
    `log_level` VARCHAR(10) NOT NULL   COMMENT '日志级别' ,
    `log_msg` VARCHAR(254) NOT NULL   COMMENT '日志信息' ,
    `env_props` TEXT    COMMENT '环境参数' ,
    `log_content` LONGTEXT      COMMENT '日志内容' ,
    `user_ip` VARCHAR(30)   COMMENT '用户IP' ,
    `proxy_ip` VARCHAR(126) COMMENT '代理IP' ,
    `create_by` BINARY(16) NOT NULL   COMMENT '创建人/用户ID' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间/操作时间' ,
    PRIMARY KEY (log_id,partition_)
)  COMMENT = '操作日志' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_token(
    `user_id` BINARY(16) NOT NULL   COMMENT '账号ID' ,
    `client_id` VARCHAR(30) NOT NULL   COMMENT '客户端ID' ,
    `redirect_uri` VARCHAR(255)    COMMENT '重定向URI' ,
    `issuer_` VARCHAR(60)    COMMENT '发行机构' ,
    `subject_` VARCHAR(30)    COMMENT '认证主体' ,
    `authorization_code` VARCHAR(30)    COMMENT '授权码' ,
    `code_expire_time` TIMESTAMP    COMMENT '授权码过期时间' ,
    `expires_in` INT    COMMENT '过期秒数' ,
    `expire_time` TIMESTAMP    COMMENT '过期时间' ,
    `access_token` VARCHAR(1790)    COMMENT '访问Token' ,
    `refresh_token` VARCHAR(120)    COMMENT '刷新Token' ,
    `token_type` VARCHAR(30)    COMMENT 'Token类型' ,
    `scope_` VARCHAR(255)    COMMENT '授权范围' ,
    `version_` INT NOT NULL   COMMENT '乐观锁' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_by` BINARY(16)    COMMENT '更新人' ,
    `update_time` TIMESTAMP NULL   COMMENT '更新时间' ,
    PRIMARY KEY (user_id,client_id)
)  COMMENT = '令牌' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_dept(
	`tenant_id` VARCHAR(32) NOT NULL COMMENT '租户' ,
    `dept_id` BINARY(16) NOT NULL   COMMENT '部门ID' ,
    `dept_code` VARCHAR(60) NOT NULL   COMMENT '部门编码' ,
    `dept_name` VARCHAR(90) NOT NULL   COMMENT '部门名称' ,
    `dept_type` VARCHAR(60) NOT NULL   COMMENT '部门类型' ,
    `parent_id` BINARY(16) NOT NULL   COMMENT '父部门' ,
    `description_` VARCHAR(1022) NOT NULL   COMMENT '部门描述' ,
    `depth_` INT NOT NULL   COMMENT '深度' ,
    `order_by` INT NOT NULL   COMMENT '排序' ,
    `status_` TINYINT NOT NULL   COMMENT '状态$启用/禁用' ,
    `is_open` TINYINT NOT NULL   COMMENT '是否开放访问' ,
    `version_` INT NOT NULL   COMMENT '乐观锁' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    `update_by` BINARY(16)    COMMENT '更新人' ,
    `update_time` TIMESTAMP NULL   COMMENT '更新时间' ,
    PRIMARY KEY (dept_id)
)  COMMENT = '组织机构' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_role(
    `role_id` BINARY(16) NOT NULL   COMMENT '角色ID/职务ID' ,
    `role_code` VARCHAR(30) NOT NULL   COMMENT '角色编码/职务编码' ,
    `role_name` VARCHAR(90) NOT NULL   COMMENT '角色名称/职务名称' ,
    `role_type` VARCHAR(30) NOT NULL   COMMENT '角色/职务' ,
    `parent_id` BINARY(16) NOT NULL   COMMENT '父角色' ,
    `depth_` INT NOT NULL   COMMENT '深度' ,
    `order_by` INT NOT NULL   COMMENT '排序' ,
    `description_` VARCHAR(1022)    COMMENT '描述' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    PRIMARY KEY (role_id)
)  COMMENT = '角色/职务表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_perm(
    `perm_id` BINARY(16) NOT NULL   COMMENT '权限ID' ,
    `perm_code` VARCHAR(126) NOT NULL   COMMENT '权限编码' ,
    `perm_name` VARCHAR(90) NOT NULL   COMMENT '权限名称' ,
    PRIMARY KEY (perm_id)
)  COMMENT = '权限表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_resource(
    `resource_id` BINARY(16) NOT NULL   COMMENT '资源ID' ,
    `module_id` BINARY(16) NOT NULL   COMMENT '模块ID' ,
    `resource_code` VARCHAR(60) NOT NULL   COMMENT '资源编码' ,
    `resource_name` VARCHAR(90) NOT NULL   COMMENT '资源名称' ,
    `resource_type` VARCHAR(30) NOT NULL   COMMENT '资源类型' ,
    `parent_id` BINARY(16) NOT NULL   COMMENT '父资源ID' ,
    `depth_` INT NOT NULL   COMMENT '深度' ,
    `order_by` INT NOT NULL   COMMENT '排序' ,
    `target_` VARCHAR(255)    COMMENT '目标' ,
    `url_` VARCHAR(254) NOT NULL   COMMENT 'URL' ,
    `hidden_` TINYINT NOT NULL   COMMENT '是否隐藏' ,
    `icon_` varchar(30) NULL  COMMENT '图标' ,
    PRIMARY KEY (resource_id)
)  COMMENT = '资源表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_operation(
    `operation_id` BINARY(16) NOT NULL   COMMENT '操作ID' ,
    `operation_code` VARCHAR(30) NOT NULL   COMMENT '操作编码' ,
    `operation_name` VARCHAR(90) NOT NULL   COMMENT '操作名称' ,
    `order_by` INT   COMMENT '排序' ,
    PRIMARY KEY (operation_id)
)  COMMENT = '操作表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_roles_perms(
    `role_id` BINARY(16) NOT NULL   COMMENT '角色ID' ,
    `perm_id` BINARY(16) NOT NULL   COMMENT '权限ID' ,
    PRIMARY KEY (role_id,perm_id)
)  COMMENT = '角色权限表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_module(
    `module_id` BINARY(16) NOT NULL   COMMENT '模块ID' ,
    `module_code` VARCHAR(30) NOT NULL   COMMENT '模块编码' ,
    `module_name` VARCHAR(90) NOT NULL   COMMENT '模块名称' ,
    `order_by` INT NOT NULL   COMMENT '模块排序' ,
    `description_` VARCHAR(1022)    COMMENT '模块描述' ,
    PRIMARY KEY (module_id)
)  COMMENT = '系统模块' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_perms_depend(
    `perm_code` VARCHAR(60) NOT NULL   COMMENT '权限编码' ,
    `depend_perm_code` VARCHAR(60) NOT NULL   COMMENT '依赖权限编码' ,
    `depend_type` VARCHAR(30) NOT NULL   COMMENT '依赖类型$frontend、backend' ,
    `create_by` BINARY(16)    COMMENT '创建人' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    PRIMARY KEY (perm_code,depend_perm_code)
)  COMMENT = '权限依赖表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_users_jobs(
	`tenant_id` VARCHAR(32) NOT NULL COMMENT '租户' ,
    `user_id` BINARY(16) NOT NULL   COMMENT '账号' ,
    `dept_id` BINARY(16) NOT NULL   COMMENT '部门ID' ,
    `job_id` BINARY(16) NOT NULL   COMMENT '岗位ID' ,
    `order_by` INT NOT NULL   COMMENT '排序' ,
    `start_time` DATETIME NOT NULL   COMMENT '开始时间' ,
    `end_time` DATETIME    COMMENT '结束时间' ,
    `status_` TINYINT NOT NULL   COMMENT '状态$启用/禁用' ,
    PRIMARY KEY (user_id,dept_id,job_id)
)  COMMENT = '用户部门职位' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_roles_duty(
    `src_role_id` BINARY(16) NOT NULL   COMMENT '职责1' ,
    `desc_role_id` BINARY(16) NOT NULL   COMMENT '职责2' ,
    `duty_type` VARCHAR(30)    COMMENT '职责分离类型$SSD（静态）、DSD（动态）' ,
    PRIMARY KEY (src_role_id,desc_role_id)
)  COMMENT = '职责分离' ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE rbac_users_roles(
	`tenant_id` VARCHAR(32) NOT NULL COMMENT '租户' ,
    `user_id` BINARY(16) NOT NULL   COMMENT '用户ID' ,
    `role_id` BINARY(16) NOT NULL   COMMENT '角色ID' ,
    PRIMARY KEY (user_id,role_id)
)  COMMENT = '用户角色表' ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*==============================================================*/
/* 外键                                                         */
/*==============================================================*/

ALTER TABLE rbac_user_secret ADD CONSTRAINT FK_rbac_user_secret_1  FOREIGN KEY (user_id) REFERENCES rbac_user (user_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_opens ADD CONSTRAINT FK_rbac_opens_1  FOREIGN KEY (user_id) REFERENCES rbac_user (user_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_token ADD CONSTRAINT FK_rbac_token_1  FOREIGN KEY (user_id) REFERENCES rbac_user (user_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_resource ADD CONSTRAINT FK_rbac_resource_1  FOREIGN KEY (module_id) REFERENCES rbac_module (module_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_roles_perms ADD CONSTRAINT FK_rbac_roles_perms_1  FOREIGN KEY (role_id) REFERENCES rbac_role (role_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE rbac_roles_perms ADD CONSTRAINT FK_rbac_roles_perms_2  FOREIGN KEY (perm_id) REFERENCES rbac_perm (perm_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_users_jobs ADD CONSTRAINT FK_rbac_users_jobs_1  FOREIGN KEY (user_id) REFERENCES rbac_user (user_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE rbac_users_jobs ADD CONSTRAINT FK_rbac_users_jobs_2  FOREIGN KEY (job_id) REFERENCES rbac_role (role_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE rbac_users_jobs ADD CONSTRAINT FK_rbac_users_jobs_3  FOREIGN KEY (dept_id) REFERENCES rbac_dept (dept_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE rbac_users_roles ADD CONSTRAINT FK_rbac_users_roles_1  FOREIGN KEY (user_id) REFERENCES rbac_user (user_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE rbac_users_roles ADD CONSTRAINT FK_rbac_users_roles_2  FOREIGN KEY (role_id) REFERENCES rbac_role (role_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

/*==============================================================*/
/* 索引                                                         */
/*==============================================================*/

create index index_rbac_user_1 on rbac_user ( directory_id , login_name );
create index index_rbac_user_2 on rbac_user ( directory_id , mobile_phone );
create index index_rbac_user_3 on rbac_user ( directory_id , email_ );
create index index_rbac_user_4 on rbac_user ( deleted_ , status_  );

create index index_rbac_user_secret_1 on rbac_user_secret ( user_id , secret_type );

create index index_rbac_user_log_1 on rbac_user_log ( create_time );
create index index_rbac_user_log_2 on rbac_user_log ( partition_ , create_time , log_name );

create index index_rbac_opens_1 on rbac_opens ( user_id );
create index index_rbac_opens_2 on rbac_opens ( open_id , app_id );
create index index_rbac_opens_3 on rbac_opens ( union_id , app_id );

create index index_rbac_log_1 on rbac_log ( create_time );
create index index_rbac_log_2 on rbac_log ( partition_ , create_time , log_level , log_name );

create index index_rbac_token_1 on rbac_token ( client_id ,  authorization_code );
create index index_rbac_token_2 on rbac_token ( access_token );
create index index_rbac_token_3 on rbac_token ( client_id ,  refresh_token );

create index index_rbac_dept_1 on rbac_dept ( parent_id );

create index index_rbac_role_1 on rbac_role ( role_type, parent_id );
create index index_rbac_role_2 on rbac_role ( role_code );

create index index_rbac_perm_1 on rbac_perm ( perm_code );

create index index_rbac_resource_1 on rbac_resource ( parent_id );
create index index_rbac_resource_2 on rbac_resource ( module_id );

create index index_rbac_operation_1 on rbac_operation ( operation_code );

create index index_rbac_roles_perms_1 on rbac_roles_perms ( role_id );
create index index_rbac_roles_perms_2 on rbac_roles_perms ( perm_id );

create index index_rbac_module_1 on rbac_module ( module_code );

create index index_rbac_perms_depend_1 on rbac_perms_depend ( depend_perm_code,depend_type );
create index index_rbac_perms_depend_2 on rbac_perms_depend ( perm_code,depend_type );

create index index_rbac_users_jobs_1 on rbac_users_jobs ( user_id , status_ );
create index index_rbac_users_jobs_2 on rbac_users_jobs ( dept_id , status_ );
create index index_rbac_users_jobs_3 on rbac_users_jobs ( job_id , status_ );

create index index_rbac_roles_duty_1 on rbac_roles_duty ( src_role_id );
create index index_rbac_roles_duty_2 on rbac_roles_duty ( desc_role_id );
create index index_rbac_roles_duty_3 on rbac_roles_duty ( duty_type );

create index index_rbac_users_roles_1 on rbac_users_roles ( user_id );
create index index_rbac_users_roles_2 on rbac_users_roles ( role_id );






















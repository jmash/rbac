/*!40101 SET NAMES utf8 */;

ALTER TABLE rbac_operation ADD order_by INT;
update rbac_operation set order_by = 1 ;

ALTER TABLE rbac_resource ADD icon_ varchar(30);
/*!40101 SET NAMES utf8 */;
ALTER TABLE rbac_user ADD tenant_id VARCHAR(32) NULL COMMENT '租户' FIRST ;
ALTER TABLE rbac_dept ADD tenant_id VARCHAR(32) NULL COMMENT '租户' FIRST ;

update rbac_user set tenant_id = 'default' where tenant_id is null; 
update rbac_dept set tenant_id = 'default' where tenant_id is null; 
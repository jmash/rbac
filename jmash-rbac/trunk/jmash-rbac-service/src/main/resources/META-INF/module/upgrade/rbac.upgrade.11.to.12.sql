/*!40101 SET NAMES utf8 */;
update rbac_user set tenant_id = 'default' where tenant_id =''; 
update rbac_dept set tenant_id = 'default' where tenant_id =''; 

ALTER TABLE rbac_user ADD unified_id VARCHAR(32) NULL COMMENT '统一身份ID' after user_id ;
update rbac_user set unified_id = '' where unified_id is null; 

ALTER TABLE rbac_users_roles ADD tenant_id VARCHAR(32) NULL COMMENT '租户' FIRST ;
ALTER TABLE rbac_users_jobs ADD tenant_id VARCHAR(32) NULL COMMENT '租户' FIRST ;

update rbac_users_roles set tenant_id = 'default' where tenant_id is null; 
update rbac_users_jobs set tenant_id = 'default' where tenant_id is null; 






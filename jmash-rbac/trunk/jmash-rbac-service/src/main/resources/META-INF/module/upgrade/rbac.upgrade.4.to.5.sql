/*!40101 SET NAMES utf8 */;
DROP TABLE IF EXISTS rbac_opens_temp;

DROP TABLE IF EXISTS rbac_open_temp;
CREATE TABLE rbac_open_temp(
    `app_id` VARCHAR(36) NOT NULL   COMMENT '应用ID' ,
    `open_id` VARCHAR(36) NOT NULL   COMMENT 'OpenID' ,
    `union_id` VARCHAR(36)    COMMENT '唯一ID' ,
    `mobile_phone` VARCHAR(126)    COMMENT '手机号' ,
    `mobile_phone_ins` VARCHAR(15)    COMMENT '手机号脱敏' ,
    `create_time` TIMESTAMP NOT NULL   COMMENT '创建时间' ,
    PRIMARY KEY (app_id,open_id)
)  COMMENT = '开放平台手机号表';



ALTER TABLE rbac_user
MODIFY COLUMN mobile_phone VARCHAR(120) COMMENT '手机号',
MODIFY COLUMN email_ VARCHAR(120) COMMENT '电子邮件',
MODIFY COLUMN real_name VARCHAR(120) COMMENT '姓名';

ALTER TABLE rbac_user ADD COLUMN email_ins VARCHAR(50) COMMENT '电子邮件脱敏' after email_;
ALTER TABLE rbac_user ADD COLUMN real_name_ins VARCHAR(30) COMMENT '姓名脱敏' after real_name;
ALTER TABLE rbac_user ADD COLUMN mobile_phone_ins VARCHAR(15) COMMENT '手机号脱敏' after mobile_phone;

ALTER TABLE rbac_token
MODIFY COLUMN `access_token` VARCHAR(1790)    COMMENT '访问Token',

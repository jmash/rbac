
package com.gitee.jmash.rbac.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.google.api.HttpBody;
import com.google.protobuf.FieldMask;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.ResourceCreateReq;
import jmash.rbac.protobuf.ResourceExportReq;
import jmash.rbac.protobuf.ResourceImportReq;
import jmash.rbac.protobuf.ResourceKey;
import jmash.rbac.protobuf.ResourceList;
import jmash.rbac.protobuf.ResourceModel;
import jmash.rbac.protobuf.ResourceMoveKey;
import jmash.rbac.protobuf.ResourcePermList;
import jmash.rbac.protobuf.ResourceReq;
import jmash.rbac.protobuf.ResourceType;
import jmash.rbac.protobuf.TreeList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class ResourceTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findResourcePermList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final ResourceReq.Builder request = ResourceReq.newBuilder();
    request.setTenant(TENANT);
    final ResourcePermList modelPage = rbacStub.findResourcePermList(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void findList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final ResourceReq.Builder request = ResourceReq.newBuilder();
    request.setTenant(TENANT);
    final ResourceList modelList = rbacStub.findResourceList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(3)
  public void findTreeList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final ResourceReq.Builder request = ResourceReq.newBuilder();
    request.setTenant(TENANT);
    request.setExcludeId("174ea4aa-0bdd-45bf-8d13-59164600c651");
    final TreeList modelList = rbacStub.findResourceTreeList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(4)
  public void testUrl() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    // 创建
    ResourceCreateReq.Builder req = ResourceCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setModuleId("C415075752D14BFAA606521EAAE11C5F");
    req.setUrl("test");
    req.setResourceCode("test");
    req.setResourceName("测试管理");
    req.setIcon("test");
    req.setResourceType(ResourceType.catalog);
    ResourceModel entity = rbacStub.createResource(req.build());
    Assertions.assertEquals("/test", entity.getUrl());

    moveOrderByUp(entity.getResourceId());
    moveOrderByDown(entity.getResourceId());
    // Delete
    ResourceKey pk =
        ResourceKey.newBuilder().setTenant(TENANT).setResourceId(entity.getResourceId()).build();
    rbacStub.deleteResource(pk);
  }


  public void moveOrderByUp(String resourceId) {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    ResourceMoveKey moveKey = ResourceMoveKey.newBuilder().setTenant(TENANT)
        .setResourceId(resourceId).setUp(true).build();
    rbacStub.moveResouce(moveKey);
  }

  public void moveOrderByDown(String resourceId) {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    ResourceMoveKey moveKey = ResourceMoveKey.newBuilder().setTenant(TENANT)
        .setResourceId(resourceId).setUp(false).build();
    rbacStub.moveResouce(moveKey);
  }

  @Test
  public void getPermList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    ResourceReq.Builder request = ResourceReq.newBuilder();
    request.setTenant(TENANT);
    request.setModuleId("C415075752D14BFAA606521EAAE11C5F");
    final ResourcePermList modelList = rbacStub.findResourcePermList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(5)
  public void exportResourceList() throws IOException, InterruptedException {
    RbacGrpc.RbacStub rbacStub1 = RbacGrpc.newStub(channel);
    rbacStub1 = rbacStub1.withCallCredentials(getAuthcCallCredentials());
    ResourceExportReq req = ResourceExportReq.newBuilder()
        .setReq(ResourceReq.newBuilder().setResourceId("8C9F6898F6B0440BB28EA587C11B6E5E").build())
        .setTenant(TENANT).build();
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp =
        FileClientUtil.createHttpBodyResp(new File("/data/jmash/test/resource.xlsx"), latchFinish);
    rbacStub1.exportResource(req, resp);
    latchFinish.await();
  }

  //@Test
  @Order(6)
  public void importResourceExcel() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    FieldMask mask = FieldMask.newBuilder().addPaths("target").build();
    rbacStub.importResource(
        ResourceImportReq.newBuilder().setRequestId(UUID.randomUUID().toString()).setTenant(TENANT)
            .setFileNames("resource.xlsx").setAddFlag(false).setUpdateMask(mask).build());
  }


}

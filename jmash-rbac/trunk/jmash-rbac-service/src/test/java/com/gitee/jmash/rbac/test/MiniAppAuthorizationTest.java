package com.gitee.jmash.rbac.test;

import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.MiniAppLoginReq;
import jmash.rbac.protobuf.MiniAppLoginResp;
import jmash.rbac.protobuf.MiniAppPhoneNumberReq;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@Disabled
@TestMethodOrder(OrderAnnotation.class)
public class MiniAppAuthorizationTest extends RbacTest{

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  /** wx.login获取到的code. */
  public static final String LOGIN_CODE = "0f1JEYFa1issMH03mrGa1qc7MQ1JEYFU";
  /** 微信小程序手机号验证组件获取到的code. */
  public static final String PHONE_CODE = "184fe3c40dd8e3cc1f93ab51311a1902a59122a4b012127d22ec52f343a8fc59";
  /** appId. */
  public static final String APP_ID = "wx7ac8fa4d58d1cc34";

  @Test
  public void miniProgramLogin(){
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    MiniAppLoginReq.Builder loginReq = MiniAppLoginReq.newBuilder();
    loginReq.setTenant(TENANT);
    loginReq.setLoginCode(LOGIN_CODE);
    loginReq.setAppId(APP_ID);
    MiniAppLoginResp resp = rbacStub.miniAppLogin(loginReq.build());
    System.out.println(resp);
  }

  @Test
  public void phoneNumberLogin(){
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    MiniAppPhoneNumberReq.Builder loginReq = MiniAppPhoneNumberReq.newBuilder();
    loginReq.setTenant(TENANT);
    loginReq.setLoginCode(LOGIN_CODE);
    loginReq.setPhoneCode(PHONE_CODE);
    loginReq.setAppId(APP_ID);
    MiniAppLoginResp resp = rbacStub.miniAppPhoneNumber(loginReq.build());
    System.out.println(resp);
  }

}

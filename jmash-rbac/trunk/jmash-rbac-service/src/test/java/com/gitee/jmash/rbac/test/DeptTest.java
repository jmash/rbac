package com.gitee.jmash.rbac.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.google.api.HttpBody;
import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import jmash.protobuf.TenantReq;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.DeptCreateReq;
import jmash.rbac.protobuf.DeptKey;
import jmash.rbac.protobuf.DeptKeyList;
import jmash.rbac.protobuf.DeptList;
import jmash.rbac.protobuf.DeptModel;
import jmash.rbac.protobuf.DeptMoveKey;
import jmash.rbac.protobuf.DeptReq;
import jmash.rbac.protobuf.DeptUpdateReq;
import jmash.rbac.protobuf.TreeList;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class DeptTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;
  private RbacGrpc.RbacStub rbac = null;

  @Test
  @Order(1)
  public void findList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final DeptReq.Builder request = DeptReq.newBuilder();
    request.setTenant(TENANT);
    final TreeList modelList = rbacStub.findDeptTreeList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(2)
  public void findDeptList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final DeptReq.Builder request = DeptReq.newBuilder();
    request.setTenant(TENANT);
    request.setHasStatus(false);
    final DeptList modelList = rbacStub.findDeptList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(3)
  public void findTreeList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final DeptReq.Builder request = DeptReq.newBuilder();
    request.setTenant(TENANT);
    request.setDeptType("union");
    request.setHasOpen(true);
    request.setIsOpen(false);
    final TreeList modelList = rbacStub.findDeptTreeList(request.build());
    System.out.println(modelList);
  }

  //@Test
  @Order(4)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    DeptCreateReq.Builder req = DeptCreateReq.newBuilder();
    req.setTenant(TENANT);
    req.setRequestId(UUID.randomUUID().toString());
    req.setDeptCode("col");
    req.setDeptName("研发部");
    req.setDeptType("dept");
    req.setDescription("研发部");
    req.setParentId("f95fe76e-3020-4838-904c-1902f703c1f6");
    // ...
    DeptModel entity = rbacStub.createDept(req.build());

    // findById
    DeptMoveKey moveKey = DeptMoveKey.newBuilder().setTenant(TENANT)
        .setDeptId("389FE148B1DA44C1932F69C2996302B8").setUp(false).build();
    rbacStub.moveDept(moveKey);
    DeptKey pk = DeptKey.newBuilder().setTenant(TENANT).setDeptId(entity.getDeptId()).build();
    entity = rbacStub.findDeptById(pk);
    // Delete
    pk = DeptKey.newBuilder().setDeptId(entity.getDeptId()).build();
    entity = rbacStub.deleteDept(pk);
  }

  static String deptId = "D1673D4C3A4549FC86058D360516A44F";

  @Test
  @Order(5)
  public void createDeptTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    DeptCreateReq.Builder req = DeptCreateReq.newBuilder();
    req.setTenant(TENANT);
    req.setRequestId(UUID.randomUUID().toString());
    req.setDeptCode("ZJGH");
    req.setDeptName("自己的工会");
    req.setDeptType("union");
    req.setDescription("研发部");
    req.setStatus(true);
    DeptModel entity = rbacStub.createDept(req.build());
    deptId = entity.getDeptId();
  }

  @Test
  @Order(6)
  public void updateDeptTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    DeptUpdateReq.Builder req = DeptUpdateReq.newBuilder();
    req.setTenant(TENANT).setDeptId(deptId);
    req.setRequestId(UUID.randomUUID().toString());
    req.setDeptName("自己的工会2");
    req.setIsOpen(true);
    // FieldMask.
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("isOpen");
    fieldMask.addPaths("deptName");
    req.setUpdateMask(fieldMask.build());
    rbacStub.updateDept(req.build());
  }

  @Test
  @Order(7)
  public void batchDeleteTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    DeptKeyList.Builder request = DeptKeyList.newBuilder();
    request.setTenant(TENANT).addDeptId(deptId);
    final Int32Value result = rbacStub.batchDeleteDept(request.build());
    System.out.println(result);
  }

  @Test
  @Order(8)
  public void download() throws IOException, InterruptedException {
    rbac = RbacGrpc.newStub(channel);
    rbac = rbac.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil
        .createHttpBodyResp(new File("/data/jmash/test/dept_template.xlsx"), latchFinish);
    rbac.downloadDept(TenantReq.newBuilder().setTenant(TENANT).build(), resp);
    latchFinish.await();
  }

  @Test
  @Order(9)
  public void findDeptListByParentId() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final DeptReq.Builder request = DeptReq.newBuilder();
    request.setTenant(TENANT);
    request.setHasStatus(false);
    request.setParentId("FF1AE98C252742A0BFDFD4F9F9946717");
    final DeptList modelList = rbacStub.findDeptList(request.build());
    System.out.println(modelList);
  }

}

package com.gitee.jmash.rbac.test;

import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.OpenAppCreateReq;
import jmash.rbac.protobuf.OpensType;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@Disabled
@TestMethodOrder(OrderAnnotation.class)
public class OpenAppTest extends RbacTest{
  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  public void create(){
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    OpenAppCreateReq.Builder req = OpenAppCreateReq.newBuilder();
    req.setTenant(TENANT);
    req.setAppId("wxf0cb877ce02918b2");
    req.setAppName("航产工会");
    req.setAppSecret("b1ef1ede62ea90e259ce0ec3314e42d8");
    req.setDescription("航产工会生产环境小程序信息");
    req.setOpenType(OpensType.wechat);
    rbacStub.createOpenApp(req.build());
  }
}

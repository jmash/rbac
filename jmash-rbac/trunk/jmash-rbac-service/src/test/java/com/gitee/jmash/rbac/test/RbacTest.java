
package com.gitee.jmash.rbac.test;

import com.gitee.jmash.common.grpc.auth.BasicCredentials;
import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.rbac.shiro.JmashShiroConfig;
import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.LoginReq;
import jmash.rbac.protobuf.TokenResp;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public abstract class RbacTest {

  protected static DefaultGrpcServer server;

  protected static ManagedChannel channel = null;

  protected static TokenResp token;

  protected static final String TENANT = "rbac";

  @BeforeAll
  public static synchronized void setup() throws Exception {
    if (server == null) {
      SeContainer container = SeContainerInitializer.newInstance().initialize();
      // Shiro 初始化.
      JmashShiroConfig.config();
      server = (DefaultGrpcServer) container.select(DefaultGrpcServer.class).get();
      server.start(true);
      channel = GrpcChannel.getInProcessChannel();
    }
  }

  @AfterAll
  public static void stop() throws InterruptedException {
    // channel.shutdownNow();
    // server.stop();
  }

  /** 认证授权. */
  public static CallCredentials getOAuthCallCredentials() {
    String clientSecret =
        "694f956fe6ccfbb2e2905f1da3f00ddc7cf72737a7bab7bfe09f8cf6b13178e679bb3e699d1a2709";
    return new BasicCredentials("TestClient", clientSecret);
  }


  /** 授权 */
  public static CallCredentials getAuthcCallCredentials() {
    if (token == null) {
      token = RbacTest.login("admin",
          "0425561d7d70045b7d2fe3198cc5d33398ef0b48f13e7d82f8fbf36cfa7bec7e1339c3b5edfcfc6e2d1c1dab3cf6ee6e19fabc95df5fbf4373ca5d5d630c35c67b7ff3d71d63a26c645b7476ae0e6acecf8d52934b9d1a774593a239dc557677da481740d5cd586a3c7b5d7d4182");
    }
    return new OAuth2Credentials(token.getAccessToken());
  }

  /** 授权 */
  public static CallCredentials getAuthcCallCredentials(String organTenant) {
    getAuthcCallCredentials();
    return new OAuth2Credentials(token.getAccessToken(), organTenant);
  }

  /** 登录系统. */
  public static TokenResp login(String userName, String pwd) {
    RbacGrpc.RbacBlockingStub rbacStub = RbacGrpc.newBlockingStub(channel);
    LoginReq request = LoginReq.newBuilder().setTenant(TENANT).setDirectoryId("jmash")
        .setUserName(userName).setEncodePwd(pwd).setClientId("test").build();
    LogFactory.getLog(RbacTest.class).warn(request);
    return rbacStub.login(request);
  }

}

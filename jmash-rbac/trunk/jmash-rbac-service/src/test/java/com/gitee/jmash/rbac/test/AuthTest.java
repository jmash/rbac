
package com.gitee.jmash.rbac.test;

import com.gitee.jmash.captcha.client.cdi.CaptchaClient;
import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.rbac.grpc.RbacImpl;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import jmash.protobuf.EntryList;
import jmash.protobuf.EnumEntryReq;
import jmash.protobuf.TenantReq;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.LoginQrcodeReq;
import jmash.rbac.protobuf.LogoutReq;
import jmash.rbac.protobuf.MenuList;
import jmash.rbac.protobuf.RolePermSet;
import jmash.rbac.protobuf.SendValidCodeReq;
import jmash.rbac.protobuf.TokenResp;
import jmash.rbac.protobuf.UserDeptJobInfoRes;
import jmash.rbac.protobuf.UserKey;
import jmash.rbac.protobuf.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class AuthTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(0)
  public void version() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    final StringValue value = rbacStub.version(Empty.getDefaultInstance());
    Assertions.assertEquals(value.getValue().substring(0, RbacImpl.version.length()),
        RbacImpl.version);
  }

  @Test
  @Order(1)
  public void enumEntry() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    final EntryList list = rbacStub
        .findEnumEntry(EnumEntryReq.newBuilder().setClassName("jmash.protobuf.Gender").build());
    Assertions.assertEquals(list.getValuesCount(), 2);
  }

  @Test
  @Order(2)
  public void userInfo() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final TenantReq.Builder request = TenantReq.newBuilder();
    request.setTenant(TENANT);
    final UserModel model = rbacStub.userInfo(request.build());
    System.out.println(model);
  }

  @Test
  @Order(3)
  public void rolePerms() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final TenantReq.Builder request = TenantReq.newBuilder();
    request.setTenant(TENANT);
    final RolePermSet rolePermSet = rbacStub.userRolesPerms(request.build());
    System.out.println(rolePermSet);
  }

  @Test
  @Order(4)
  public void userMenus() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final TenantReq.Builder request = TenantReq.newBuilder();
    request.setTenant(TENANT);
    MenuList list = rbacStub.userMenus(request.build());
    System.out.println(list);
  }

  // @Test
  @Order(5)
  public void logout() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final LogoutReq.Builder request = LogoutReq.newBuilder();
    request.setTenant(TENANT);
    request.setAccessToken(token.getAccessToken());
    request.setClientId("test");
    final BoolValue model = rbacStub.logout(request.build());
    System.out.println(model);
  }

  // @Test
  @Order(6)
  public void sendValidCode() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    SendValidCodeReq sendValidCodeReq = SendValidCodeReq.newBuilder().setDirectoryId("jmash")
        .setName("18636635682").setTenant(TENANT).build();
    rbacStub.sendValidCode(sendValidCodeReq);
  }

  @Test
  @Order(5)
  public void pingTest() {
    StringValue value = CaptchaClient.getCaptchaBlockingStub().ping(Empty.getDefaultInstance());
    Assertions.assertEquals(value.getValue(), "PONG");
  }

  @Test
  public void selectUserInfo() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserKey userKey = UserKey.newBuilder().setTenant(TENANT)
        .setUserId("a12cb34c-1985-49a6-9da6-f220edf50e1f").build();
    UserDeptJobInfoRes userModel = rbacStub.selectUserInfo(userKey);
    System.out.println(userModel);
  }

  @Test
  public void loginOrgan() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    String organToken = "a17a54983d4f49b0a2d87fc558f8f10e@e01";
    TokenResp token = rbacStub.loginOrgan(StringValue.of(organToken));
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(new OAuth2Credentials(token.getAccessToken()));
    final TenantReq.Builder request = TenantReq.newBuilder().setTenant(organToken);
    final UserModel model = rbacStub.userInfo(request.build());
    System.out.println(model);
    rbacStub = rbacStub.withCallCredentials(new OAuth2Credentials(token.getAccessToken()));
    LogoutReq logout= LogoutReq.newBuilder().setAccessToken(token.getAccessToken()).setClientId("test").setTenant(organToken).build();
    rbacStub.logout(logout);
  }

  @Test
  public void organUserInfo() {
    // 本地调试修改OrganUserAccessToken.loginOrgan管道.
    rbacStub = RbacGrpc.newBlockingStub(channel);
    String organTenant = "a17a54983d4f49b0a2d87fc558f8f10e@e01";
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials(organTenant));
    final TenantReq.Builder request = TenantReq.newBuilder().setTenant(organTenant);
    final UserModel model = rbacStub.userInfo(request.build());
    System.out.println(model);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials(TENANT));
    LogoutReq logout= LogoutReq.newBuilder().setAccessToken(token.getAccessToken()).setClientId("test").setTenant(TENANT).build();
    rbacStub.logout(logout);
  }

  @Test
  public void loginOrgan2() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    String organToken = "2b1a91379c074b2b834892f984059f4e@e02";
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials(TENANT));

    // TokenResp token = rbacStub.loginOrgan(StringValue.of(organToken));

    // rbacStub = RbacGrpc.newBlockingStub(channel);
    // rbacStub = rbacStub.withCallCredentials(new OAuth2Credentials(token.getAccessToken()));
    final TenantReq.Builder request = TenantReq.newBuilder().setTenant(organToken);
    final UserModel model = rbacStub.userInfo(request.build());
    System.out.println(model);
  }

  @Test
  public void loginByQrcode() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    LoginQrcodeReq.Builder req = LoginQrcodeReq.newBuilder();
    req.setTenant(TENANT).setDirectoryId("jmash").setClientId("test");
    req.setAuthorizerAppid("wx2ec7549343d60f3a").setTicket(
        "gQE07zwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyZlMyUkZTMVk4YmYxaHNtd05EY3cAAgRMCGBnAwQQDgAA");
    TokenResp resp = rbacStub.loginByQrcode(req.build());
    System.out.println(resp);
  }

}

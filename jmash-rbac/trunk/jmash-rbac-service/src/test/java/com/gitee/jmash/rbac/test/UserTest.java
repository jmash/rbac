
package com.gitee.jmash.rbac.test;

import com.gitee.jmash.core.lib.FileClientUtil;
import com.gitee.jmash.core.utils.NetSecretUtil;
import com.google.api.HttpBody;
import com.google.protobuf.BoolValue;
import com.google.protobuf.FieldMask;
import com.google.protobuf.Int32Value;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import jmash.protobuf.Gender;
import jmash.protobuf.TenantReq;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.DirectoryListResp;
import jmash.rbac.protobuf.OrganUserCreateReq;
import jmash.rbac.protobuf.UserCreateReq;
import jmash.rbac.protobuf.UserDeptJobInfoRes;
import jmash.rbac.protobuf.UserEnableKey;
import jmash.rbac.protobuf.UserExportReq;
import jmash.rbac.protobuf.UserImportReq;
import jmash.rbac.protobuf.UserJobs;
import jmash.rbac.protobuf.UserKey;
import jmash.rbac.protobuf.UserModel;
import jmash.rbac.protobuf.UserNameReq;
import jmash.rbac.protobuf.UserPage;
import jmash.rbac.protobuf.UserReq;
import jmash.rbac.protobuf.UserResetPwdReq;
import jmash.rbac.protobuf.UserRoleReq;
import jmash.rbac.protobuf.UserStatus;
import jmash.rbac.protobuf.UserUpdateReq;
import jmash.rbac.protobuf.VerifyUserReq;
import org.jose4j.json.internal.json_simple.JSONObject;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 */
@TestMethodOrder(OrderAnnotation.class)
public class UserTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;
  private RbacGrpc.RbacStub rbac = null;

  private final static String userId = "cd81f6b2-d763-4bcd-85f2-dbc029bccf53";


  @Test
  @Order(1)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final UserReq.Builder request = UserReq.newBuilder().setTenant(TENANT);
    request.setLoginName("admin");
    // request.setDeptId("a509de8e-cb38-4bff-9bd5-78472a919dfa");
    request.setShowDeleted(false);
    request.setPageSize(10);
    request.setCurPage(1);
    final UserPage modelPage = rbacStub.findUserPage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    UserCreateReq.Builder req = UserCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setDirectoryId("jmash");
    req.setLoginName("TEST-" + Math.random());
    req.setMobilePhone("");
    req.setRealName("测试数据1");
    req.setStatus(UserStatus.enabled);
    UserModel entity = rbacStub.createUser(req.build());
    System.out.println(entity);

    // findById
    UserKey userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(entity.getUserId()).build();
    entity = rbacStub.findUserById(userKey);

    // Update
    UserUpdateReq.Builder updateReq = UserUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT).setUserId(entity.getUserId());
    updateReq.setRealName("王燕红1");
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("realName");
    updateReq.setUpdateMask(fieldMask.build());
    entity = rbacStub.updateUser(updateReq.build());

    resetPwdTest(entity.getUserId());
    enableTest(entity.getUserId());
    // Delete
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(entity.getUserId()).build();
    entity = rbacStub.deleteUser(userKey);
  }

  public void resetPwdTest(String userId) {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    String pwd= NetSecretUtil.encrypt("1b1221");
    UserResetPwdReq req = UserResetPwdReq.newBuilder().setTenant(TENANT).setUserId(userId)
        .setPwd(pwd).setRepeatPwd(pwd).build();
    rbacStub.resetPwd(req);
  }

  public void enableTest(String userId) {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserEnableKey userKey =
        UserEnableKey.newBuilder().setTenant(TENANT).setUserId(userId).setEnabled(true).build();
    rbacStub.enableUser(userKey);
  }

  @Test
  @Order(3)
  public void download() throws IOException, InterruptedException {
    rbac = RbacGrpc.newStub(channel);
    rbac = rbac.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp = FileClientUtil
        .createHttpBodyResp(new File("/data/jmash/test/user_template.xlsx"), latchFinish);
    rbac.downloadUserTemplate(TenantReq.newBuilder().setTenant(TENANT).build(), resp);
    latchFinish.await();
  }

  @Test
  @Order(4)
  public void exportUser() throws IOException, InterruptedException {
    rbac = RbacGrpc.newStub(channel);
    rbac = rbac.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp =
        FileClientUtil.createHttpBodyResp(new File("/data/jmash/test/user.xlsx"), latchFinish);
    rbac.exportUser(UserExportReq.newBuilder().setTenant(TENANT).build(), resp);
    latchFinish.await();
  }

  @Test
  @Order(5)
  public void printUser() throws IOException, InterruptedException {
    rbac = RbacGrpc.newStub(channel);
    rbac = rbac.withCallCredentials(getAuthcCallCredentials());
    final CountDownLatch latchFinish = new CountDownLatch(1);
    StreamObserver<HttpBody> resp =
        FileClientUtil.createHttpBodyResp(new File("/data/jmash/test/user.pdf"), latchFinish);
    rbac.printUser(UserExportReq.newBuilder().setTenant(TENANT).build(), resp);
    latchFinish.await();
  }

  // @Test
  @Order(6)
  public void importUpdateExcel() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    FieldMask mask = FieldMask.newBuilder().addPaths("gender").addPaths("loginName")
        .addPaths("directoryId").addPaths("birthDate").build();
    rbacStub.importUser(UserImportReq.newBuilder().setRequestId(UUID.randomUUID().toString())
        .setTenant(TENANT).setFileNames("user.xlsx").setAddFlag(false).setUpdateMask(mask).build());
  }

  @Test
  @Order(7)
  public void importInsertExcel() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    FieldMask mask = FieldMask.newBuilder().build();
    rbacStub.importUser(
        UserImportReq.newBuilder().setRequestId(UUID.randomUUID().toString()).setTenant(TENANT)
            .setFileNames("rbac/text/2024_01/de67c633-2a6f-46b7-a346-98eb7d9dbfe4.xlsx")
            .setAddFlag(true).setUpdateMask(mask).build());
  }

  // @Test
  @Order(8)
  public void assignUser() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserRoleReq.Builder request = UserRoleReq.newBuilder().setTenant(TENANT)
        .setUserId("4F20C68BCBFF4A1DBFD928DD64F227F6").addRoleCodes("tester");
    final Int32Value result = rbacStub.assignUser(request.build());
    System.out.println(result);
  }

  @Test
  @Order(9)
  public void deassignUser() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserRoleReq.Builder request = UserRoleReq.newBuilder().setTenant(TENANT)
        .setUserId("4F20C68BCBFF4A1DBFD928DD64F227F6").addRoleCodes("tester");
    final Int32Value result = rbacStub.deassignUser(request.build());
    System.out.println(result);
  }

  // @Test
  public void userRoleUpdate() {
    // 测试逻辑有无误.
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    List<String> roleIds = new ArrayList();
    roleIds.add("ded4c31f-3604-4f58-ac07-de8bfa5c15fd");
    roleIds.add("ee2a6f5e-3c6e-451c-b19c-fac152a509a3");
    roleIds.add("5848cf88-3d1e-4e79-a969-b6d07868231b");
    UserUpdateReq updateUserReq = UserUpdateReq.newBuilder().setTenant(TENANT)
        .setRequestId(UUID.randomUUID().toString()).addAllRoleIds(roleIds).build();

    UserModel userModel = rbacStub.updateUser(updateUserReq);
    System.out.println(userModel);
  }

  @Test
  public void getAllowRunAsUser() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    TenantReq tenantReq = TenantReq.newBuilder().setTenant(TENANT).build();
    rbacStub.allowRunAsUser(tenantReq);
  }

  @Test
  public void createUserEmpty() {
    // create
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserCreateReq createUserReq =
        UserCreateReq.newBuilder().setTenant(TENANT).setStatus(UserStatus.enabled)
            .setDirectoryId("jmash").setRequestId(UUID.randomUUID().toString()).build();
    UserModel userModel = rbacStub.createUser(createUserReq);
    String userId = userModel.getUserId();
    System.out.println(userModel);
    // select
    UserKey userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.findUserById(userKey);
    System.out.println(userModel);
    // update
    FieldMask fieldMask = FieldMask.newBuilder().build();
    UserUpdateReq userUpdateReq = UserUpdateReq.newBuilder().setTenant(TENANT)
        .setRequestId(UUID.randomUUID().toString()).setUpdateMask(fieldMask).setApproved(true)
        .setPhoneApproved(true).setEmailApproved(true).setStatus(UserStatus.locked)
        .setUserId(userId).setDirectoryId("jmash").build();
    userModel = rbacStub.updateUser(userUpdateReq);
    System.out.println(userModel);
    // select
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.findUserById(userKey);
    System.out.println(userModel);
    // delete
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.deleteUser(userKey);
    System.out.println(userModel);
  }

  @Test
  public void createUserAll() {
    // create
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserJobs userJobs = UserJobs.newBuilder().setJobId("").setDeptId("").build();
    UserCreateReq createUserReq = UserCreateReq.newBuilder().setTenant(TENANT)
        .setStatus(UserStatus.enabled).setDirectoryId("jmash")
        .setRequestId(UUID.randomUUID().toString()).setAvatar("/profile/UploadFile/2222222.jpg")
        .setBirthDate("1998-10-26").setGender(Gender.male)
        .setLoginName("jmash" + generateRandomString(4)).setNickName("小吴").setRealName("吴毓豪")
        .addRoleIds("74378066-ca91-48a8-b967-e08bbb558ac1").addUserJobs(userJobs).build();
    UserModel userModel = rbacStub.createUser(createUserReq);
    String userId = userModel.getUserId();
    System.out.println(userModel);
    // select
    UserKey userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.findUserById(userKey);
    System.out.println(userModel);
    // 查询部门岗位信息
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    UserDeptJobInfoRes userDeptInfo = rbacStub.getUserDeptInfo(userKey);
    Map<String, Object> result = new HashMap<>();
    result.put("result", userDeptInfo);
    System.out.println(JSONObject.toJSONString(result));
    // 用户名/手机号/邮箱名称是否已存在
    VerifyUserReq verifyUserReq = VerifyUserReq.newBuilder().setTenant(TENANT)
        .setDirectoryId("jmash").setUserName("1").setUserId(userId).build();
    rbacStub.existUser(verifyUserReq);
    // update
    UserUpdateReq userUpdateReq =
        UserUpdateReq.newBuilder().setTenant(TENANT).setRequestId(UUID.randomUUID().toString())
            .setAvatar("/profile/UploadFile/2222222.jpg").setBirthDate("1998-10-26")
            .setGender(Gender.male).setNickName("小吴").setRealName("吴毓豪").setUserId(userId).build();
    userModel = rbacStub.updateUser(userUpdateReq);
    System.out.println(userModel);
    // select
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.findUserById(userKey);
    System.out.println(userModel);
    // delete
    userKey = UserKey.newBuilder().setTenant(TENANT).setUserId(userId).build();
    userModel = rbacStub.deleteUser(userKey);
    System.out.println(userModel);

  }

  @Test
  public void getDsdRoleList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    TenantReq req = TenantReq.newBuilder().setTenant(TENANT).build();
    rbacStub.selectDsdRoles(req);
  }

  @Test
  public void getDirectoryIdList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    TenantReq req = TenantReq.newBuilder().setTenant(TENANT).build();
    DirectoryListResp directoryList = rbacStub.selectDirectoryIds(req);
    System.out.println(directoryList);
  }


  @Test
  public void findUserPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserReq req = UserReq.newBuilder().setTenant(TENANT).setCurPage(1).setPageSize(10).build();
    UserPage page = rbacStub.findUserPage(req);
    System.out.println(page);
  }



  private static String generateRandomString(int length) {
    // 定义字符数组
    char[] characters =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    // 使用 Random 生成随机索引
    Random random = new Random();

    // 生成随机字符串
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < length; i++) {
      char randomChar = characters[random.nextInt(characters.length)];
      stringBuilder.append(randomChar);
    }

    return stringBuilder.toString();
  }

  @Test
  public void findUserByNameTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserNameReq req = UserNameReq.newBuilder().setTenant(TENANT).setDirectoryId("jmash")
        .setUserName("18903413101").build();
    rbacStub.findUserByName(req);
  }

  @Test
  public void existUserTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    VerifyUserReq req = VerifyUserReq.newBuilder().setTenant(TENANT).setDirectoryId("user")
        .setUserName("15346789056").build();
    BoolValue existUser = rbacStub.existUser(req);
    System.out.println(existUser);
  }

  @Test
  public void findUserInfoList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    List<String> users = new ArrayList<>();
    users.add("768144ca-1ac1-4647-b6f5-5eb0b2de3971");
    users.add("bb6adedc-e804-4ec6-a98c-e92b71ee7f3b");
    UserReq req = UserReq.newBuilder().setTenant(TENANT).addAllUserId(users).build();
    System.out.println(rbacStub.findUserInfoList(req));
  }

  @Test
  public void findUserInfoPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserReq req = UserReq.newBuilder().setTenant(TENANT)
        .setDeptId("012a067e-7cd6-4cbd-99a3-3be61298344e").build();
    System.out.println(rbacStub.findUserInfoPage(req));
  }
  
  @Test
  public void createOrganManager() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    OrganUserCreateReq req = OrganUserCreateReq.newBuilder()
        .setOrganTenant("a17a54983d4f49b0a2d87fc558f8f10e@e02")
        .setCreator(true)
        .setDirectoryId("xyvcard")
        .setRealName("T1管理员").build();
    UserModel model= rbacStub.createOrganUser(req);
    System.out.println(model);
  }
  
  @Test
  public void createOrganUser() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    OrganUserCreateReq req = OrganUserCreateReq.newBuilder()
        .setOrganTenant("a17a54983d4f49b0a2d87fc558f8f10a@e02")
        .setCreator(false)
        .setDirectoryId("xyvcard")
        .addDeptId("BB36352D18F04F53B7410FA2EEC8D4B5")
        .setRealName("职工").build();
    UserModel model= rbacStub.createOrganUser(req);
    System.out.println(model);
  }

}

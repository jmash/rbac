
call  docker build --platform linux/%1 --build-arg GOARCH=%1 -t jmash-rbac-service-%1:1.0.0  -f ./Dockerfile  ../

call  docker login harbor.crenjoy.com:9443
jmash

call  docker  tag  jmash-rbac-service-%1:1.0.0  harbor.crenjoy.com:9443/library/jmash-rbac-service-%1:1.0.0

call  docker  push harbor.crenjoy.com:9443/library/jmash-rbac-service-%1:1.0.0

call  docker  pull harbor.crenjoy.com/library/jmash-rbac-service-%1:1.0.0
